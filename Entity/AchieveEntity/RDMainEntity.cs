﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using AchieveCommon;
//using Maticsoft.DBUtility;//Please add references
namespace AchieveEntity
{
    /// <summary>
    /// 类RDMainEntity。
    /// </summary>
    [Serializable]
    public partial class RDMainEntity
    {
        public RDMainEntity()
        { }
        #region Model
        private string _projectid;
        private string _projectno;
        private string _projectname;
        private string _projectmanager;
        private string _projectclerk;
        private string _remark;
        private DateTime? _updatetime;
        private string _updateby;
        private string _appendid;
        private string _appendlistid;
        private string _createby;
        private DateTime? _createtime;
        private int? _status;
        private string _department;
        private string _icmono;
        private string _fitemno;
        private string _fmodel;
        private string _fname;
        private string _rdtype;
        private int? _plevel;
        private decimal? _afactor;
        private decimal? _ifactor;
        private decimal? _rdestcost;
        private decimal? _estprice;
        private DateTime? _starttime;
        private DateTime? _endtime;
        private decimal? _awardamount;
        private decimal? _evalamount;
        /// <summary>
        /// 
        /// </summary>
        public string ProjectID
        {
            set { _projectid = value; }
            get { return _projectid; }
        }
        /// <summary>
        /// 研发项目编号
        /// </summary>
        public string ProjectNo
        {
            set { _projectno = value; }
            get { return _projectno; }
        }
        /// <summary>
        /// 研发项目名称
        /// </summary>
        public string ProjectName
        {
            set { _projectname = value; }
            get { return _projectname; }
        }
        /// <summary>
        /// 项目经理
        /// </summary>
        public string ProjectManager
        {
            set { _projectmanager = value; }
            get { return _projectmanager; }
        }
        /// <summary>
        /// 设备负责人
        /// </summary>
        public string ProjectClerk
        {
            set { _projectclerk = value; }
            get { return _projectclerk; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdateTime
        {
            set { _updatetime = value; }
            get { return _updatetime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AppendID
        {
            set { _appendid = value; }
            get { return _appendid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AppendListID
        {
            set { _appendlistid = value; }
            get { return _appendlistid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CreateBy
        {
            set { _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreateTime
        {
            set { _createtime = value; }
            get { return _createtime; }
        }
        /// <summary>
        /// 项目状态，0：未确认，1：已确认；2：已完成；
        /// </summary>
        public int? Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string Department
        {
            set { _department = value; }
            get { return _department; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string IcmoNo
        {
            set { _icmono = value; }
            get { return _icmono; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FitemNo
        {
            set { _fitemno = value; }
            get { return _fitemno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FModel
        {
            set { _fmodel = value; }
            get { return _fmodel; }
        }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string FName
        {
            set { _fname = value; }
            get { return _fname; }
        }
        /// <summary>
        /// 研发类别，分指令、指导
        /// </summary>
        public string RDType
        {
            set { _rdtype = value; }
            get { return _rdtype; }
        }
        /// <summary>
        /// 优先级，1-10
        /// </summary>
        public int? pLevel
        {
            set { _plevel = value; }
            get { return _plevel; }
        }
        /// <summary>
        /// 先进系数，1.2
        /// </summary>
        public decimal? aFactor
        {
            set { _afactor = value; }
            get { return _afactor; }
        }
        /// <summary>
        /// 增型系数，1，0.3
        /// </summary>
        public decimal? iFactor
        {
            set { _ifactor = value; }
            get { return _ifactor; }
        }
        /// <summary>
        /// 研发费用估算（万元）
        /// </summary>
        public decimal? RDEstCost
        {
            set { _rdestcost = value; }
            get { return _rdestcost; }
        }
        /// <summary>
        /// 设备预售单价（万元）
        /// </summary>
        public decimal? estPrice
        {
            set { _estprice = value; }
            get { return _estprice; }
        }
        /// <summary>
        /// 项目开始时间
        /// </summary>
        public DateTime? startTime
        {
            set { _starttime = value; }
            get { return _starttime; }
        }
        /// <summary>
        /// 项目结束时间
        /// </summary>
        public DateTime? endTime
        {
            set { _endtime = value; }
            get { return _endtime; }
        }
        /// <summary>
        /// 研发奖励金额
        /// </summary>
        public decimal? awardAmount
        {
            set { _awardamount = value; }
            get { return _awardamount; }
        }
        /// <summary>
        /// 研发考核金额
        /// </summary>
        public decimal? evalAmount
        {
            set { _evalamount = value; }
            get { return _evalamount; }
        }
        #endregion Model


        #region  Method

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public RDMainEntity(string ProjectID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ProjectID,ProjectNo,ProjectName,ProjectManager,ProjectClerk,Remark,UpdateTime,UpdateBy,AppendID,AppendListID,CreateBy,CreateTime,Status,Department,IcmoNo,FitemNo,FModel,FName,RDType,pLevel,aFactor,iFactor,RDEstCost,estPrice,startTime,endTime,awardAmount,evalAmount ");
            strSql.Append(" FROM [tbRDMain] ");
            strSql.Append(" where ProjectID=@ProjectID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectID", SqlDbType.NVarChar,-1)};
            parameters[0].Value = ProjectID;

            DataSet ds = SqlHelper.GetDataset(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ProjectID"] != null)
                {
                    this.ProjectID = ds.Tables[0].Rows[0]["ProjectID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectNo"] != null)
                {
                    this.ProjectNo = ds.Tables[0].Rows[0]["ProjectNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectName"] != null)
                {
                    this.ProjectName = ds.Tables[0].Rows[0]["ProjectName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectManager"] != null)
                {
                    this.ProjectManager = ds.Tables[0].Rows[0]["ProjectManager"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectClerk"] != null)
                {
                    this.ProjectClerk = ds.Tables[0].Rows[0]["ProjectClerk"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null)
                {
                    this.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateTime"] != null && ds.Tables[0].Rows[0]["UpdateTime"].ToString() != "")
                {
                    this.UpdateTime = DateTime.Parse(ds.Tables[0].Rows[0]["UpdateTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null)
                {
                    this.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AppendID"] != null)
                {
                    this.AppendID = ds.Tables[0].Rows[0]["AppendID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AppendListID"] != null)
                {
                    this.AppendListID = ds.Tables[0].Rows[0]["AppendListID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreateBy"] != null)
                {
                    this.CreateBy = ds.Tables[0].Rows[0]["CreateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreateTime"] != null && ds.Tables[0].Rows[0]["CreateTime"].ToString() != "")
                {
                    this.CreateTime = DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    this.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Department"] != null)
                {
                    this.Department = ds.Tables[0].Rows[0]["Department"].ToString();
                }
                if (ds.Tables[0].Rows[0]["IcmoNo"] != null)
                {
                    this.IcmoNo = ds.Tables[0].Rows[0]["IcmoNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FitemNo"] != null)
                {
                    this.FitemNo = ds.Tables[0].Rows[0]["FitemNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FModel"] != null)
                {
                    this.FModel = ds.Tables[0].Rows[0]["FModel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FName"] != null)
                {
                    this.FName = ds.Tables[0].Rows[0]["FName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RDType"] != null)
                {
                    this.RDType = ds.Tables[0].Rows[0]["RDType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pLevel"] != null && ds.Tables[0].Rows[0]["pLevel"].ToString() != "")
                {
                    this.pLevel = int.Parse(ds.Tables[0].Rows[0]["pLevel"].ToString());
                }
                if (ds.Tables[0].Rows[0]["aFactor"] != null && ds.Tables[0].Rows[0]["aFactor"].ToString() != "")
                {
                    this.aFactor = decimal.Parse(ds.Tables[0].Rows[0]["aFactor"].ToString());
                }
                if (ds.Tables[0].Rows[0]["iFactor"] != null && ds.Tables[0].Rows[0]["iFactor"].ToString() != "")
                {
                    this.iFactor = decimal.Parse(ds.Tables[0].Rows[0]["iFactor"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RDEstCost"] != null && ds.Tables[0].Rows[0]["RDEstCost"].ToString() != "")
                {
                    this.RDEstCost = decimal.Parse(ds.Tables[0].Rows[0]["RDEstCost"].ToString());
                }
                if (ds.Tables[0].Rows[0]["estPrice"] != null && ds.Tables[0].Rows[0]["estPrice"].ToString() != "")
                {
                    this.estPrice = decimal.Parse(ds.Tables[0].Rows[0]["estPrice"].ToString());
                }
                if (ds.Tables[0].Rows[0]["startTime"] != null && ds.Tables[0].Rows[0]["startTime"].ToString() != "")
                {
                    this.startTime = DateTime.Parse(ds.Tables[0].Rows[0]["startTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["endTime"] != null && ds.Tables[0].Rows[0]["endTime"].ToString() != "")
                {
                    this.endTime = DateTime.Parse(ds.Tables[0].Rows[0]["endTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["awardAmount"] != null && ds.Tables[0].Rows[0]["awardAmount"].ToString() != "")
                {
                    this.awardAmount = decimal.Parse(ds.Tables[0].Rows[0]["awardAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["evalAmount"] != null && ds.Tables[0].Rows[0]["evalAmount"].ToString() != "")
                {
                    this.evalAmount = decimal.Parse(ds.Tables[0].Rows[0]["evalAmount"].ToString());
                }
            }
        }

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        //public bool Exists(string ProjectID)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select count(1) from [tbRDMain]");
        //    strSql.Append(" where ProjectID=@ProjectID ");

        //    SqlParameter[] parameters = {
        //            new SqlParameter("@ProjectID", SqlDbType.NVarChar,-1)};
        //    parameters[0].Value = ProjectID;

        //    return DbHelperSQL.Exists(strSql.ToString(), parameters);
        //}


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into [tbRDMain] (");
            strSql.Append("ProjectID,ProjectNo,ProjectName,ProjectManager,ProjectClerk,Remark,UpdateTime,UpdateBy,AppendID,AppendListID,CreateBy,CreateTime,Status,Department,IcmoNo,FitemNo,FModel,FName,RDType,pLevel,aFactor,iFactor,RDEstCost,estPrice,startTime,endTime,awardAmount,evalAmount)");
            strSql.Append(" values (");
            strSql.Append("@ProjectID,@ProjectNo,@ProjectName,@ProjectManager,@ProjectClerk,@Remark,@UpdateTime,@UpdateBy,@AppendID,@AppendListID,@CreateBy,@CreateTime,@Status,@Department,@IcmoNo,@FitemNo,@FModel,@FName,@RDType,@pLevel,@aFactor,@iFactor,@RDEstCost,@estPrice,@startTime,@endTime,@awardAmount,@evalAmount)");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectID", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectNo", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectName", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectManager", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectClerk", SqlDbType.NVarChar,50),
					new SqlParameter("@Remark", SqlDbType.NVarChar,255),
					new SqlParameter("@UpdateTime", SqlDbType.DateTime),
					new SqlParameter("@UpdateBy", SqlDbType.NVarChar,50),
					new SqlParameter("@AppendID", SqlDbType.NVarChar,50),
					new SqlParameter("@AppendListID", SqlDbType.NVarChar,50),
					new SqlParameter("@CreateBy", SqlDbType.NVarChar,50),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.TinyInt,1),
					new SqlParameter("@Department", SqlDbType.NVarChar,50),
					new SqlParameter("@IcmoNo", SqlDbType.NVarChar,50),
					new SqlParameter("@FitemNo", SqlDbType.NVarChar,50),
					new SqlParameter("@FModel", SqlDbType.NVarChar,50),
					new SqlParameter("@FName", SqlDbType.NVarChar,50),
					new SqlParameter("@RDType", SqlDbType.NVarChar,10),
					new SqlParameter("@pLevel", SqlDbType.Int,4),
					new SqlParameter("@aFactor", SqlDbType.Decimal,5),
					new SqlParameter("@iFactor", SqlDbType.Decimal,5),
					new SqlParameter("@RDEstCost", SqlDbType.Decimal,9),
					new SqlParameter("@estPrice", SqlDbType.Decimal,9),
					new SqlParameter("@startTime", SqlDbType.DateTime),
					new SqlParameter("@endTime", SqlDbType.DateTime),
					new SqlParameter("@awardAmount", SqlDbType.Decimal,9),
					new SqlParameter("@evalAmount", SqlDbType.Decimal,9)};
            parameters[0].Value = ProjectID;
            parameters[1].Value = ProjectNo;
            parameters[2].Value = ProjectName;
            parameters[3].Value = ProjectManager;
            parameters[4].Value = ProjectClerk;
            parameters[5].Value = Remark;
            parameters[6].Value = UpdateTime;
            parameters[7].Value = UpdateBy;
            parameters[8].Value = AppendID;
            parameters[9].Value = AppendListID;
            parameters[10].Value = CreateBy;
            parameters[11].Value = CreateTime;
            parameters[12].Value = Status;
            parameters[13].Value = Department;
            parameters[14].Value = IcmoNo;
            parameters[15].Value = FitemNo;
            parameters[16].Value = FModel;
            parameters[17].Value = FName;
            parameters[18].Value = RDType;
            parameters[19].Value = pLevel;
            parameters[20].Value = aFactor;
            parameters[21].Value = iFactor;
            parameters[22].Value = RDEstCost;
            parameters[23].Value = estPrice;
            parameters[24].Value = startTime;
            parameters[25].Value = endTime;
            parameters[26].Value = awardAmount;
            parameters[27].Value = evalAmount;

            SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update [tbRDMain] set ");
            strSql.Append("ProjectNo=@ProjectNo,");
            strSql.Append("ProjectName=@ProjectName,");
            strSql.Append("ProjectManager=@ProjectManager,");
            strSql.Append("ProjectClerk=@ProjectClerk,");
            //strSql.Append("Remark=@Remark,");
            strSql.Append("UpdateTime=@UpdateTime,");
            strSql.Append("UpdateBy=@UpdateBy,");
            //strSql.Append("AppendID=@AppendID,");
            //strSql.Append("AppendListID=@AppendListID,");
            //strSql.Append("CreateBy=@CreateBy,");
            //strSql.Append("CreateTime=@CreateTime,");
            strSql.Append("Status=@Status,");
            strSql.Append("Department=@Department,");
            strSql.Append("IcmoNo=@IcmoNo,");
            strSql.Append("FitemNo=@FitemNo,");
            strSql.Append("FModel=@FModel,");
            strSql.Append("FName=@FName,");
            strSql.Append("RDType=@RDType,");
            strSql.Append("pLevel=@pLevel,");
            strSql.Append("aFactor=@aFactor,");
            strSql.Append("iFactor=@iFactor,");
            strSql.Append("RDEstCost=@RDEstCost,");
            strSql.Append("estPrice=@estPrice,");
            strSql.Append("startTime=@startTime,");
            strSql.Append("endTime=@endTime,");
            strSql.Append("awardAmount=@awardAmount,");
            strSql.Append("evalAmount=@evalAmount");
            strSql.Append(" where ProjectID=@ProjectID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectNo", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectName", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectManager", SqlDbType.NVarChar,50),
					new SqlParameter("@ProjectClerk", SqlDbType.NVarChar,50),
					new SqlParameter("@Remark", SqlDbType.NVarChar,255),
					new SqlParameter("@UpdateTime", SqlDbType.DateTime),
					new SqlParameter("@UpdateBy", SqlDbType.NVarChar,50),
					new SqlParameter("@AppendID", SqlDbType.NVarChar,50),
					new SqlParameter("@AppendListID", SqlDbType.NVarChar,50),
					new SqlParameter("@CreateBy", SqlDbType.NVarChar,50),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.TinyInt,1),
					new SqlParameter("@Department", SqlDbType.NVarChar,50),
					new SqlParameter("@IcmoNo", SqlDbType.NVarChar,50),
					new SqlParameter("@FitemNo", SqlDbType.NVarChar,50),
					new SqlParameter("@FModel", SqlDbType.NVarChar,50),
					new SqlParameter("@FName", SqlDbType.NVarChar,50),
					new SqlParameter("@RDType", SqlDbType.NVarChar,10),
					new SqlParameter("@pLevel", SqlDbType.Int,4),
					new SqlParameter("@aFactor", SqlDbType.Decimal,5),
					new SqlParameter("@iFactor", SqlDbType.Decimal,5),
					new SqlParameter("@RDEstCost", SqlDbType.Decimal,9),
					new SqlParameter("@estPrice", SqlDbType.Decimal,9),
					new SqlParameter("@startTime", SqlDbType.DateTime),
					new SqlParameter("@endTime", SqlDbType.DateTime),
					new SqlParameter("@awardAmount", SqlDbType.Decimal,9),
					new SqlParameter("@evalAmount", SqlDbType.Decimal,9),
					new SqlParameter("@ProjectID", SqlDbType.NVarChar,50)};
            parameters[0].Value = ProjectNo;
            parameters[1].Value = ProjectName;
            parameters[2].Value = ProjectManager;
            parameters[3].Value = ProjectClerk;
            parameters[4].Value = Remark;
            parameters[5].Value = UpdateTime;
            parameters[6].Value = UpdateBy;
            parameters[7].Value = AppendID;
            parameters[8].Value = AppendListID;
            parameters[9].Value = CreateBy;
            parameters[10].Value = CreateTime;
            parameters[11].Value = Status;
            parameters[12].Value = Department;
            parameters[13].Value = IcmoNo;
            parameters[14].Value = FitemNo;
            parameters[15].Value = FModel;
            parameters[16].Value = FName;
            parameters[17].Value = RDType;
            parameters[18].Value = pLevel;
            parameters[19].Value = aFactor;
            parameters[20].Value = iFactor;
            parameters[21].Value = RDEstCost;
            parameters[22].Value = estPrice;
            parameters[23].Value = startTime;
            parameters[24].Value = endTime;
            parameters[25].Value = awardAmount;
            parameters[26].Value = evalAmount;
            parameters[27].Value = ProjectID;

            int rows = SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string ProjectID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from [tbRDMain] ");
            strSql.Append(" where ProjectID=@ProjectID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectID", SqlDbType.NVarChar,-1)};
            parameters[0].Value = ProjectID;

            int rows = SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public void GetModel(string ProjectID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ProjectID,ProjectNo,ProjectName,ProjectManager,ProjectClerk,Remark,UpdateTime,UpdateBy,AppendID,AppendListID,CreateBy,CreateTime,Status,Department,IcmoNo,FitemNo,FModel,FName,RDType,pLevel,aFactor,iFactor,RDEstCost,estPrice,startTime,endTime,awardAmount,evalAmount ");
            strSql.Append(" FROM [tbRDMain] ");
            strSql.Append(" where ProjectID=@ProjectID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectID", SqlDbType.NVarChar,-1)};
            parameters[0].Value = ProjectID;

            DataSet ds = SqlHelper.GetDataset(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ProjectID"] != null)
                {
                    this.ProjectID = ds.Tables[0].Rows[0]["ProjectID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectNo"] != null)
                {
                    this.ProjectNo = ds.Tables[0].Rows[0]["ProjectNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectName"] != null)
                {
                    this.ProjectName = ds.Tables[0].Rows[0]["ProjectName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectManager"] != null)
                {
                    this.ProjectManager = ds.Tables[0].Rows[0]["ProjectManager"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectClerk"] != null)
                {
                    this.ProjectClerk = ds.Tables[0].Rows[0]["ProjectClerk"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null)
                {
                    this.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateTime"] != null && ds.Tables[0].Rows[0]["UpdateTime"].ToString() != "")
                {
                    this.UpdateTime = DateTime.Parse(ds.Tables[0].Rows[0]["UpdateTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null)
                {
                    this.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AppendID"] != null)
                {
                    this.AppendID = ds.Tables[0].Rows[0]["AppendID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AppendListID"] != null)
                {
                    this.AppendListID = ds.Tables[0].Rows[0]["AppendListID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreateBy"] != null)
                {
                    this.CreateBy = ds.Tables[0].Rows[0]["CreateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreateTime"] != null && ds.Tables[0].Rows[0]["CreateTime"].ToString() != "")
                {
                    this.CreateTime = DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    this.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Department"] != null)
                {
                    this.Department = ds.Tables[0].Rows[0]["Department"].ToString();
                }
                if (ds.Tables[0].Rows[0]["IcmoNo"] != null)
                {
                    this.IcmoNo = ds.Tables[0].Rows[0]["IcmoNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FitemNo"] != null)
                {
                    this.FitemNo = ds.Tables[0].Rows[0]["FitemNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FModel"] != null)
                {
                    this.FModel = ds.Tables[0].Rows[0]["FModel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FName"] != null)
                {
                    this.FName = ds.Tables[0].Rows[0]["FName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RDType"] != null)
                {
                    this.RDType = ds.Tables[0].Rows[0]["RDType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pLevel"] != null && ds.Tables[0].Rows[0]["pLevel"].ToString() != "")
                {
                    this.pLevel = int.Parse(ds.Tables[0].Rows[0]["pLevel"].ToString());
                }
                if (ds.Tables[0].Rows[0]["aFactor"] != null && ds.Tables[0].Rows[0]["aFactor"].ToString() != "")
                {
                    this.aFactor = decimal.Parse(ds.Tables[0].Rows[0]["aFactor"].ToString());
                }
                if (ds.Tables[0].Rows[0]["iFactor"] != null && ds.Tables[0].Rows[0]["iFactor"].ToString() != "")
                {
                    this.iFactor = decimal.Parse(ds.Tables[0].Rows[0]["iFactor"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RDEstCost"] != null && ds.Tables[0].Rows[0]["RDEstCost"].ToString() != "")
                {
                    this.RDEstCost = decimal.Parse(ds.Tables[0].Rows[0]["RDEstCost"].ToString());
                }
                if (ds.Tables[0].Rows[0]["estPrice"] != null && ds.Tables[0].Rows[0]["estPrice"].ToString() != "")
                {
                    this.estPrice = decimal.Parse(ds.Tables[0].Rows[0]["estPrice"].ToString());
                }
                if (ds.Tables[0].Rows[0]["startTime"] != null && ds.Tables[0].Rows[0]["startTime"].ToString() != "")
                {
                    this.startTime = DateTime.Parse(ds.Tables[0].Rows[0]["startTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["endTime"] != null && ds.Tables[0].Rows[0]["endTime"].ToString() != "")
                {
                    this.endTime = DateTime.Parse(ds.Tables[0].Rows[0]["endTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["awardAmount"] != null && ds.Tables[0].Rows[0]["awardAmount"].ToString() != "")
                {
                    this.awardAmount = decimal.Parse(ds.Tables[0].Rows[0]["awardAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["evalAmount"] != null && ds.Tables[0].Rows[0]["evalAmount"].ToString() != "")
                {
                    this.evalAmount = decimal.Parse(ds.Tables[0].Rows[0]["evalAmount"].ToString());
                }
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM [tbRDMain] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return SqlHelper.GetDataset(strSql.ToString()); 
        }

        #endregion  Method
    }
}

