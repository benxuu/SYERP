﻿using System;
namespace AchieveEntity
{


    public class operTime
    {
        public operTime(string operName, int operId)
        {
            this.operName = operName;
            this.operId = operId;
            this.workTime = 0;
        }
        public operTime()
        {
        }
        public string operName;
        public int operId;
        public double workTime;
        public DateTime startDay;
        public string cycle;
    }

    

    //public class operTimeGroup
    //{
    //    public 
    //}
    public class OperEntity
    {
        public OperEntity()
        { 
        }
        public int OperID { get; set; }
        public string OperNote { get; set; }
        public float DayTime { get; set; }
        public float WeekTime { get; set; }
        public float MonthTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public string CheckBy { get; set; }
        public DateTime CheckTime { get; set; }
        public bool ischeck { get; set; }
        public string Remark { get; set; }

    }
}
