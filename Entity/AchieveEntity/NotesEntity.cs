﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AchieveCommon;


namespace AchieveEntity
{
    public class NotesEntity
    {
        public NotesEntity()
        { }

        #region Model
        private string _id;
        private string _typeid;
        private string _ftitle;
        private string _fcontent;
        /// <summary>
        /// 
        /// </summary>
        public string ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public string typeID
        {
            set { _typeid = value; }
            get { return _typeid; }
        }
        /// <summary>
        /// 状态，初始0，提交1，审核2，
        /// </summary>
        public int status{ get; set; }
        /// <summary>
        /// 引用ID
        /// </summary>
        public string citeID { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title{ get; set; }
        
        /// <summary>
        /// 内容
        /// </summary>
        public string noteContent{ get; set; }
        
        /// <summary>
        /// 审核人
        /// </summary>
        public string checkBy { get; set; }
        /// <summary>
        /// 审核时间 
        /// </summary>
        public DateTime checkTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string createBy { get; set; } 
        /// <summary>
        /// 创建时间 
        /// </summary>
        public DateTime createTime { get; set; }

        /// <summary>
        /// 最后更新人 
        /// </summary>
        public string updateBy { get; set; }

        /// <summary>
        /// 最后更新时间 
        /// </summary>
        public DateTime updateTime { get; set; }
        #endregion Model

        public bool Exists(string ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from tbNote");
            strSql.Append(" where ");
            strSql.Append(" ID = @ID  ");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.NVarChar,50)			};
            parameters[0].Value = ID;
            //DbHelperSQL.connectionString = "";
            return SqlHelper.Exists(strSql.ToString(), parameters);
        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(NotesEntity model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into tbNote(");
            strSql.Append("ID,updateBy,checkTime,checkBy,typeID,citeID,title,noteContent,status,createTime,createBy,updateTime");
            strSql.Append(") values (");
            strSql.Append("@ID,@updateBy,@checkTime,@checkBy,@typeID,@citeID,@title,@noteContent,@status,@createTime,@createBy,@updateTime");
            strSql.Append(") ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@updateBy", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@checkTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@checkBy", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@typeID", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@citeID", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@title", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@noteContent", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@createTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@createBy", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@updateTime", SqlDbType.DateTime)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.updateBy;
            parameters[2].Value = model.checkTime;
            parameters[3].Value = model.checkBy;
            parameters[4].Value = model.typeID;
            parameters[5].Value = model.citeID;
            parameters[6].Value = model.title;
            parameters[7].Value = model.noteContent;
            parameters[8].Value = model.status;
            parameters[9].Value = model.createTime;
            parameters[10].Value = model.createBy;
            parameters[11].Value = model.updateTime;
            SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AchieveEntity.NotesEntity model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tbNote set ");

            strSql.Append(" ID = @ID , ");
            strSql.Append(" updateBy = @updateBy , ");
            strSql.Append(" checkTime = @checkTime , ");
            strSql.Append(" checkBy = @checkBy , ");
            strSql.Append(" typeID = @typeID , ");
            strSql.Append(" citeID = @citeID , ");
            strSql.Append(" title = @title , ");
            strSql.Append(" noteContent = @noteContent , ");
            strSql.Append(" status = @status , ");
            strSql.Append(" createTime = @createTime , ");
            strSql.Append(" createBy = @createBy , ");
            strSql.Append(" updateTime = @updateTime  ");
            strSql.Append(" where ID=@ID  ");

            SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@updateBy", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@checkTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@checkBy", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@typeID", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@citeID", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@title", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@noteContent", SqlDbType.NVarChar,500) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@createTime", SqlDbType.DateTime) ,            
                        new SqlParameter("@createBy", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@updateTime", SqlDbType.DateTime)             
              
            };

            parameters[0].Value = model.ID;
            parameters[1].Value = model.updateBy;
            parameters[2].Value = model.checkTime;
            parameters[3].Value = model.checkBy;
            parameters[4].Value = model.typeID;
            parameters[5].Value = model.citeID;
            parameters[6].Value = model.title;
            parameters[7].Value = model.noteContent;
            parameters[8].Value = model.status;
            parameters[9].Value = model.createTime;
            parameters[10].Value = model.createBy;
            parameters[11].Value = model.updateTime;
            int rows = SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tbNote ");
            strSql.Append(" where ID=@ID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.NVarChar,50)			};
            parameters[0].Value = ID;


            int rows = SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AchieveEntity.NotesEntity GetModel(string ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID, updateBy, checkTime, checkBy, typeID, citeID, title, noteContent, status, createTime, createBy, updateTime  ");
            strSql.Append("  from tbNote ");
            strSql.Append(" where ID=@ID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.NVarChar,50)			};
            parameters[0].Value = ID;


           NotesEntity model = new NotesEntity();
            DataSet ds =  SqlHelper.GetDataset(strSql.ToString(), parameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                model.ID = ds.Tables[0].Rows[0]["ID"].ToString();
                model.updateBy = ds.Tables[0].Rows[0]["updateBy"].ToString();
                if (ds.Tables[0].Rows[0]["checkTime"].ToString() != "")
                {
                    model.checkTime = DateTime.Parse(ds.Tables[0].Rows[0]["checkTime"].ToString());
                }
                model.checkBy = ds.Tables[0].Rows[0]["checkBy"].ToString();
                model.typeID = ds.Tables[0].Rows[0]["typeID"].ToString();
                model.citeID = ds.Tables[0].Rows[0]["citeID"].ToString();
                model.title = ds.Tables[0].Rows[0]["title"].ToString();
                model.noteContent = ds.Tables[0].Rows[0]["noteContent"].ToString();
                if (ds.Tables[0].Rows[0]["status"].ToString() != "")
                {
                    model.status = int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["createTime"].ToString() != "")
                {
                    model.createTime = DateTime.Parse(ds.Tables[0].Rows[0]["createTime"].ToString());
                }
                model.createBy = ds.Tables[0].Rows[0]["createBy"].ToString();
                if (ds.Tables[0].Rows[0]["updateTime"].ToString() != "")
                {
                    model.updateTime = DateTime.Parse(ds.Tables[0].Rows[0]["updateTime"].ToString());
                }

                return model;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM tbNote ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return SqlHelper.GetDataset(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * ");
            strSql.Append(" FROM tbNote ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return SqlHelper.GetDataset(strSql.ToString());
        }
    
    }
}
