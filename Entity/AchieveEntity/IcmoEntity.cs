﻿using AchieveCommon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AchieveEntity
{
   public class IcmoEntity
    {
       public IcmoEntity() { }
       public string FBillNo;//单号，即ficmono
    
       public bool hasChild//是否有子单
       {
           set { hasChild = value; }
           get { return childList.Count > 0; }
       }

       public DateTime docDate;//制单日期，FHEADSELFJ0178
       public int FStatus;//单据状态
       public string FStatusStr //单据状态
       {       
       get {
                    string x;
                switch (this.FStatus) {
                    case 0:
                        x = "计划";
                        break;
                    case 1:
                        x = "未完成";
                        break;
                    case 2:
                        x = "未完成";
                        break;
                    case 3:
                        x = "已完成";
                        break;
                    default:
                        x = this.FStatus.ToString();
                        break;
                }
                return x;       
       }}
       public bool fmrpclosed;//是否关闭
       public bool ppbomstatus;//是否领料
      
       public bool started;//是否未开工
       //public bool fmrpclosed;//是否关闭
       //public bool fmrpclosed;//是否关闭
       public string FName;//物料名称
       public string FModel;//规格型号
       public int FQty;//计划数量
       public int FCommitQty;//实际数量
       public int incomQty;//入库数量，fauxstockqty
       public int inProQty;//在制数量，FHEADSELFJ0182
       public int FStockQty;//库存数量
       public DateTime FPlanCommitDate;//计划开工日期
       public DateTime FPlanFinishDate;//计划完工日期
       public DateTime FCheckDate;//单据下达日期
       public DateTime FStartDate;//实际开工日期
       public DateTime FFinishDate;//实际完工日期

       public DateTime FEntrySelfz0374;//预排开工日期
       public DateTime FEntrySelfz0375;//预排完工日期

       public List<workOrder> workOrderList;
       public List<IcmoEntity> childList;
        public bool overdue         {//逾期        
        get 
        {
           // 获取所有子单
           //检查子单是否逾期
           //子单和父单均设置逾期
           //检查本单是否逾期           
           if (this.FPlanFinishDate < DateTime.Now)
           {
               return true;
           }
         
           foreach (IcmoEntity item in this.childList){
             if(item.overdue){
                      return true;
                        }
           
           } 
           return false;
           }}
     /// <summary>
     /// 根据主生产任务单构建整个生产任务单模型
     /// </summary>
        /// <param name="FBillNo">主生产任务单 单号</param>
       public void getEntityByFBillNo(string FBillNo){
           char[] key = { '-', '_' };
           int index = FBillNo.IndexOfAny(key);
           string mainFBillNo;
           if (index < 1)
           {
               mainFBillNo = FBillNo;
           }
           else
           {
               mainFBillNo = FBillNo.Substring(0, index);
           }           
           StringBuilder sbsql = new StringBuilder();
           sbsql.Append("select FBillNo,fbominterid,FHEADSELFJ0178,FStatus,fmrpclosed,FQty,FCommitQty,fauxstockqty,FPlanCommitDate,");
           sbsql.Append("FPlanFinishDate,FCheckDate,FStartDate,FFinishDate,FType,FWorkShop,ICMO.FItemID as FItemID,FName,FModel,FInterID,FHEADSELFJ0182,FHEADSELFJ0183 ");
           sbsql.Append("from ICMO left join t_ICITEMCORE on ICMO.FItemID=t_ICITEMCORE.FItemID where FBillNo='"+FBillNo+"'");
           DataTable dticmo = AchieveCommon.SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, sbsql.ToString(), null);
           if (dticmo.Rows.Count>0)
           {
                this.docDate = Convert.ToDateTime( dticmo.Rows[0]["FHEADSELFJ0178"]);
                this.FBillNo = FBillNo;
                this.FCheckDate = Convert.ToDateTime(dticmo.Rows[0]["FCheckDate"]);
                this.FCommitQty = Convert.ToInt32(dticmo.Rows[0]["FCommitQty"]);
                this.FFinishDate = Convert.ToDateTime(dticmo.Rows[0]["FFinishDate"]);
                this.FModel = Convert.ToString(dticmo.Rows[0]["FModel"]);
                this.FName = Convert.ToString(dticmo.Rows[0]["FName"]);
           }
          
       
       }

   }
           
       
        
    

   public class workOrder
   {
       public string ficmono;//所属任务单单号，即FBillNo
       public string FName;//物料名称
       public int FQTYPLAN;//计划数量
       public string fopernote;//工序名称
       public int fstatus;//单据状态
       public int FWorkCenterID;//工作中心id
       public string FWorkCenterName()
       {//工作中心 
       switch(FWorkCenterID){
           case 15293:
       return "制造部"; 
           case 16015:
        return "委外"; 
           default:
            return FWorkCenterID.ToString(); 

       }

       }
       
       DateTime FPlanCommitDate;//计划开工日期
       DateTime FPlanFinishDate;//计划完工日期
       DateTime FCheckDate;//单据下达日期
       DateTime FStartDate;//实际开工日期
       DateTime FFinishDate;//实际完工日期
       int ffinishtime;//运行时间

   }
}
