﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using AchieveCommon;
namespace AchieveEntity
{
	/// <summary>
	/// 类ActionLog。
	/// </summary>
	[Serializable]
	public partial class ActionLog
	{
		public ActionLog()
		{}
		#region Model
		private int _id;
		private string _actioncode;
		private string _actionname;
		private string _logmessage;
		private string _loginfo;
		private string _logtext;
		private string _updateby;
		private string _updateip;
		private DateTime? _updatetime;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string actionCode
		{
			set{ _actioncode=value;}
			get{return _actioncode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string actionName
		{
			set{ _actionname=value;}
			get{return _actionname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string logMessage
		{
			set{ _logmessage=value;}
			get{return _logmessage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string logInfo
		{
			set{ _loginfo=value;}
			get{return _loginfo;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string logText
		{
			set{ _logtext=value;}
			get{return _logtext;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdateIP
		{
			set{ _updateip=value;}
			get{return _updateip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdateTime
		{
			set{ _updatetime=value;}
			get{return _updatetime;}
		}
		#endregion Model


		#region  Method

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public ActionLog(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select id,actionCode,actionName,logMessage,logInfo,logText,UpdateBy,UpdateIP,UpdateTime ");
        //    strSql.Append(" FROM [tbActionLog] ");
        //    strSql.Append(" where id=@id ");
        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    DataSet ds=SqlHelper.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
        //        {
        //            this.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["actionCode"]!=null)
        //        {
        //            this.actionCode=ds.Tables[0].Rows[0]["actionCode"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["actionName"]!=null)
        //        {
        //            this.actionName=ds.Tables[0].Rows[0]["actionName"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["logMessage"]!=null)
        //        {
        //            this.logMessage=ds.Tables[0].Rows[0]["logMessage"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["logInfo"]!=null)
        //        {
        //            this.logInfo=ds.Tables[0].Rows[0]["logInfo"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["logText"]!=null)
        //        {
        //            this.logText=ds.Tables[0].Rows[0]["logText"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateBy"]!=null)
        //        {
        //            this.UpdateBy=ds.Tables[0].Rows[0]["UpdateBy"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateIP"]!=null)
        //        {
        //            this.UpdateIP=ds.Tables[0].Rows[0]["UpdateIP"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateTime"]!=null && ds.Tables[0].Rows[0]["UpdateTime"].ToString()!="")
        //        {
        //            this.UpdateTime=DateTime.Parse(ds.Tables[0].Rows[0]["UpdateTime"].ToString());
        //        }
        //    }
        //}

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        //public bool Exists()
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from [tbActionLog]");
        //    strSql.Append(" where id=@id ");

        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    return SqlHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into [tbActionLog] (");
			strSql.Append("actionCode,actionName,logMessage,logInfo,logText,UpdateBy,UpdateIP,UpdateTime)");
			strSql.Append(" values (");
			strSql.Append("@actionCode,@actionName,@logMessage,@logInfo,@logText,@UpdateBy,@UpdateIP,@UpdateTime)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@actionCode", SqlDbType.NVarChar,50),
					new SqlParameter("@actionName", SqlDbType.NVarChar,50),
					new SqlParameter("@logMessage", SqlDbType.NVarChar,500),
					new SqlParameter("@logInfo", SqlDbType.NVarChar,500),
					new SqlParameter("@logText", SqlDbType.NVarChar,2000),
					new SqlParameter("@UpdateBy", SqlDbType.NVarChar,50),
					new SqlParameter("@UpdateIP", SqlDbType.NChar,20),
					new SqlParameter("@UpdateTime", SqlDbType.DateTime)};
			parameters[0].Value = actionCode;
			parameters[1].Value = actionName;
			parameters[2].Value = logMessage;
			parameters[3].Value = logInfo;
			parameters[4].Value = logText;
			parameters[5].Value = UpdateBy;
			parameters[6].Value = UpdateIP;
			parameters[7].Value = UpdateTime;

            object obj = SqlHelper.ExecuteNonQuery(strSql.ToString(), parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        //public bool Update()
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("update [tbActionLog] set ");
        //    strSql.Append("actionCode=@actionCode,");
        //    strSql.Append("actionName=@actionName,");
        //    strSql.Append("logMessage=@logMessage,");
        //    strSql.Append("logInfo=@logInfo,");
        //    strSql.Append("logText=@logText,");
        //    strSql.Append("UpdateBy=@UpdateBy,");
        //    strSql.Append("UpdateIP=@UpdateIP,");
        //    strSql.Append("UpdateTime=@UpdateTime");
        //    strSql.Append(" where id=@id ");
        //    SqlParameter[] parameters = {
        //            new SqlParameter("@actionCode", SqlDbType.NVarChar,50),
        //            new SqlParameter("@actionName", SqlDbType.NVarChar,50),
        //            new SqlParameter("@logMessage", SqlDbType.NVarChar,500),
        //            new SqlParameter("@logInfo", SqlDbType.NVarChar,500),
        //            new SqlParameter("@logText", SqlDbType.NVarChar,2000),
        //            new SqlParameter("@UpdateBy", SqlDbType.NVarChar,50),
        //            new SqlParameter("@UpdateIP", SqlDbType.NChar,20),
        //            new SqlParameter("@UpdateTime", SqlDbType.DateTime),
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = actionCode;
        //    parameters[1].Value = actionName;
        //    parameters[2].Value = logMessage;
        //    parameters[3].Value = logInfo;
        //    parameters[4].Value = logText;
        //    parameters[5].Value = UpdateBy;
        //    parameters[6].Value = UpdateIP;
        //    parameters[7].Value = UpdateTime;
        //    parameters[8].Value = id;

        //    int rows=SqlHelper.ExecuteSql(strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from [tbActionLog] ");
        //    strSql.Append(" where id=@id ");
        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    int rows=SqlHelper.ExecuteSql(strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public void GetModel(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select id,actionCode,actionName,logMessage,logInfo,logText,UpdateBy,UpdateIP,UpdateTime ");
        //    strSql.Append(" FROM [tbActionLog] ");
        //    strSql.Append(" where id=@id ");
        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    DataSet ds=SqlHelper.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
        //        {
        //            this.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["actionCode"]!=null )
        //        {
        //            this.actionCode=ds.Tables[0].Rows[0]["actionCode"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["actionName"]!=null )
        //        {
        //            this.actionName=ds.Tables[0].Rows[0]["actionName"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["logMessage"]!=null )
        //        {
        //            this.logMessage=ds.Tables[0].Rows[0]["logMessage"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["logInfo"]!=null )
        //        {
        //            this.logInfo=ds.Tables[0].Rows[0]["logInfo"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["logText"]!=null )
        //        {
        //            this.logText=ds.Tables[0].Rows[0]["logText"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateBy"]!=null )
        //        {
        //            this.UpdateBy=ds.Tables[0].Rows[0]["UpdateBy"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateIP"]!=null )
        //        {
        //            this.UpdateIP=ds.Tables[0].Rows[0]["UpdateIP"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateTime"]!=null && ds.Tables[0].Rows[0]["UpdateTime"].ToString()!="")
        //        {
        //            this.UpdateTime=DateTime.Parse(ds.Tables[0].Rows[0]["UpdateTime"].ToString());
        //        }
        //    }
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select * ");
        //    strSql.Append(" FROM [tbActionLog] ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return SqlHelper.Query(strSql.ToString());
        //}

		#endregion  Method
	}
}

