﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using AchieveCommon;
 

namespace AchieveEntity
{
	/// <summary>
	/// 类tbUserLocation。
	/// </summary>
	[Serializable]
	public partial class UserLocationEntity
	{
		public UserLocationEntity()
		{}
		#region Model
		private int _id;
		private string _accountname;
		private string _realname;
		private int? _regioncode;
		private string _describe;
		private decimal? _lng;
		private decimal? _lat;
		private DateTime? _updatetime;
		private string _province;
		private string _city;
		private string _county;
		private bool _iscurrent;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string accountname
		{
			set{ _accountname=value;}
			get{return _accountname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string realname
		{
			set{ _realname=value;}
			get{return _realname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? regionCode
		{
			set{ _regioncode=value;}
			get{return _regioncode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string describe
		{
			set{ _describe=value;}
			get{return _describe;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? lng
		{
			set{ _lng=value;}
			get{return _lng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? lat
		{
			set{ _lat=value;}
			get{return _lat;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdateTime
		{
			set{ _updatetime=value;}
			get{return _updatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string province
		{
			set{ _province=value;}
			get{return _province;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string city
		{
			set{ _city=value;}
			get{return _city;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string county
		{
			set{ _county=value;}
			get{return _county;}
		}
		/// <summary>
		/// 是否当前位置
		/// </summary>
		public bool isCurrent
		{
			set{ _iscurrent=value;}
			get{return _iscurrent;}
		}
		#endregion Model


		#region  Method

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public UserLocationEntity(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,accountname,realname,regionCode,describe,lng,lat,UpdateTime,province,city,county,isCurrent ");
			strSql.Append(" FROM [tbUserLocation] ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = id;

			DataSet ds=SqlHelper.GetDataset(SqlHelper.connStr,CommandType.Text, strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					this.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["accountname"]!=null)
				{
					this.accountname=ds.Tables[0].Rows[0]["accountname"].ToString();
				}
				if(ds.Tables[0].Rows[0]["realname"]!=null)
				{
					this.realname=ds.Tables[0].Rows[0]["realname"].ToString();
				}
				if(ds.Tables[0].Rows[0]["regionCode"]!=null && ds.Tables[0].Rows[0]["regionCode"].ToString()!="")
				{
					this.regionCode=int.Parse(ds.Tables[0].Rows[0]["regionCode"].ToString());
				}
				if(ds.Tables[0].Rows[0]["describe"]!=null)
				{
					this.describe=ds.Tables[0].Rows[0]["describe"].ToString();
				}
				if(ds.Tables[0].Rows[0]["lng"]!=null && ds.Tables[0].Rows[0]["lng"].ToString()!="")
				{
					this.lng=decimal.Parse(ds.Tables[0].Rows[0]["lng"].ToString());
				}
				if(ds.Tables[0].Rows[0]["lat"]!=null && ds.Tables[0].Rows[0]["lat"].ToString()!="")
				{
					this.lat=decimal.Parse(ds.Tables[0].Rows[0]["lat"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdateTime"]!=null && ds.Tables[0].Rows[0]["UpdateTime"].ToString()!="")
				{
					this.UpdateTime=DateTime.Parse(ds.Tables[0].Rows[0]["UpdateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["province"]!=null)
				{
					this.province=ds.Tables[0].Rows[0]["province"].ToString();
				}
				if(ds.Tables[0].Rows[0]["city"]!=null)
				{
					this.city=ds.Tables[0].Rows[0]["city"].ToString();
				}
				if(ds.Tables[0].Rows[0]["county"]!=null)
				{
					this.county=ds.Tables[0].Rows[0]["county"].ToString();
				}
				if(ds.Tables[0].Rows[0]["isCurrent"]!=null && ds.Tables[0].Rows[0]["isCurrent"].ToString()!="")
				{
					if((ds.Tables[0].Rows[0]["isCurrent"].ToString()=="1")||(ds.Tables[0].Rows[0]["isCurrent"].ToString().ToLower()=="true"))
					{
						this.isCurrent=true;
					}
					else
					{
						this.isCurrent=false;
					}
				}

			}
		}
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists()
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from [tbUserLocation]");
        //    strSql.Append(" where id=@id ");

        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    return DbHelperSQL.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into [tbUserLocation] (");
			strSql.Append("accountname,realname,regionCode,describe,lng,lat,UpdateTime,province,city,county,isCurrent)");
			strSql.Append(" values (");
			strSql.Append("@accountname,@realname,@regionCode,@describe,@lng,@lat,@UpdateTime,@province,@city,@county,@isCurrent)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@accountname", SqlDbType.NVarChar,50),
					new SqlParameter("@realname", SqlDbType.NVarChar,50),
					new SqlParameter("@regionCode", SqlDbType.Int,4),
					new SqlParameter("@describe", SqlDbType.NVarChar,50),
					new SqlParameter("@lng", SqlDbType.Decimal,9),
					new SqlParameter("@lat", SqlDbType.Decimal,9),
					new SqlParameter("@UpdateTime", SqlDbType.DateTime),
					new SqlParameter("@province", SqlDbType.NVarChar,50),
					new SqlParameter("@city", SqlDbType.NVarChar,50),
					new SqlParameter("@county", SqlDbType.NVarChar,50),
					new SqlParameter("@isCurrent", SqlDbType.Bit,1)};
			parameters[0].Value = accountname;
			parameters[1].Value = realname;
			parameters[2].Value = regionCode;
			parameters[3].Value = describe;
			parameters[4].Value = lng;
			parameters[5].Value = lat;
			parameters[6].Value = UpdateTime;
			parameters[7].Value = province;
			parameters[8].Value = city;
			parameters[9].Value = county;
			parameters[10].Value = isCurrent;

            object obj = SqlHelper.ExecuteScalar(SqlHelper.connStr, CommandType.Text, strSql.ToString(), parameters);
               // DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update [tbUserLocation] set ");
			strSql.Append("accountname=@accountname,");
			strSql.Append("realname=@realname,");
			strSql.Append("regionCode=@regionCode,");
			strSql.Append("describe=@describe,");
			strSql.Append("lng=@lng,");
			strSql.Append("lat=@lat,");
			strSql.Append("UpdateTime=@UpdateTime,");
			strSql.Append("province=@province,");
			strSql.Append("city=@city,");
			strSql.Append("county=@county,");
			strSql.Append("isCurrent=@isCurrent");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@accountname", SqlDbType.NVarChar,50),
					new SqlParameter("@realname", SqlDbType.NVarChar,50),
					new SqlParameter("@regionCode", SqlDbType.Int,4),
					new SqlParameter("@describe", SqlDbType.NVarChar,50),
					new SqlParameter("@lng", SqlDbType.Decimal,9),
					new SqlParameter("@lat", SqlDbType.Decimal,9),
					new SqlParameter("@UpdateTime", SqlDbType.DateTime),
					new SqlParameter("@province", SqlDbType.NVarChar,50),
					new SqlParameter("@city", SqlDbType.NVarChar,50),
					new SqlParameter("@county", SqlDbType.NVarChar,50),
					new SqlParameter("@isCurrent", SqlDbType.Bit,1),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = accountname;
			parameters[1].Value = realname;
			parameters[2].Value = regionCode;
			parameters[3].Value = describe;
			parameters[4].Value = lng;
			parameters[5].Value = lat;
			parameters[6].Value = UpdateTime;
			parameters[7].Value = province;
			parameters[8].Value = city;
			parameters[9].Value = county;
			parameters[10].Value = isCurrent;
			parameters[11].Value = id;

            int rows = SqlHelper.ExecuteNonQuery(SqlHelper.connStr, CommandType.Text, strSql.ToString(), parameters);
                //DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from [tbUserLocation] ");
        //    strSql.Append(" where id=@id ");
        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public void GetModel(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select id,accountname,realname,regionCode,describe,lng,lat,UpdateTime,province,city,county,isCurrent ");
        //    strSql.Append(" FROM [tbUserLocation] ");
        //    strSql.Append(" where id=@id ");
        //    SqlParameter[] parameters = {
        //            new SqlParameter("@id", SqlDbType.Int,4)};
        //    parameters[0].Value = id;

        //    DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
        //        {
        //            this.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["accountname"]!=null )
        //        {
        //            this.accountname=ds.Tables[0].Rows[0]["accountname"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["realname"]!=null )
        //        {
        //            this.realname=ds.Tables[0].Rows[0]["realname"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["regionCode"]!=null && ds.Tables[0].Rows[0]["regionCode"].ToString()!="")
        //        {
        //            this.regionCode=int.Parse(ds.Tables[0].Rows[0]["regionCode"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["describe"]!=null )
        //        {
        //            this.describe=ds.Tables[0].Rows[0]["describe"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["lng"]!=null && ds.Tables[0].Rows[0]["lng"].ToString()!="")
        //        {
        //            this.lng=decimal.Parse(ds.Tables[0].Rows[0]["lng"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["lat"]!=null && ds.Tables[0].Rows[0]["lat"].ToString()!="")
        //        {
        //            this.lat=decimal.Parse(ds.Tables[0].Rows[0]["lat"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["UpdateTime"]!=null && ds.Tables[0].Rows[0]["UpdateTime"].ToString()!="")
        //        {
        //            this.UpdateTime=DateTime.Parse(ds.Tables[0].Rows[0]["UpdateTime"].ToString());
        //        }
        //        if(ds.Tables[0].Rows[0]["province"]!=null )
        //        {
        //            this.province=ds.Tables[0].Rows[0]["province"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["city"]!=null )
        //        {
        //            this.city=ds.Tables[0].Rows[0]["city"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["county"]!=null )
        //        {
        //            this.county=ds.Tables[0].Rows[0]["county"].ToString();
        //        }
        //        if(ds.Tables[0].Rows[0]["isCurrent"]!=null && ds.Tables[0].Rows[0]["isCurrent"].ToString()!="")
        //        {
        //            if((ds.Tables[0].Rows[0]["isCurrent"].ToString()=="1")||(ds.Tables[0].Rows[0]["isCurrent"].ToString().ToLower()=="true"))
        //            {
        //                this.isCurrent=true;
        //            }
        //            else
        //            {
        //                this.isCurrent=false;
        //            }
        //        }
        //    }
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select * ");
        //    strSql.Append(" FROM [tbUserLocation] ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return DbHelperSQL.Query(strSql.ToString());
        //}

		#endregion  Method
	}
}

