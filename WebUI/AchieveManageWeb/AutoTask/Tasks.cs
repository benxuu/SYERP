﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AchieveManageWeb.Models.ActionFilters;

namespace AchieveManageWeb.AutoTask
{
    /// <summary>
    /// 测试任务，每半小时执行一次,IntervalSeconds=600
    /// </summary>
    //[AutoTask(EnterMethod = "StartTask", IntervalSeconds = 1800, StartTime = "2020-12-28 10:45:00")]
    //public class TestTask
    //{
    //    public static void StartTask()
    //    {            
    //        LoggerHelper.Info("项目需要维护，请联系开发人员！"+DateTime.Now.ToString());
    //        System.Diagnostics.Debug.Write("\r\n项目需要维护，请联系开发人员！" + DateTime.Now.ToString());
    //        LoggerHelper.Info("项目需要维护，请联系开发人员！！");
    //    }
    //}

    /// <summary>
    /// 每天执行，检查项目，自动确认、任务自动下达
    /// </summary>
    [AutoTask(EnterMethod = "autoConfirmProject", IntervalSeconds = 86400, StartTime = "2019-07-10 02:00:00")]
    public class PMhandleTask
    {
        public static void autoConfirmProject()
        {
            try
            {
                    int x=AchieveBLL.ProjectBLL.autoConfirmProject();
                    string log="计划任务【项目自动确认下达】已执行完毕！共处理数据"+x+"条,完成时间："+ DateTime.Now.ToString();
                    LoggerHelper.Notes(new AchieveManageWeb.Models.LogContent("autoConfirmProject", log, "计划任务-项目自动确认下达"));
                       
                   // System.Diagnostics.Debug.Write("\r\n" + log);
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }
    }
}