﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using AchieveCommon;
using System.IO;
using System.Text;

namespace AchieveManageWeb.WebForms
{
    public partial class RptWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    loadReport();
            //}            
        }
        //private void loadReport()
        //{
        //    DataTable dt = new DataTable();
        //    string strSql = "select * from RPT01";

        //    dt=SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, strSql.ToString());
        //    ReportDataSource reportDataSource = new ReportDataSource("gslkDS", dt);
        //    ReportViewer1.LocalReport.DataSources.Clear();
        //    ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
        //    ReportViewer1.LocalReport.Refresh();
        //}
        //protected void SearchBtn_Click(object sender, EventArgs e)
        //{
        //    string xmlStr = EncryptHelper.AesDecrpt(this.xmlStr.Value);
        //    string tbJson = this.hidDataTable.Value;
        //    string rpName = this.rpName.Value;
        //    DataTable dt = new DataTable();
        //    if (!string.IsNullOrEmpty(tbJson) && tbJson != "[]")
        //    {
        //        dt = JSON.ToDataTable(tbJson);
        //    }
        //    reportViewer1.LocalReport.DataSources.Clear();
        //    reportViewer1.LocalReport.DisplayName = rpName;
        //    reportViewer1.LocalReport.LoadReportDefinition(GenerateRdlc(xmlStr));
        //    ReportDataSource reportDataSource = new ReportDataSource("DataSet1", dt);
        //    reportViewer1.LocalReport.DataSources.Add(reportDataSource);
        //    reportViewer1.LocalReport.Refresh();

        //}

        /// <summary>
        /// 以内存流形式返回rdlc报表配置信息
        /// </summary>
        /// <param name="inStr"></param>
        /// <returns></returns>
        public MemoryStream GenerateRdlc(string inStr)
        {
            byte[] b = Encoding.UTF8.GetBytes(inStr);
            MemoryStream ms = new MemoryStream(b);
            return ms;
        }
        protected void SearchBtn_Click(object sender, EventArgs e)
        {
            //string xmlStr = EncryptHelper.AesDecrpt(this.xmlStr.Value);
            string tbJson = this.hidDataTable.Value;
            string rpName = this.rpName.Value;
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(tbJson) && tbJson != "[]")
            {
                dt = JsonHelper.JsonToDataTable(tbJson);
            }
            //ReportParameter zzbfk = new ReportParameter("zzbfk", dt.Rows[27][2].ToString());
            //ReportParameter zhbfk = new ReportParameter("zhbfk", dt.Rows[28][2].ToString());
            //ReportParameter qtbmfk = new ReportParameter("qtbmfk", dt.Rows[29][2].ToString());
            //ReportParameter hj30 = new ReportParameter("hj30", dt.Rows[30][2].ToString());
            //ReportParameter clfk = new ReportParameter("qtbmfk", dt.Rows[31][2].ToString());
            //ReportParameter hjfk32 = new ReportParameter("hjfk32", dt.Rows[32][2].ToString());

           //for (int i = dt.Rows.Count-1; i > 26; i--)
           // {
           //     dt.Rows.RemoveAt(i);
           // }
            
          
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DisplayName = rpName;            
            //ReportViewer1.LocalReport.LoadReportDefinition(GenerateRdlc(xmlStr));
            ReportViewer1.LocalReport.ReportPath = @"WebForms\rptSYHZ.rdlc";
            ReportDataSource reportDataSource = new ReportDataSource("rptDS", dt);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
            ReportParameter rptitle = new ReportParameter("rptitle", rpName);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rptitle });
           // ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rptitle,zzbfk,zhbfk,qtbmfk,clfk,hj30,hjfk32 }); 
            ReportViewer1.LocalReport.Refresh();

        }


    }
}
