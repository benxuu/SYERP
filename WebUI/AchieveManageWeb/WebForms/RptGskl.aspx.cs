﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using AchieveCommon;
using System.IO;
using System.Text;

namespace AchieveManageWeb.WebForms
{
    public partial class RptGskl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    loadReport();
            //}            
        }
        //private void loadReport()
        //{
        //    DataTable dt = new DataTable();
        //    string strSql = "select * from RPT01";

        //    dt=SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, strSql.ToString());
        //    ReportDataSource reportDataSource = new ReportDataSource("gslkDS", dt);
        //    ReportViewer1.LocalReport.DataSources.Clear();
        //    ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
        //    ReportViewer1.LocalReport.Refresh();
        //}
        //protected void SearchBtn_Click(object sender, EventArgs e)
        //{
        //    string xmlStr = EncryptHelper.AesDecrpt(this.xmlStr.Value);
        //    string tbJson = this.hidDataTable.Value;
        //    string rpName = this.rpName.Value;
        //    DataTable dt = new DataTable();
        //    if (!string.IsNullOrEmpty(tbJson) && tbJson != "[]")
        //    {
        //        dt = JSON.ToDataTable(tbJson);
        //    }
        //    reportViewer1.LocalReport.DataSources.Clear();
        //    reportViewer1.LocalReport.DisplayName = rpName;
        //    reportViewer1.LocalReport.LoadReportDefinition(GenerateRdlc(xmlStr));
        //    ReportDataSource reportDataSource = new ReportDataSource("DataSet1", dt);
        //    reportViewer1.LocalReport.DataSources.Add(reportDataSource);
        //    reportViewer1.LocalReport.Refresh();

        //}

        /// <summary>
        /// 以内存流形式返回rdlc报表配置信息
        /// </summary>
        /// <param name="inStr"></param>
        /// <returns></returns>
        public MemoryStream GenerateRdlc(string inStr)
        {
            byte[] b = Encoding.UTF8.GetBytes(inStr);
            MemoryStream ms = new MemoryStream(b);
            return ms;
        }
        protected void SearchBtn_Click(object sender, EventArgs e)
        {
            //string xmlStr = EncryptHelper.AesDecrpt(this.xmlStr.Value);
            string tbJson = this.hidDataTable.Value;
            string rpName = this.rpName.Value;
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(tbJson) && tbJson != "[]")
            {
                dt = JsonHelper.JsonToDataTable(tbJson);
            }           
          
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DisplayName = rpName;            
            //ReportViewer1.LocalReport.LoadReportDefinition(GenerateRdlc(xmlStr));
            ReportViewer1.LocalReport.ReportPath = @"WebForms\rpt.rdlc";
            ReportDataSource reportDataSource = new ReportDataSource("gsklDS", dt);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
            ReportParameter rptitle = new ReportParameter("rptitle", rpName);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rptitle }); 
            ReportViewer1.LocalReport.Refresh();

        }


    }
}
