﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptGskl.aspx.cs" Inherits="AchieveManageWeb.WebForms.RptGskl" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #form1 {            height: 900px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="800px" Width="1350px" style="margin-right: 0px">
        </rsweb:ReportViewer>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>      
         <asp:HiddenField ID="rpName" runat="server" />
        <asp:HiddenField ID="xmlStr" runat="server" />
        <asp:HiddenField ID="hidDataTable" runat="server" />
        <div style="display: none">
            <asp:Button ID="SearchBtn" runat="server" OnClick="SearchBtn_Click" Text="查询" />
        </div>
    </form>
</body>
</html>
