﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using AchieveCommon;
using System.IO;
using System.Text;

namespace AchieveManageWeb.WebForms
{
    public partial class rpt1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadReport();
            }            
        }

        private void loadReport()
        {
             
            //obtain the query para
          //  string RptName = Request["RptName"] == null ? "" : Request["RptName"];
            string RptName = "gskl";
            string deptTb = string.Empty;
            switch (RptName)
            {
                case "gskl"://钢水控流
                    deptTb = "rpt01";
                    break;
                case "yjgc"://冶金工程
                    deptTb = "rpt02";
                    break;
                case "nyhj":
                    deptTb = "rpt03";//能源环境
                    break;
                case "dljt":
                    deptTb = "rpt06";//电缆卷筒
                    break;
                case "zzb"://制造部
                    deptTb = "rpt05";
                    break;

                default:
                    break;

            }

            DataTable dt = new DataTable();
            string strSql = @"select rowid=30, 科目,isNull(一月,0)as 一月,isNull(二月,0)as 二月,isNull(三月,0)as 三月,isNull(四月,0)as 四月,
                            isNull(五月,0)as 五月,isNull(六月,0)as 六月,isNull(七月,0)as 七月,isNull(八月,0)as 八月,isNull(九月,0)as 九月,
                                isNull(十月,0)as 十月,isNull(十一月,0)as 十一月,isNull(十二月,0)as 十二月 from " + deptTb;

            dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, strSql.ToString());

            #region 初始化报表数据视图
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i]["科目"].ToString().Trim())
                {
                    case "开票金额":
                        dt.Rows[i]["rowid"] = 0;
                        break;
                    case "回款金额":
                        dt.Rows[i]["rowid"] = 1;
                        break;
                    case "发货金额":
                        dt.Rows[i]["rowid"] = 2;
                        break;
                    case "直接材料":
                        dt.Rows[i]["rowid"] = 4;
                        break;
                    case "直接人工":
                        dt.Rows[i]["rowid"] = 5;
                        break;
                    case "易耗品":
                        dt.Rows[i]["rowid"] = 6;
                        break;
                    case "运输费":
                        dt.Rows[i]["rowid"] = 7;
                        break;
                    case "外协费":
                        dt.Rows[i]["rowid"] = 8;
                        break;
                    case "部门工资":
                        dt.Rows[i]["rowid"] = 12;
                        break;
                    case "办公费":
                        dt.Rows[i]["rowid"] = 13;
                        break;
                    case "差旅费":
                        dt.Rows[i]["rowid"] = 14;
                        break;
                    case "招待费":
                        dt.Rows[i]["rowid"] = 15;
                        break;
                    case "研发费":
                        dt.Rows[i]["rowid"] = 21;
                        break;
                    case "退款金额":
                        dt.Rows[i]["rowid"] = 22;
                        break;
                    case "回款退款":
                        if (RptName == "zzb")
                        {
                            dt.Rows[i]["rowid"] = 22;
                        }
                        break;
                    case "付款金额":
                        dt.Rows[i]["rowid"] = 23;
                        break;
                    default:
                        // dt.Rows[i]["rowid"] =30;
                        break;
                }
            }

            if (RptName == "zzb")//制造部需分别合并两项办公费、两项差旅费
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["rowid"]) == 13)//办公费
                    {
                        for (int j = i + 1; j < dt.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(dt.Rows[j]["rowid"]) == 13)//第二个办公费
                            {
                                //累加到第一行
                                for (int k = 2; k < 14; k++)
                                {
                                    dt.Rows[i][k] = (decimal)dt.Rows[i][k] + (decimal)dt.Rows[j][k];
                                }
                                //删除第二行
                                dt.Rows.RemoveAt(j);
                                break;
                            }
                        }
                        break;
                    }
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["rowid"]) == 14)//差旅费
                    {
                        for (int j = i + 1; j < dt.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(dt.Rows[j]["rowid"]) == 14)//第二个差旅费
                            {
                                //累加到第一行
                                for (int k = 2; k < 14; k++)
                                {
                                    dt.Rows[i][k] = (decimal)dt.Rows[i][k] + (decimal)dt.Rows[j][k];
                                }
                                //删除第二行
                                dt.Rows.RemoveAt(j);
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            dt.Rows.Add(3, "销售金额");
            dt.Rows.Add(9, "制造费用");
            dt.Rows.Add(10, "项目利润");
            dt.Rows.Add(11, "项目利润率");
            dt.Rows.Add(16, "部门费用");
            dt.Rows.Add(17, "研发费用");
            if (RptName == "gskl" | RptName == "yjgc" | RptName == "nyhj")//三个需要计算公摊费用的部门
            {
                string sql = "select 18 as rowid,'公摊费用' as 科目, sum(一月)/3 as 一月,sum(二月)/3 as 二月,sum(三月)/3 as 三月,sum(四月)/3 as 四月,sum(五月)/3 as 五月,sum(六月)/3 as 六月,sum(七月)/3 as 七月,sum(八月)/3 as 八月,sum(九月)/3 as 九月,sum(十月)/3 as 十月,sum(十一月)/3 as 十一月,sum(十二月)/3 as 十二月 from rpt04 WHERE 科目 not in('综合部付款','补贴收入','营业外收入')";
                DataTable dtf = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql);
                dt.Rows.Add(dtf.Rows[0].ItemArray);
            }
            else
            {
                dt.Rows.Add(18, "公摊费用", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            }

            dt.Rows.Add(19, "利润");
            dt.Rows.Add(20, "利润率");

            DataView dataView = dt.DefaultView;//利用视图排序
            dataView.Sort = "rowid asc";
            dt = dataView.ToTable();//排序完毕
            dt.Columns.Add("全年", typeof(Decimal));//添加全年统计列

            #endregion

            #region 报表月数据公式计算
            for (int i = 2; i < 14; i++)//选择列，0：id；1：科目；2：一月...
            {
                #region//计算研发比率和订单比例
                decimal yfRate = 0;
                decimal ddRate = 1;
                try
                {
                    decimal rate = Convert.ToDecimal(dt.Rows[21][i]) / (Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[6][i]));//   研发费/(直接材料+易耗品)>1 

                    if (rate > 1)
                    {
                        yfRate = 1;
                    }
                    else
                    {
                        yfRate = rate;
                    }
                }
                catch (Exception)
                {

                    yfRate = 0;
                }
                ddRate = 1 - yfRate;
                #endregion
                //00计算原始制造费用，原始制造费用：=SUM(4:8)，仅用于研发费用计算！
                decimal ozzfy = Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[5][i]) + Convert.ToDecimal(dt.Rows[6][i]) + Convert.ToDecimal(dt.Rows[7][i]) + Convert.ToDecimal(dt.Rows[8][i]);
                //0，开票金额：原始数据
                //1，回款金额:回款金额+退款金额
                dt.Rows[1][i] = Convert.ToDecimal(dt.Rows[1][i]) + Convert.ToDecimal(dt.Rows[22][i]);
                //2,发货金额：发货金额/1.16
                dt.Rows[2][i] = Convert.ToDecimal(dt.Rows[2][i]) / 1.16m;
                //3,销售金额:
                dt.Rows[3][i] = Convert.ToDecimal(dt.Rows[0][i]) * 0.2m + Convert.ToDecimal(dt.Rows[1][i]) / 1.16m * 0.3m + Convert.ToDecimal(dt.Rows[2][i]) * 0.5m;
                //4,直接材料:直接材料*订单比例
                dt.Rows[4][i] = Convert.ToDecimal(dt.Rows[4][i]) * ddRate;
                //5.直接人工:直接人工*订单比例
                dt.Rows[5][i] = Convert.ToDecimal(dt.Rows[5][i]) * ddRate;
                //6.易耗品:易耗品*订单比例
                dt.Rows[6][i] = Convert.ToDecimal(dt.Rows[6][i]) * ddRate;
                //7.运输费:运输费*订单比例
                dt.Rows[7][i] = Convert.ToDecimal(dt.Rows[7][i]) * ddRate;
                //8.外协费:外协费*订单比例
                dt.Rows[8][i] = Convert.ToDecimal(dt.Rows[8][i]) * ddRate;
                //9.制造费用：=SUM(4:8)
                dt.Rows[9][i] = Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[5][i]) + Convert.ToDecimal(dt.Rows[6][i]) + Convert.ToDecimal(dt.Rows[7][i]) + Convert.ToDecimal(dt.Rows[8][i]);
                //10.项目利润
                dt.Rows[10][i] = Convert.ToDecimal(dt.Rows[3][i]) - Convert.ToDecimal(dt.Rows[9][i]);
                //11.项目利润率
                try
                {
                    dt.Rows[11][i] = Convert.ToDecimal(dt.Rows[10][i]) / Convert.ToDecimal(dt.Rows[3][i]);
                }
                catch (Exception)
                {

                    dt.Rows[11][i] = 0;
                }
                //12：15.部门工资、办公费、差旅费、招待费，使用原始数据
                //16.部门费用，sum(12:15)
                dt.Rows[16][i] = Convert.ToDecimal(dt.Rows[12][i]) + Convert.ToDecimal(dt.Rows[13][i]) + Convert.ToDecimal(dt.Rows[14][i]) + Convert.ToDecimal(dt.Rows[15][i]);
                //17.研发费用，原始制造费用*研发比率 
                dt.Rows[17][i] = ozzfy * yfRate;

                //18 公摊费用,视图初始化中已加载
                //19 利润,
                dt.Rows[19][i] = Convert.ToDecimal(dt.Rows[10][i]) - Convert.ToDecimal(dt.Rows[16][i]) - Convert.ToDecimal(dt.Rows[17][i]) - Convert.ToDecimal(dt.Rows[18][i]);
                //20.计算利润率
                try
                {
                    dt.Rows[20][i] = Convert.ToDecimal(dt.Rows[19][i]) / Convert.ToDecimal(dt.Rows[3][i]);
                }
                catch (Exception)
                {

                    dt.Rows[20][i] = 0;
                }

            }

            #endregion

            # region  计算全年统计列

            for (int i = 0; i < 21; i++)//共20行
            {
                if (i == 11)//项目利润率
                {
                    decimal lrl = 0;

                    try
                    {
                        lrl = Convert.ToDecimal(dt.Rows[10]["全年"]) / Convert.ToDecimal(dt.Rows[3]["全年"]);
                    }
                    catch (Exception)
                    {

                        lrl = 0;
                    }
                    dt.Rows[i]["全年"] = lrl;
                }
                else if (i == 20)//利润率
                {
                    decimal lrl = 0;

                    try
                    {
                        lrl = Convert.ToDecimal(dt.Rows[19]["全年"]) / Convert.ToDecimal(dt.Rows[3]["全年"]);
                    }
                    catch (Exception)
                    {

                        lrl = 0;
                    }
                    dt.Rows[i]["全年"] = lrl;
                }
                else
                {
                    decimal sum = 0;

                    for (int j = 2; j < 14; j++)
                    {
                        sum += Convert.ToDecimal(dt.Rows[i][j]);
                    }
                    dt.Rows[i]["全年"] = sum;

                }

            }
            #endregion

            //删除不需要显示的行数据（科目），保留前21行
            for (int i = dt.Rows.Count - 1; i > 20; i--)
            {
                dt.Rows.RemoveAt(i);
            }


            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DisplayName = "前端显示测试报表";
            //ReportViewer1.LocalReport.LoadReportDefinition(GenerateRdlc(xmlStr));
            ReportViewer1.LocalReport.ReportPath = @"WebForms\rpt.rdlc";
            ReportDataSource reportDataSource = new ReportDataSource("gsklDS", dt);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
            ReportParameter rptitle = new ReportParameter("rptitle", "前端显示测试报表");
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rptitle });
            ReportViewer1.LocalReport.Refresh();
        }
    }
}