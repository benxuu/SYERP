﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net.Layout;
using log4net.Layout.Pattern;
using System.Reflection;
using System.Web.Mvc;
using AchieveCommon;

namespace AchieveManageWeb.Models
{
    public class LogPublicClass
    {
    }
    /// <summary>
    /// 包含了所有的自定字段属性
    /// </summary>
    public class LogContent
    {
        public LogContent(string userIP, string username, string actionsclick, string description, string logInfo = "")
        {
            UserIP = userIP;
            UserName = username;
            ActionsClick = actionsclick;
            LogText = description;
            LogInfo = logInfo;
        }
        public LogContent(string actionsclick, string description, string logInfo = "")
        {
            //UserIP = System.Web.HttpContext.Current.Session["IP"].ToString();
            //UserName = System.Web.HttpContext.Current.Session["userName"].ToString();
            UserID= AES.DecryptStr(CookiesHelper.GetCookieValue("UserID"));
            UserName = CookiesHelper.GetCookieValue("Username");
            string ip = Comm.Get_ClientIP();            
            if (string.IsNullOrEmpty(ip))
            {
                UserIP= "localhost";
            }
            else
            {
                UserIP = ip;
            }
            ActionsClick = actionsclick;
            LogText = description;
            LogInfo = logInfo;
        }
        public LogContent(ViewDataDictionary viewdata, string actionsclick, string description, string logInfo = "")
        {
            UserIP = viewdata["IP"].ToString();
            UserName = viewdata["AccountName"].ToString();
            //string AccountName = ViewData["AccountName"].ToString();
            //string RealName = ViewData["RealName"].ToString();
            ActionsClick = actionsclick;
            LogText = description;
            LogInfo = logInfo;
        }

        /// <summary>
        /// 访问IP
        /// </summary>
        public string UserIP { get; set; }
        public string UserID { get; set; }

        /// <summary>
        /// 系统登陆用户
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 动作事件
        /// </summary>
        public string ActionsClick { get; set; }

        /// <summary>
        /// 日志描述信息
        /// </summary>
        public string LogText { get; set; }
        /// <summary>
        /// 日志描述信息2
        /// </summary>
        public string LogInfo { get; set; }


    }

    public class MyLayout: PatternLayout
    {
        public MyLayout()
        {
            this.AddConverter("property", typeof(LogInfoPatternConverter));
        }
    }



    public class LogInfoPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (Option != null)
            {
                // Write the value for the specified key
                WriteObject(writer, loggingEvent.Repository, LookupProperty(Option, loggingEvent));
            }
            else
            {   // Write all the key value pairs
                WriteDictionary(writer, loggingEvent.Repository, loggingEvent.GetProperties());
            }
        }

        /// <summary>
        /// 通过反射获取传入的日志对象的某个属性的值
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private object LookupProperty(string property, log4net.Core.LoggingEvent loggingEvent)
        {
            object propertyValue = string.Empty;
            PropertyInfo propertyInfo = loggingEvent.MessageObject.GetType().GetProperty(property);
            if (propertyInfo != null)
                propertyValue = propertyInfo.GetValue(loggingEvent.MessageObject, null);
            return propertyValue;
        }

    }
}