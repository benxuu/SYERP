﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AchieveManageWeb.Models.ActionFilters
{
    public class LoggerHelper
    {
        static readonly log4net.ILog loginfo = log4net.LogManager.GetLogger("loginfo");      
        static readonly log4net.ILog logerror = log4net.LogManager.GetLogger("logerror");
        static readonly log4net.ILog logmonitor = log4net.LogManager.GetLogger("logmonitor");
      

        static readonly log4net.ILog logInfo = log4net.LogManager.GetLogger("logInfo");
        static readonly log4net.ILog logError = log4net.LogManager.GetLogger("logError");
        static readonly log4net.ILog logMonitor = log4net.LogManager.GetLogger("logMonitor");
        static readonly log4net.ILog logNotes = log4net.LogManager.GetLogger("logNote");//系统记录日志到数据库使用这个！
         static readonly log4net.ILog logDB = log4net.LogManager.GetLogger("logdb");//系统仅记录日志到数据库使用这个！
 
        
 
        
        public static void Error(string ErrorMsg, Exception ex = null)
        {
            if (ex != null)
            {
                logerror.Error(ErrorMsg, ex);
            }
            else
            {
                logerror.Error(ErrorMsg);
            }
        }            

        public static void Info(string Msg)
        {
            loginfo.Info(Msg);
        }

        public static void Monitor(string Msg)
        {
            logmonitor.Info(Msg);
        }

        public static void ErrorAdo(string ErrorMsg, Exception ex = null)
        {
            if (ex != null)
            {
                logError.Error(ErrorMsg, ex);
            }
            else
            {
                logError.Error(ErrorMsg);
            }
        } 
        public static void MonitorAdo(string Msg)
        {
            logMonitor.Info(Msg);
        }
        public static void InfoAdo(string Msg)
        {
            logInfo.Info(Msg);
        }

        public static void Notes(LogContent lc)
        {
            logNotes.Info(lc);
        }

        public static void DBonly(LogContent lc)
        {
            logDB.Info(lc);
        }    
    }
}