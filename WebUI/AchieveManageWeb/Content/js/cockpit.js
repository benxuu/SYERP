﻿//列显示样式处理函数
function valuedisplay(value, row, index) {
    a = parseFloat(value).toFixed(1);
    p = value * 100 / (row.DayTime * 6) > 100 ? 100 : value * 100 / (row.DayTime * 6);
    if (value > row.AlertValue * row.DayTime * 6 / 100) {
        return '<div style="width:' + p + '%;height:20px;  line-height: 22px;  font-size: 22px; position:relative;background:red; "> <span style="text-align:right; width:75px; position:absolute; ">' + a + '</span></div>';
    } else {
        return '<div style="width:' + p + '%;height:20px; line-height: 22px;  font-size: 22px; position:relative;background:green; "> <span style="text-align:right; width:75px; position:absolute; ">' + a + '</span></div>';
    }
}
//月视图列显示样式处理函数
function monthvaluedisplay(value, row, index) {
    a = parseFloat(value).toFixed(1);
    p = value * 100 / (row.DayTime * 25) > 100 ? 100 : value * 100 / (row.DayTime * 25);
    if (value > row.AlertValue * row.DayTime * 6 / 100) {
        return '<div style="width:' + p + '%;height:20px;  line-height: 22px;  font-size: 22px; position:relative;background:red; "> <span style="text-align:right; width:75px; position:absolute; ">' + a + '</span></div>';
    } else {
        return '<div style="width:' + p + '%;height:20px; line-height: 22px; font-size: 22px; position:relative;background:yellow; "> <span style="text-align:right; width:75px; position:absolute; ">' + a + '</span></div>';
    }
}

//预定义week列格式
var weekColums = [
         { field: 'OperGroupID', title: 'Code', width: 80, hidden: true },
           {
               field: 'OperGroupName', title: '工艺', width: 60, styler: function (value, row, index) {
                   if (value) {
                       return 'font-size: 22px;';
                       // the function can return predefined css class and inline style
                       // return {class:'c1',style:'color:red'}
                   }
               }},
            { field: 'AlertValue', title: '预警%', width: 50 },
             {
                 field: 'DayTime', title: '周能力', width: 50,
                 formatter: function (value, row, index) { return value * 6; }
             },
                 {
                     field: '1', title: '1', width: 80,
                     formatter: function (value, row, index) {
                         return valuedisplay(value, row, index);
                     }
                                  },
              {
                  field: '2', title: '2', width: 80,
                  formatter: function(value,row,index){
                     // return parseFloat(value).toFixed(1);
                      return valuedisplay(value, row, index);
                  }, styler: function (value, row, index) {
                      if (value) {
                          return 'font-size: 22px;'; 
                      }
                  }
              },
              {
                  field: '3', title: '3', width: 80,
                  formatter: function (value, row, index) {
                      return valuedisplay(value, row, index);
                  }, styler: function (value, row, index) {
                      if (value) {
                          return 'font-size: 22px;';
                      }
                  }
              },
            {
                field: '4', title: '4', width: 80, 
                formatter: function (value, row, index) {
                    return valuedisplay(value, row, index);
                }, styler: function (value, row, index) {
                    if (value) {
                        return 'font-size: 22px;';
                    }
                }
            },
            {
                field: '5', title: '5', width: 80, 
                formatter: function (value, row, index) {
                    return valuedisplay(value, row, index);
                }, styler: function (value, row, index) {
                    if (value) {
                        return 'font-size: 22px;';
                    }
                }
            }
];

//预定义month列格式
var monthColums = [
         { field: 'OperGroupID', title: 'Code', width: 80, hidden: true },
           {
               field: 'OperGroupName', title: '工艺', width: 60, styler: function (value, row, index) {
                   if (value) {
                       return 'font-size: 22px;';
                   }
               }
           },
            {
                field: 'AlertValue', title: '预警%', width: 50, styler: function (value, row, index) {
                    if (value) {
                        return 'font-size: 22px;';
                    }
                }
            },
             {
                 field: 'DayTime', title: '月能力', width: 50,
                 formatter: function (value, row, index) { return value * 25; }, styler: function (value, row, index) {
              
                         return 'font-size: 22px;';
                 
                 }
             },
                 {
                     field: '1', title: '1', width: 80,
                     formatter: function (value, row, index) {
                         return monthvaluedisplay(value, row, index);
                     }, styler: function (value, row, index) {
                       
                             return 'font-size: 22px;';
                        
                     }
                 },
              {
                  field: '2', title: '2', width: 80,
                  formatter: function (value, row, index) {
                      // return parseFloat(value).toFixed(1);
                      return monthvaluedisplay(value, row, index);
                  } , styler: function (value, row, index) {
         
                          return 'font-size: 22px;'; 
 
}
                 
              },
              {
                  field: '3', title: '3', width: 80,
                  formatter: function (value, row, index) {
                      return monthvaluedisplay(value, row, index);
                  }, styler: function (value, row, index) {
                   
                          return 'font-size: 22px;';
                   
                  }
              },
            {
                field: '4', title: '4', width: 80,
                formatter: function (value, row, index) {
                    return monthvaluedisplay(value, row, index);
                }, styler: function (value, row, index) {
                 
                        return 'font-size: 22px;';
                  
                }
            },
            {
                field: '5', title: '5', width: 80,
                formatter: function (value, row, index) {
                    return monthvaluedisplay(value, row, index);
                }, styler: function (value, row, index) {
              
                        return 'font-size: 22px;';
                   
                }
            }
];

var weekadjust = -1;//定义距离本周前后n周的值；

function lastweek() {
    weekadjust--;
    weekdatagrid(weekadjust);

}

function nextweek() {
    weekadjust++;
    weekdatagrid(weekadjust);

}

function weekdatagrid(weekadjust) {   
    $.ajax({     //请求当前数据的列名
        url: "/Home/GetOperWeekAlert?r=" + Math.random(),
        type: "post",
        data: { "key": "columns", "weekadjust": weekadjust },
        dataType: "json",
        timeout: 5000,
        success: function (data) {
            //if (data.success) { 
            //动态更新Colums
            for (var i = 4; i < 9; i++) {
                weekColums[i].title = data.columns[i - 1].title;
                weekColums[i].field = data.columns[i - 1].field;
            };

            $("#weektime_dg").datagrid({
                url: "/Home/GetOperWeekAlert?key=datarows",
                queryParams: {
                    key: 'datarows',
                    weekadjust: weekadjust
                },
                striped: true, rownumbers: true, pagination: false, pageSize: 20, singleSelect: true,
                fitColumns: false,
                //idField: '',
                //sortName: '',
                //sortOrder: 'desc',
                pageList: [20, 40, 60, 80, 100],
                //frozenColumns: [],
                columns: [weekColums],
                //禁止行选中
                onClickRow: function () {
                    $('#weektime_dg').datagrid('clearSelections');
                },
                toolbar: [{
                    text: '上一周',
                    iconCls: 'icon-arrow_left',
                    handler: function () { lastweek() }
                }, {
                    text: '下一周',
                    iconCls: 'icon-arrow_right',                    
                    handler: function () { nextweek() }
                }]
            });

            //} else {
            //    $.show_alert("错误", data.result);
            //}
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                $.show_alert("提示", "请求超时，请刷新当前页重试！");
            }
            else {
                $.show_alert("错误", textStatus + "：" + errorThrown);
            }
        }
    })
}




//月产能预警monthtime_dg
var monthadjust = -1;//定义距离本月前后n月的值；

function lastmonth() {
    monthadjust--;
    monthdatagrid(monthadjust);

}

function nextmonth() {
    monthadjust++;
    monthdatagrid(monthadjust);
}

function monthdatagrid(monthadjust) {

    $.ajax({     //请求当前数据的列名
        url: "/Home/GetOperMonthAlert?r=" + Math.random(),
        type: "post",
        data: { "key": "columns", "monthadjust": monthadjust, "useoper": "false" },//useoper:false,不分工时
        dataType: "json",
        timeout: 5000,
        success: function (data) {
            //动态更新Colums
            for (var i = 4; i < 9; i++) {
                monthColums[i].title = data.columns[i - 1].title;
                monthColums[i].field = data.columns[i - 1].field;
            };
            //获取数据
            $("#monthtime_dg").datagrid({
                url: "/Home/GetOperMonthAlert?key=datarows",
                queryParams: {
                    key: 'datarows',
                    monthadjust: monthadjust
                },
                striped: true, rownumbers: true, pagination: false, pageSize: 20, singleSelect: true,
                fitColumns: true, fit: true,//自适应
                //idField: '',
                //sortName: '',
                //sortOrder: 'desc',
                pageList: [20, 40, 60, 80, 100],
                //frozenColumns: [],
                columns: [monthColums],
                //禁止行选中
                onClickRow: function () {
                    $('#monthtime_dg').datagrid('clearSelections');
                },
                onLoadSuccess: function () {
                   
                },
                toolbar: [{
                    text: '上一月',
                    iconCls: 'icon-arrow_left',
                    handler: function () { lastmonth() }
                }, {
                    text: '下一月',
                    iconCls: 'icon-arrow_right',
                    handler: function () { nextmonth() }
                }]
            });

            //} else {
            //    $.show_alert("错误", data.result);
            //}
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                $.show_alert("提示", "请求超时，请刷新当前页重试！");
            }
            else {
                $.show_alert("错误", textStatus + "：" + errorThrown);
            }
        }
    })
}
$(function () {
  
   // $('.panel-title ').css('font-size', '18px'); //标题
  //  $('.datagrid-pager').css('display', 'none'); //分页工具栏
    weekdatagrid(weekadjust);
    monthdatagrid(monthadjust);

    $('.datagrid-cell').css('font-size', '18px'); //更改的是datagrid中的数据
    $('.datagrid-header .datagrid-cell span ').css('font-size', '19px'); //datagrid中的列名称

})


