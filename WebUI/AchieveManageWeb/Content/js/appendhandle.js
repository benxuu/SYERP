﻿function uploadfile(appendlistid, appenddatagrid) {
    $("<div/>").dialog({
        id: "ui_pm_upload_dialog",
        href: "/Project/uploadform",
        title: "上传附件",
        height: 270,
        width: 510,
        modal: true,
        buttons: [{
            id: "ui_file_add_btn",
            text: '添 加',
            handler: function () {
                //获取上传文件控件内容
                var file = $("#fileImport").filebox("files")[0];
                //判断控件中是否存在文件内容，如果不存在，弹出提示信息，阻止进一步操作
                if (file == null) { alert('错误，请选择文件'); return; }
                //获取文件名称
                var fileName = file.name;
                //获取文件类型名称
                var file_typename = fileName.substring(fileName.lastIndexOf('.'), fileName.length);
                //这里限定上传文件文件类型必须为.xlsx，如果文件类型不符，提示错误信息
                if (file_typename == '.xlsx' || true) {
                    //计算文件大小
                    var fileSize = 0;
                    //如果文件大小大于1024字节X1024字节，则显示文件大小单位为MB，否则为KB
                    if (file.size > 1024 * 1024) {
                        fileSize = Math.round(file.size * 100 / (1024 * 1024)) / 100;
                        if (fileSize > 10) {
                            alert('错误，文件超过10MB，禁止上传！'); return;
                        }
                        fileSize = fileSize.toString() + 'MB';
                    }
                    else {
                        fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
                    }
                    //将文件名和文件大小显示在前端label文本中
                    document.getElementById('fileName').innerHTML = "<span style='color:Blue'>文件名: " + file.name + ',大小: ' + fileSize + "</span>";
                    //获取form数据
                    var formData = new FormData($("#importFileForm")[0]);
                    //调用apicontroller后台action方法，将form数据传递给后台处理。contentType必须设置为false,否则chrome和firefox不兼容
                    $.ajax({
                        url: "/Project/uploadfile?appendlistid=" + appendlistid + "&r=" + Math.random(),
                        type: 'POST',
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (returnInfo) {
                            //上传成功后将控件内容清空，并显示上传成功信息
                            document.getElementById('fileImport').value = null;
                            document.getElementById('uploadInfo').innerHTML = "<span style='color:Red'>" + returnInfo + "</span>";
                            appenddatagrid.datagrid("reload");//刷新附件清单
                        },
                        error: function (returnInfo) {
                            //上传失败时显示上传失败信息
                            document.getElementById('uploadInfo').innerHTML = "<span style='color:Red'>" + returnInfo + "</span>";
                        }
                    });
                }
                else {
                    alert("文件类型错误");
                    //将错误信息显示在前端label文本中
                    document.getElementById('fileName').innerHTML = "<span style='color:Red'>错误提示:上传文件应该是.xlsx后缀而不应该是" + file_typename + ",请重新选择文件</span>"
                }
            }
        }, {
            text: '关 闭',
            handler: function () {
                $("#ui_pm_upload_dialog").dialog('destroy');

            }
        }],
        onLoad: function () {

        },
        onClose: function () {
            $("#ui_pm_upload_dialog").dialog('destroy');  //销毁dialog对象
        }
    });
}
//根据附件id删除附件，更新表格
function Delfile(appenddatagrid) {
    var rows = appenddatagrid.datagrid("getChecked");
    if (rows.length < 1) {
        $.show_alert("提示", "请先勾选要删除的项目");
        return;
    }
    if (rows.length > 1) {
        $.show_alert("提示", "不支持批量删除");
        appenddatagrid.datagrid('clearSelections').datagrid('clearChecked');
        return;
    }
    $.messager.confirm('提示', '确定删除：' + rows[0].AppendName + '？', function (r) {
        if (r) {
            $.ajax({
                url: "/Project/DelAppendByID?r=" + Math.random(),
                data: {
                    ID: rows[0].AppendID,
                    AppendPath: rows[0].AppendPath
                },
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $.show_alert("提示", data.msg);
                        $("#ui_pm_dg").datagrid("reload").datagrid('clearSelections').datagrid('clearChecked');
                        appenddatagrid.datagrid("reload");//刷新附件清单
                        ////如果“用户管理”标签页处于打开状态需要刷新，否则被删除的角色还显示，再点编辑用户就出错
                        //if ($('#tabs').tabs('exists', '用户管理')) {
                        //    $('#tabs').tabs('getTab', '用户管理').panel('refresh');
                        //}

                    } else {
                        $.show_alert("提示", data.msg);
                    }
                }
            });
        }
    });
}
function downloadfile(appenddatagrid) {
    var rows = appenddatagrid.datagrid("getChecked");
    if (rows.length < 1) {
        $.show_alert("提示", "请先勾选要下载的项目");
        return;
    }
    if (rows.length > 1) {
        $.show_alert("提示", "不支持批量处理");
        appenddatagrid.datagrid('clearSelections').datagrid('clearChecked');
        return;
    }
    // var path = "${ctx}/file/download.do?filePath=" + value ;
    window.open("../uploads/" + rows[0].AppendPath);
}