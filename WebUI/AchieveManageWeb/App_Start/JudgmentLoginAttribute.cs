﻿using AchieveCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AchieveManageWeb.App_Start
{
    /// <summary>
    /// 判断是否登录的过滤器
    /// </summary>
    public class JudgmentLoginAttribute : ActionFilterAttribute
    {
        public AchieveEntity.UserEntity accountmodelJudgment;

        //执行Action之前操作
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            //判断是否登录或是否用权限，如果有那么就进行相应的操作，否则跳转到登录页或者授权页面
            string s_accountId = AES.DecryptStr(CookiesHelper.GetCookieValue("UserID"));
          
            int i_accountId = 0;
            //判断是否有cookie
            if (int.TryParse(s_accountId, out i_accountId))
            {
                AchieveEntity.UserEntity m_account = new AchieveBLL.UserBLL().GetUserById(i_accountId.ToString());
                if (m_account != null)
                {
                    accountmodelJudgment = m_account;
                    filterContext.Controller.ViewData["Account"] = m_account;
                    filterContext.Controller.ViewData["AccountName"] = m_account.AccountName;
                    filterContext.Controller.ViewData["RealName"] = m_account.RealName;
                    filterContext.Controller.ViewData["userID"] = m_account.ID;
                   // filterContext.Controller.ViewData["roleID"] = m_account.ID;,角色ID，多个
                    string ip = Comm.Get_ClientIP();
                     if (string.IsNullOrEmpty(ip))
                    {
                       filterContext.Controller.ViewData["IP"] =  "localhost";
                    }
                    else
                    {
                        filterContext.Controller.ViewData["IP"] = ip;
                    }
                    
   #region //控制器的功能权限控制
                     //controller
                     var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                     //action
                     var actionName = filterContext.ActionDescriptor.ActionName;
                     //获取参数数组
                     var arrParameter = filterContext.ActionDescriptor.GetParameters();
                     // Console.WriteLine("weekNum:{0} --start:{1}--end:{2}", weekNum, weekStart.ToString(), weekEnd.ToString());
                     //System.Diagnostics.Debug.WriteLine("weekNum:{0} --start:{1}--end:{2}", weekNum, weekStart.ToString(), weekEnd.ToString());
                     System.Diagnostics.Debug.WriteLine("模拟权限控制，controllerName：{0}，actionName：{1}，accountname:{2}，accountID：{3}", controllerName, actionName, m_account.AccountName,m_account.ID);
                     //System.Diagnostics.Debug.WriteLine("controllerName：{0}", controllerName);
                     //System.Diagnostics.Debug.WriteLine("actionNamee：{0}", actionName);
                     ////System.Diagnostics.Debug.WriteLine("arrParametere：{0}", arrParameter);
                     //System.Diagnostics.Debug.WriteLine("AccountNamee：{0}", m_account.AccountName);
                    //对重要的action进行权限管控；
                     string sql = @" declare @aL int;
select @aL =accessLevel from tbAction  where Action = '{0}' and Controller='{1}'
if @aL=0 or @aL is null
begin
select 1
end
if @aL=1
begin
select count(*) from tbRoleAction a 
left join tbUserRole b on a.RoleId=b.RoleId
left join tbUser c on c.ID =b.UserId 
left join tbAction d on a.ActionId=d.id
where d.action = '{0}' and d.controller='{1}' and c.ID={2}
end
if @aL=2
begin
select 0
end";
 


                    sql = string.Format(sql,actionName,controllerName,m_account.ID);
                    int x = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.connStr, sql));
                    if (x==0)
                    {
                        filterContext.Result = new RedirectResult("/Validator/Error");
                    }
                      

    #endregion
                     //处理Action之前操作内容根据我们提供的规则来定义这部分内容
                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    CookiesHelper.AddCookie("UserID", System.DateTime.Now.AddDays(-1));
                    filterContext.Result = new RedirectResult("/Login/Index");
                }
            }
            else
            {
                filterContext.Result = new RedirectResult("/Login/Index");
            }
        }
    }
}