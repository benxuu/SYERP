﻿using AchieveBLL;
using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AchieveManageWeb.Controllers
{
    [AchieveManageWeb.App_Start.JudgmentLogin]
    public class RoleController : Controller
    {
        //
        // GET: /Role/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllRoleInfo()
        {
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "Id" : Request["sort"];
            string order = Request["order"] == null ? "asc" : Request["order"];

            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);

            if (!string.IsNullOrEmpty(Request["RoleName"]) && !SqlInjection.GetString(Request["RoleName"]))
            {
                strWhere += " and RoleName like '%" + Request["RoleName"] + "%'";
            }

            int totalCount;   //输出参数
            string strJson = new RoleBLL().GetPager("tbRole", "Id,RoleName,Description,CreateTime,CreateBy,UpdateTime,UpdateBy ", sort + " " + order, pagesize, pageindex, strWhere, out totalCount);
            var jsonResult = new { total = totalCount.ToString(), rows = strJson };
            return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        }

        /// <summary>
        /// 新增页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult RoleAdd()
        {
            return View();
        }

        /// <summary>
        /// 新增 角色
        /// </summary>
        /// <returns></returns>
        public ActionResult AddRole()
        {
            try
            {
                string rolename = Request["RoleName"];
                string description = Request["Description"];

                UserEntity uInfo = ViewData["Account"] as UserEntity;
                RoleEntity roleAdd = new RoleEntity();
                roleAdd.RoleName = rolename.Trim();
                roleAdd.Description = description.Trim();
                roleAdd.CreateBy = uInfo.AccountName;
                roleAdd.CreateTime = DateTime.Now;
                roleAdd.UpdateBy = uInfo.AccountName;
                roleAdd.UpdateTime = DateTime.Now;

                int roleId = new RoleBLL().AddRole(roleAdd);
                if (roleId > 0)
                {
                    return Content("{\"msg\":\"添加成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"添加失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"添加失败," + ex.Message + "\",\"success\":false}");
            }
        }

        /// <summary>
        /// 编辑页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult RoleEdit()
        {
            return View();
        }

        /// <summary>
        /// 编辑 用户
        /// </summary>
        /// <returns></returns>
        public ActionResult EditRole()
        {
            try
            {
                int id = Convert.ToInt32(Request["id"]);
                string originalName = Request["originalName"];
                string rolename = Request["RoleName"];
                string description = Request["Description"];
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                RoleEntity roleEdit = new RoleEntity();
                roleEdit.Id = id;
                roleEdit.RoleName = rolename.Trim();
                roleEdit.Description = description.Trim();
                roleEdit.UpdateBy = uInfo.AccountName;
                roleEdit.UpdateTime = DateTime.Now;
                if (new RoleBLL().EditRole(roleEdit, originalName))
                {
                    return Content("{\"msg\":\"修改成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"修改失败！\",\"success\":true}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }
        }

        public ActionResult DelRoleByIDs()
        {
            try
            {
                string Ids = Request["IDs"] == null ? "" : Request["IDs"];
                if (!string.IsNullOrEmpty(Ids))
                {
                    if (new RoleBLL().DeleteRole(int.Parse(Ids)))
                    {
                        return Content("{\"msg\":\"删除成功！\",\"success\":true}");
                    }
                    else
                    {
                        return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                    }
                }
                else
                {
                    return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"删除失败," + ex.Message + "\",\"success\":false}");
            }
        }

        /// <summary>
        /// 角色菜单权限 页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult RoleMenu()
        {
            return View();
        }
        /// <summary>
        /// 角色工艺权限 页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult RoleOper()
        {
            return View();
        }

        /// <summary>
        /// 新增 角色菜单权限
        /// </summary>
        /// <returns></returns>
        public ActionResult SetRoleMenu()
        {
            try
            {
                string menuIds = Request["menuIds"].Trim(',');
                int roleId = Convert.ToInt32(Request["roleId"]);
                if (new RoleBLL().SetRoleMenu(roleId, menuIds))
                {
                    return Content("{\"msg\":\"授权成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"授权失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"授权失败," + ex.Message + "\",\"success\":false}");
            }
        }

        /// <summary>
        /// 获取角色所属用户
        /// </summary>
        /// <returns></returns>
        public ActionResult GetRoleUserByRoleId()
        {
            int roleUserId = int.Parse(Request["roleId"]);
            string sortRoleUser = Request["sort"];  //排序列
            string orderRoleUser = Request["order"];  //排序方式 asc或者desc
            int pageindexRoleUser = int.Parse(Request["page"]);
            int pagesizeRoleUser = int.Parse(Request["rows"]);

            string strJsonRoleUser = new RoleBLL().GetPagerRoleUser(roleUserId, sortRoleUser + " " + orderRoleUser, pagesizeRoleUser, pageindexRoleUser);
            return Content(strJsonRoleUser);
        }

        /// <summary>
        /// 新增 角色菜单按钮权限
        /// </summary>
        /// <returns></returns>
        public ActionResult SetRoleMenuButton() 
        {
            try
            {
                string menuButtonId = Request["menuButtonId"].Trim(',');
                int roleId = Convert.ToInt32(Request["roleId"]);
                
                bool res = new RoleBLL().Authorize(roleId, menuButtonId); //new RoleBLL().Authorize(roleId, menuButtonId)
                if (res)
                {
                    return Content("{\"msg\":\"授权成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"授权失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"授权失败," + ex.Message + "\",\"success\":false}");
            }
        }
        /// <summary>
        /// 新增 角色工艺组权限
        /// </summary>
        /// <returns></returns>
        public ActionResult SetRoleOper()
        {
            try
            {
                string opergroupIds = Request["operIds"].Trim(',');//按工艺组处理
                int roleId = Convert.ToInt32(Request["roleId"]);
                string[] operIdslist = opergroupIds.Split(','); 
                List<string> sqllist = new List<string>();
                //先删除，后插入
                sqllist.Add(string.Format("delete tbRoleOpergroup where roleId={0};", roleId)); 
                foreach (string item in operIdslist)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        sqllist.Add(string.Format("insert into tbRoleOpergroup(RoleId,OpergroupId) values({0},{1});", roleId, Convert.ToInt32(item))); 
                    }
                }

                int id = 0;
                id = SqlHelper.ExecuteNonQuery(SqlHelper.connStr, sqllist);
                if (id > 0)
                {
                    return Content("{\"msg\":\"授权成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"授权失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"授权失败," + ex.Message + "\",\"success\":false}");
            }
        }
        /// <summary>
        /// 角色工艺树
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllRoleOperTree()
        {
            int roleid = Convert.ToInt32(Request["roleid"]);
            //StringBuilder sqlStr = new StringBuilder();
            string sqlStr = @"select r.RoleId roleid,g.OperGroupName opergroupname, g.OperGroupId OperGroupId, 
                                case when isnull(r.RoleId , 0) = 0 then 'false' else 'true' end checked
                                from tbOperGroup g 
                                left join tbRoleOpergroup r on(g.OperGroupId=r.OperGroupId and r.RoleId=@RoleId) 
                                where g.OperGroupId<2000
                                order by r.OperGroupId asc";
            //sqlStr.Append("select o.OperID operid,o.OperName opername,r.RoleId roleid,o.OperGroupID opergroupid,g.OperGroupName opergroupname, case when isnull(r.RoleId , 0) = 0 then 'false' else 'true' end checked");
            //sqlStr.Append(" from tbOper o");
            //sqlStr.Append(" left join tbRoleOper r on(o.OperID=r.OperId and r.RoleId=@RoleId)");
            //sqlStr.Append(" left join tbOperGroup g on(o.OperGroupId = g.OperGroupID)");
            //sqlStr.Append(" order by o.OperID asc");
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStr, CommandType.Text, sqlStr.ToString(), new SqlParameter("@RoleId", roleid));

            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            //DataRow[] rows = dt.Select("parentid > 0");
            if (dt.Rows.Count> 0)
            {
                //DataView dataView = new DataView(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("{\"id\":\"" + dt.Rows[i]["OperGroupId"].ToString() + "\",\"text\":\"" + dt.Rows[i]["opergroupname"].ToString() + "\",\"checked\":" + dt.Rows[i]["checked"].ToString() + "},");                  
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("]");
            }
            else
            {
                sb.Append("]");
            }

            return Content(sb.ToString());
        }

        /// <summary>
        /// 递归
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="menuid"></param>
        /// <returns></returns>
        //public string GetChildMenu(DataTable dt, string menuid, int roleId)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    DataRow[] r_list = dt.Select(string.Format("parentid={0}", menuid));
        //    if (r_list.Length > 0)
        //    {
        //        for (int j = 0; j < r_list.Length; j++)
        //        {
        //            DataRow[] child_list = dt.Select(string.Format("parentid={0}", r_list[j]["menuid"].ToString()));
        //            if (child_list.Length > 0)
        //            {
        //                sb.Append("{\"id\":\"" + r_list[j]["menuid"].ToString() + "\",\"text\":\"" + r_list[j]["menuname"].ToString() + "\",\"attributes\":{\"menuid\":\"" + r_list[j]["menuid"].ToString() + "\"},\"children\":[");
        //                sb.Append(GetChildMenu(dt, r_list[j]["menuid"].ToString(), roleId));
        //            }
        //            else
        //            {
        //                sb.Append("{\"id\":\"" + roleId + "\",\"text\":\"" + r_list[j]["menuname"].ToString() + "\",\"checked\":" + r_list[j]["checked"].ToString() + ",\"attributes\":{\"menuid\":\"" + r_list[j]["menuid"].ToString() + "\"}},");
        //            }
        //        }
        //        sb.Remove(sb.Length - 1, 1);
        //        sb.Append("]},");
        //    }
        //    else  //根节点下没有子节点
        //    {
        //        sb.Append("]},");  //跟上面if条件之外的字符串拼上
        //    }
        //    return sb.ToString();
        //}

    }
}
