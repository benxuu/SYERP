﻿using AchieveBLL;
using AchieveDAL;
using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using AchieveManageWeb.Models;
namespace AchieveManageWeb.Controllers
{
    [AchieveManageWeb.App_Start.JudgmentLogin]
    public class ProduceController : Controller
    {

        public  ActionResult FBillFinishRate()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "订单计划达成率")); //日志记录
            return View();
        }
        public ActionResult FBillFinishRate2()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "订单计划达成率")); //日志记录
            return View();
        }
        /// <summary>
        /// 查询生产任务单的订单计划达成率，查询生产任务计划达成率，icmo表，默认年份2020,
        /// </summary>
        /// <returns>返回0-12行数据，0表示全年截至当前日期，其余为月份；</returns>
        public ActionResult getFBillFinishRate()
        {
            int yearID = Request["yearID"] == null ? 2020 : int.Parse(Request["yearID"]);
            string type = Request["type"] == null ? "" : Request["type"];//includeDraw
            DataTable dt = manufactureBLL.getFBillFinishRate(type,yearID);
            string strJson = JsonHelper.ToJson(dt);
            string content;
            content = "{\"total\": " + 13 + ",\"rows\":" + strJson + "}";
            return Content(content); 
        }

        /// <summary>
        /// 制造部申请免考核量
        /// </summary>
        /// <returns></returns>
        public ActionResult updateDisCheckCount()
        {
            int yearID = Request["yearID"] == null ? 2020 : int.Parse(Request["yearID"]);
             int m1  = Request["m1"] == null ? 0 : int.Parse(Request["m1"]);
            int m2  = Request["m2"] == null ? 0 : int.Parse(Request["m2"]);
            int m3  = Request["m3"] == null ? 0 : int.Parse(Request["m3"]);
            int m4  = Request["m4"] == null ? 0 : int.Parse(Request["m4"]);
            int m5  = Request["m5"] == null ? 0 : int.Parse(Request["m5"]);
            int m6  = Request["m6"] == null ? 0 : int.Parse(Request["m6"]);
            int m7  = Request["m7"] == null ? 0 : int.Parse(Request["m7"]);
            int m8  = Request["m8"] == null ? 0 : int.Parse(Request["m8"]);
            int m9  = Request["m9"] == null ? 0 : int.Parse(Request["m9"]);
            int m10  = Request["m10"] == null ? 0 : int.Parse(Request["m10"]);
            int m11  = Request["m11"] == null ? 0 : int.Parse(Request["m11"]);
            int m12  = Request["m12"] == null ? 0 : int.Parse(Request["m12"]);
            string remark = Request["remark"] == null ? "" :Request["remark"];
            string sql = "update tbDisCheckOrder set m1={1},m2={2},m3={3},m4={4},m5={5},m6={6},m7={7},m8={8},m9={9},m10={10},m11={11},m12={12},remark='{13}' where type='disCheckCount' and year={0}";
            sql = string.Format(sql, yearID, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12,remark); 
            int id = SqlHelper.ExecuteNonQuery(SqlHelper.connStr, CommandType.Text,sql);
            if (id > 0)
            {
                return Content("{\"msg\":\"更新成功！\",\"success\":true}");
            }
            else
            {
                return Content("{\"msg\":\"更新失败！\",\"success\":false}");
            }
        }
           

        public ActionResult OperMgr()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "工艺组管理")); //日志记录
            return View();
        }

        /// <summary>
        /// 新增工序组操作
        /// </summary>
        /// <returns></returns>
        public ActionResult AddOperGroup()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                OperEntity oper = new OperEntity();
                int OperGroupID = Request["FoperID"] == null ? 0 : int.Parse(Request["FoperID"]);
                string UpdateTime = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                string UpdateBy = uInfo.AccountName;
                string OperGroupName = Request["OperNote"] == null ? "" : Request["OperNote"];
                float DayTime = Request["DayTime"] == null ? 0 : float.Parse(Request["DayTime"]);
                string Remark = Request["Remark"] == null ? "" : Request["Remark"];
                int AlertValue = Request["AlertValue"] == null ? 0 : int.Parse(Request["AlertValue"]);
                bool IsCheck = Request["IsCheck"] == null ? false : bool.Parse(Request["IsCheck"]);
                //oper.WeekTime = float.Parse(Request["WeekTime"]);
                //oper.MonthTime = float.Parse(Request["MonthTime"]);
                if (OperGroupID == 0)
                {
                    return Content("{\"msg\":\"添加失败,工艺组编码为空！\",\"success\":false}");
                }
                string sql = string.Format("insert into  tbOperGroup(OperGroupID,OperGroupName,DayTime,Remark,UpdateBy,UpdateTime,AlertValue,IsCheck) values ({0},'{1}','{2}','{3}','{4}', '{5}','{6}','{7}')", OperGroupID, OperGroupName, DayTime, Remark, UpdateBy, UpdateTime, AlertValue, IsCheck);

                int i = SqlHelper.ExecuteNonQuerySql(SqlHelper.connStr, sql);
                // int i = new AchieveBLL.ProduceBLL().AddOper(oper);
                //int operid = new AchieveDAL.ProduceDAL().AddOper(oper);
                if (i > 0)
                {
                    return Content("{\"msg\":\"添加成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"添加失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"添加失败," + ex.Message + "\",\"success\":false}");
            }
        }
        /// <summary>
        /// 新增工艺组页面展示
        /// </summary>
        /// <returns></returns>       

        public ActionResult OperGroupAdd()
        {
            return View();
        }

        public ActionResult test()
        {
            return View();
        }

        public ActionResult AlarmMoStart()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "开工预警")); //日志记录
            return View();
        }
        /// <summary>
        /// 加急订单--比例--视图
        /// </summary>
        /// <returns></returns>
        public ActionResult RushOrder()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "加急订单")); //日志记录
            return View();
        }

        /// <summary>
        /// 根据部门汇总首道工序未开工单据数量
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAlarmMo() {
          //  string sql = "select fheadselfz0324 as dept, count(b.S) as c from shworkbill a left join shworkbillentry b on a.FinterID =b.FinterID where a.FStatus<3 and b.S='Y' group by fheadselfz0324 order by dept desc";
            string sql = @"select isnull(fheadselfj0181,'其他') as dept, count(FBillNo) as c 
from icmo a where a.FStatus<3 and
a.FBillNo in(select distinct FEntrySelfz0373 from shworkbillentry c where c.S='Y')
group by fheadselfj0181 order by c desc";
            
            return Content(easyUiHelper.queryDatagridJson(sql,true));
        }

        /// <summary>
        /// 根据部门汇总加急任务单据数量
        /// </summary>
        /// <returns></returns>
        public ActionResult GetRushMo()
        {
            int year = Request["year"] == null ? 2019 : Convert.ToInt32(Request["year"]);
            DataTable dt = new DataTable();
            try
            {
                dt = ProduceBLL.GetRushMo(year);
            }
            catch (Exception)
            {
                
                throw;
            }
            string strJson = JsonHelper.ToJson(dt);
            string content = "{\"total\": " + dt.Rows.Count.ToString() + ",\"rows\":" + strJson + "}";
            return Content(content);
        }

       
        /// <summary>
        /// 车间作业管理view
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopJobMgr()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "车间作业管理")); //日志记录
            return View();
        }


        /// <summary>
        /// 获取工序计划表，输入FBillNo，ICMO.FBillNo==shworkbill.FICMONO，按是否紧急订单、计划完工日期升序排序
        /// GetShopJobList
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPageShopJob()
        { 
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "FPLANENDDATE" : Request["sort"];
            if (sort == "fcheckdate")
            {
                sort = "b.fcheckdate";
            }
            string order = Request["order"] == null ? "asc" : Request["order"];
            // string view = Request["view"] == null ? "" : Request["view"];

            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);//输出的数据页码
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);//每页输出数量
            string ficmono = Request["ficmono"] == null ? "" : Request["ficmono"];
            string FName = Request["FName"] == null ? "" : Request["FName"];
            //string isable = Request["isable"] == null ? "" : Request["isable"];
            //已领料
            bool YiLingliao = Request["YiLingliao"] == null ? false : Convert.ToBoolean(Request["YiLingliao"]);
            if (YiLingliao)
            {
                strWhere += " and e.FStatus=1 ";
            }
            //查询条件：仅本周以前
            bool beforeThisWeek = Request["beforeThisWeek"] == null ? false : Convert.ToBoolean(Request["beforeThisWeek"]);
            //查询条件：仅本周以前的参考日期取值
            string refDate = Request["refDate"] == null ? "" : Request["refDate"];
             if (beforeThisWeek)
	            {
		             DateTime enddate=DateHelper.GetWeekEnd(DateTime.Now);
                     string enddatestr = enddate.ToString("yyyy-MM-dd");
             switch (refDate)
	            {
                        case    "planStartDate":
                        strWhere += " and b.FPLANSTARTDATE < '" + enddatestr.Trim() + "'";
                        break;
                        case "planEndDate"://计划完工日期
                        strWhere += " and b.FPLANENDDATE < '" + enddatestr.Trim() + "'";
                        break;
                        case "checkDate"://审核日期
                        strWhere += " and b.fcheckdate < '" + enddatestr.Trim() + "'";
                        break;
		            default:
             break;
	            }
	            }
           
 


            string FModel = Request["FModel"] == null ? "" : Request["FModel"];
            string FStatus = Request["FStatus"] == null ? "" : Request["FStatus"];
            string FPLANSTARTDATE = Request["FPLANSTARTDATE"] == null ? "" : Request["FPLANSTARTDATE"];
            int FWorkCenterID = string.IsNullOrWhiteSpace(Request["FWorkCenterID"]) ? 15293 : Convert.ToInt32(Request["FWorkCenterID"]);//工作中心，，默认制造部
            string fopernote = Request["fopernote"] == null ? "" : Request["fopernote"];//工艺名称
            //string opergroupid = Request["opergroupid"] == null ? "" : Request["opergroupid"];//工艺名称
 
            if (fopernote.Trim() != "" && !SqlInjection.GetString(fopernote))   //防止sql注入,前端输入工艺查询条件
            { strWhere += string.Format(" and b.fopernote = '{0}'", fopernote.Trim()); }
            //else if (opergroupid!="")//传入了工艺组ID
            //{
                
            //}
            else//前端无查询条件，根据权限分配工艺进行查询
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                UserDAL ud = new UserDAL(); 
                List<string> ls = ud.GetUserOper(uInfo.ID);
                string opers = "";
                foreach (string item in ls)
                {
                    opers += string.Format("'{0}',", item);
                }
                opers = opers.TrimEnd(",".ToCharArray());
                strWhere += string.Format(" and b.fopernote in ({0}) ",opers);
            }

            if (ficmono.Trim() != "" && !SqlInjection.GetString(ficmono))   //防止sql注入
                strWhere += string.Format(" and a.ficmono like '%{0}%'", ficmono.Trim());

            if (FName.Trim() != "" && !SqlInjection.GetString(FName))   //防止sql注入
                strWhere += string.Format(" and c.FName like '%{0}%'", FName.Trim());

            if (FModel.Trim() != "" && !SqlInjection.GetString(FModel))   //防止sql注入
                strWhere += string.Format(" and c.FModel like '%{0}%'", FModel.Trim());
            if (FWorkCenterID != 0)
            {
                strWhere += string.Format(" and b.FWorkCenterID = {0}", FWorkCenterID);
            }

            if (FStatus == "")//所有未完成
            {
                strWhere += string.Format(" and b.FStatus < 2 ");
            }
            else if (FStatus == "0")//未开始
            {
                strWhere += string.Format(" and b.FStatus = 0 ");
            }
            else if (FStatus == "1")//开始未完成
            {
                strWhere += string.Format(" and b.FStatus = 1 ");
            }

            if (FPLANSTARTDATE.Trim() != "")
                strWhere += " and b.FPLANSTARTDATE > '" + FPLANSTARTDATE.Trim() + "'";

            
            //强制排序规则： (case when d.FWORKTYPEID = 68 then 1  else 0 end) desc , b.FPLANENDDATE asc 
            //ppbomstatus:是否领料0,1;
            string content = ""; 
            //a,SHWORKBILL;b,shworkbillentry;c.t_ICITEMCORE
            string tables = "SHWORKBILL as a left join shworkbillentry as b  on a.FInterID=b.FInterID left join t_ICITEMCORE as c on a.FItemID = c.FItemID left join ICMO d on d.FBillNo=a.FICMONO left join ppbom e on e.ficmointerid=a.ficmointerid ";
            string fields = @" a.ficmono,a.ficmointerid,a.fitemid,c.FName,c.FModel,b.FQTYPLAN,b.FPLANSTARTDATE,b.FPLANENDDATE, b.FStartWorkDate,b.FEndWorkDate,b.fcheckdate,b.fopernote,b.FStatus,
                            b.FWorkCenterID,b.ffinishtime,b.FtotalWorkTime,case when d.FWORKTYPEID = 68 then 1  else 0 end isRush,e.FStatus ppbomstatus";
            string sqltable = string.Format("select top {0} * from (select row_number() over(order by case when d.FWORKTYPEID = 68 then 1  else 0 end  desc, {1} {2}) as rownum, {3} from {4} where {5}) as T where T.Rownum >({6}-1)*{0} order by rownum asc", pagesize, sort, order, fields, tables, strWhere, pageindex);
            
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, sqltable);

            string sqlcount = string.Format("select count(*) from {0} where {1}", tables, strWhere);
            int totalCount = (int)SqlHelper.ExecuteScalar(SqlHelper.connStrK3, sqlcount);

            //总工时
            string sqlalltime = string.Format("select sum(b.FtotalWorkTime-b.ffinishtime) from {0} where {1}", tables, strWhere);
            object o1 =SqlHelper.ExecuteScalar(SqlHelper.connStrK3, sqlalltime);
            double totalWorkTime=0;
            if (o1 is DBNull)
            {

            }
            else
            {
                totalWorkTime = Convert.ToDouble(o1);
            }
         

            string strJson = JsonHelper.ToJson(dt);
            content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + ",\"totalWorkTime\":" + totalWorkTime + "}";
            return Content(content);
        }


        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public ActionResult OperGroupEdit()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                OperEntity oper = new OperEntity();
                int OperGroupID = Request["FoperID"] == null ? 0 : int.Parse(Request["FoperID"]);
                string UpdateTime = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                string UpdateBy = uInfo.AccountName;
                string OperGroupName = Request["OperNote"] == null ? "" : Request["OperNote"];
                float DayTime = Request["DayTime"] == null ? 0 : float.Parse(Request["DayTime"]);
                bool IsCheck = Request["IsCheck"] == null ? false : bool.Parse(Request["IsCheck"]);
                string Remark = Request["Remark"] == null ? "" : Request["Remark"];
                int AlertValue = Request["AlertValue"] == null ? 0 : int.Parse(Request["AlertValue"]);
                //oper.WeekTime = float.Parse(Request["WeekTime"]);
                //oper.MonthTime = float.Parse(Request["MonthTime"]);
                if (OperGroupID == 0)
                {
                    return Content("{\"msg\":\"失败,工艺组编码为空！\",\"success\":false}");
                }

                string sql = string.Format("update tbOperGroup set OperGroupName='{0}', DayTime={1},  Remark='{2}', UpdateTime='{3}', UpdateBy='{4}', AlertValue= '{5}',IsCheck='{6}' where OperGroupID={7}", OperGroupName, DayTime, Remark, UpdateTime, UpdateBy, AlertValue, IsCheck, OperGroupID);

                int result = SqlHelper.ExecuteNonQuerySql(SqlHelper.connStr, sql);
                if (result > 0)
                {
                    return Content("{\"msg\":\"修改成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"修改失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }
        }
        /// <summary>
        /// 分配工艺组成员页面
        /// </summary>
        /// <returns></returns>
        public ActionResult OpermemberSet()
        {
            return View();
        }
        /// <summary>
        /// 车间作业管理--工艺产能预警视图
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopJobMgrAlert()
        {
            return View();
        }
        /// <summary>
        /// 车间作业管理--工艺任务单视图
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopJobList()
        {
            return View();
        }
        /// <summary>
        /// 获取工艺树
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllOperTree()
        {
            //DataTable dt = dal.GetAllButton(where);
            string sql = "select infoID as OperID,infoname as OperName from tbbasicinfo where parentID=40000";
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStr, sql);
            string sb = "[{\"id\":\"0\",\"text\":\"全选\",\"children\": [";
            foreach (DataRow dr in dt.Rows)
            {
                sb += "{\"id\":\"" + dr["OperID"] + "\",\"text\":\"" + dr["OperName"] + "\"},";
            }
            sb = sb.Trim(",".ToCharArray());
            sb += "]}]";
            return Content(sb);
        }

        /// <summary>
        /// 分配工艺组成员
        /// </summary>
        /// <returns></returns>
        public ActionResult SetOperMember()
        {
            string opergroupid = Request["opergroupid"];
            string operids = Request["operids"];

            // bool result = new MenuButtonBLL().SaveMenuButton(menuid, buttonids);

            List<string> list = new List<string>();
            list.Add("delete from tbOper where OperGroupID =" + opergroupid);
            foreach (string btnid in operids.TrimStart(',').TrimEnd(',').Split(','))
            {
                if (btnid != "0")
                {
                    list.Add(string.Format("INSERT INTO tbOper(OperGroupID,OperID,opername) select {0},{1}, infoname from tbbasicinfo where infoID={1}", opergroupid, btnid));
                }
            }
            try
            {
                if (SqlHelper.ExecuteNonQuery(SqlHelper.connStr, list) > 0)
                {
                    return Content("{\"msg\":\"分配工艺成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"分配工艺失败！\",\"success\":false}");
                }
            }
            catch
            {
                return Content("{\"msg\":\"出现异常，分配工艺失败！\",\"success\":false}");
            }


        }

        ///// <summary>
        ///// 获取已配置的工艺组成员
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetMenuButtonByMenuID()
        //{
        //    int mid = Convert.ToInt32(Request["menuid"]);  //菜单id
        //    string jsonStr = new MenuButtonBLL().GetButtonByMenuId(mid);
        //    return Content(jsonStr);
        //}


        public ActionResult DelOperGroup()
        {
            try
            {

                int OperGroupID = Request["FoperID"] == null ? 0 : int.Parse(Request["FoperID"]);

                if (OperGroupID > 0)
                {
                    string sql = string.Format("delete tbOperGroup where  OperGroupID ={0}", OperGroupID);
                    int result = SqlHelper.ExecuteNonQuerySql(SqlHelper.connStr, sql);
                    if (result > 0)
                    {
                        return Content("{\"msg\":\"删除成功！\",\"success\":true}");
                    }
                    else
                    {
                        return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                    }
                }
                else
                {
                    return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"删除失败," + ex.Message + "\",\"success\":false}");
            }
        }
        /// <summary>
        /// 获取所有工艺组
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllOperGroupInfo()
        {
            string sort = "OperGroupID  asc";
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
            int pagesize = Request["rows"] == null ? 8 : Convert.ToInt32(Request["rows"]);
            string strWhere = "1=1";
            int totalCount;   //输出参数           
            string strJson = new ProduceBLL().GetJsonPager("tbOperGroup", "OperGroupID,OperGroupName,DayTime,WeekTime,MonthTime,AlertValue,IsCheck,UpdateTime,UpdateBy,Remark", sort, pagesize, pageindex, strWhere, out totalCount);
            return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        }
        /// <summary>
        /// 根据工艺组ID查询所属成员
        /// </summary>
        /// <returns></returns>
        public ActionResult GetOperByGroupID()
        {
            string sort = "OperID desc";
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
            int pagesize = Request["rows"] == null ? 50 : Convert.ToInt32(Request["rows"]);
            int OperGroupID = Request["OperGroupID"] == null ? 1 : Convert.ToInt32(Request["OperGroupID"]);
            string view = Request["view"] == null ? "" : Convert.ToString(Request["view"]);

            if (view == "combotree")//传值给下选单
            {
                string ids = "";
                DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStr, "select OperID from tbOper where OperGroupID=" + OperGroupID);
                foreach (DataRow dr in dt.Rows)
                {
                    ids += dr["OperID"].ToString() + ",";
                }
                return Content(ids.TrimEnd(','));
            }
            else
            {
                string strWhere = "OperGroupID=" + OperGroupID;
                int totalCount;   //输出参数           
                string strJson = new ProduceBLL().GetJsonPager("tbOper", "OperID,OperName,Remark", sort, pagesize, pageindex, strWhere, out totalCount);
                return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
            }

        }

        /// <summary>
        /// 获取所有工艺能力设置
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllOperInfo()
        {

            //string FBillNo = Request["FBillNo"];
            string sort = "OperID desc";
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
            int pagesize = Request["rows"] == null ? 8 : Convert.ToInt32(Request["rows"]);
            string strWhere = "1=1";
            int totalCount;   //输出参数           
            string strJson = new ProduceBLL().GetJsonPager("tbOper", "OperID,OperNote,DayTime,WeekTime,MonthTime,UpdateTime,UpdateBy,Remark", sort, pagesize, pageindex, strWhere, out totalCount);
            return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        }


        //public ActionResult GetAllProduceInfo()
        //{
        //    string strWhere = "1=1";
        //    string sort = Request["sort"] == null ? "FPlanCommitDate" : Request["sort"];
        //    string order = Request["order"] == null ? "desc" : Request["order"];

        //    //首先获取前台传递过来的参数
        //    int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
        //    int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);
        //    string FBillNo = Request["FBillNo"] == null ? "" : Request["FBillNo"];
        //    string FItemID = Request["FItemID"] == null ? "" : Request["FItemID"];
        //    string isable = Request["isable"] == null ? "" : Request["isable"];
        //    string ifchangepwd = Request["ifchangepwd"] == null ? "" : Request["ifchangepwd"];
        //    string userperson = Request["userperson"] == null ? "" : Request["userperson"];
        //    string FPlanCommitDate = Request["FPlanCommitDate"] == null ? "" : Request["FPlanCommitDate"];
        //    string FPlanFinishDate = Request["FPlanFinishDate"] == null ? "" : Request["FPlanFinishDate"];

        //    if (FBillNo.Trim() != "" && !SqlInjection.GetString(FBillNo))   //防止sql注入
        //        strWhere += string.Format(" and FBillNo like '%{0}%'", FBillNo.Trim());
        //    //FName为非主表字段，暂不支持直接查询；
        //    //后期解决思路，先根据FName在子表中查询对应的FItemID，可能有多个，则将这多个拼接成where条件；
        //    //例如  FItemID = id1 and FItemID = id2  and FItemID = id3...
        //    //if (FName.Trim() != "" && !SqlInjection.GetString(FName))
        //    //    strWhere += string.Format(" and FName like '%{0}%'", FName.Trim());
        //    if (FItemID.Trim() != "" && !SqlInjection.GetString(FItemID))
        //        strWhere += string.Format(" and FItemID like '%{0}%'", FItemID.Trim());
        //    if (isable.Trim() != "select" && isable.Trim() != "")
        //        strWhere += " and IsAble = '" + isable.Trim() + "'";
        //    if (ifchangepwd.Trim() != "select" && ifchangepwd.Trim() != "")
        //        strWhere += " and IfChangePwd = '" + ifchangepwd.Trim() + "'";
        //    if (FPlanCommitDate.Trim() != "")
        //        strWhere += " and FPlanCommitDate > '" + FPlanCommitDate.Trim() + "'";
        //    if (FPlanFinishDate.Trim() != "")
        //        strWhere += " and FPlanFinishDate < '" + FPlanFinishDate.Trim() + "'";

        //    //抽取主作业计划单,规则不包含-、_两种连接符,ESCAPE表示该
        //    strWhere += "and Fbillno not like '%v_%'  ESCAPE   'v'  and  Fbillno not like '%v-%' ESCAPE   'v'";

        //    int totalCount;   //输出参数           
        //    string strJson = new ProjectBLL().GetJsonPager("ICMO", "FBillNo,FStatus,FQty,FCommitQty,FPlanCommitDate,FPlanFinishDate,FStartDate,FFinishDate,FType,FWorkShop,FItemID", sort + " " + order, pagesize, pageindex, strWhere, out totalCount);
        //    //var jsonResult = new { total = totalCount.ToString(), rows = strJson };
        //    return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        //}

        /// <summary>
        /// 将生产栏的查询请求跳转到项目栏
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Projectgrid(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Project/Projectgrid");
            }
            catch
            {
                return View();
            }
        }


        //
        // GET: /Produce/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexDetail()
        {
            return View();
        }

        //
        // GET: /Produce/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Produce/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Produce/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Produce/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Produce/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Produce/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Produce/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
