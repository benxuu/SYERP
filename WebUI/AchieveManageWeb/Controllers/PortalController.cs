﻿using AchieveBLL;
using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using AchieveManageWeb.Models.ActionFilters;
using AchieveManageWeb.Models;
using AchieveDAL;

namespace AchieveManageWeb.Controllers
{
  [AchieveManageWeb.App_Start.JudgmentLogin]
    public class PortalController : Controller
    {
        //
        // GET: /Portal/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyProject()
        {
            return View();
        }
        public ActionResult ProjectSearch()
        {
            return View();
        }

        public ActionResult getNotice()
        {
            //Session["name"] = Label.Text
            //string sql = "select top 1 ftitle,fcontent from tbNews where ftypeid=4 ORDER BY UpdateTime desc ";
            string sql = "select top 1 ftitle,fcontent from tbNews ORDER BY UpdateTime desc ";
            DataTable dt = SqlHelper.GetDataTableIE(sql);
            if (dt.Rows.Count==1)
            {
            string title = string.Format("<h1>{0}</h1>", dt.Rows[0]["ftitle"].ToString());
            string content = string.Format("<p>{0}</p>", dt.Rows[0]["fcontent"].ToString());
            return Content(title+content);
            }
            else
            {
                return Content(string.Format("<h1>{0}</h1>", "无"));
            }
           

            //string JsonRows = JsonHelper.ToJson(rdt);
            ////string  content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + ",\"success\":true}";
            //return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + "}"); 
        }
           /// <summary>
           /// 
           /// </summary>
           /// <returns></returns>
        public ActionResult queryMixtureProject()
        {
            string ProjectNo = string.IsNullOrWhiteSpace(Request["ProjectNo"]) ? "" : Request["ProjectNo"].Trim();
            string ProjectName = string.IsNullOrWhiteSpace(Request["ProjectName"]) ? "" : Request["ProjectName"].Trim();
            
            string type = Request["type"]; 
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);//输出的数据页码
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);//每页输出数量
            int totalCount=0;
            if (SqlInjection.GetString(ProjectNo)||SqlInjection.GetString(ProjectName))
            {
                 return Content("{\"msg\":\"获取数据失败,查询参数中存在非法字符！\",\"success\":false}");
            }
            DataTable rdt=new DataTable();
            string where; 
 
            switch (type)
            {
                case   "pm":
                   where = string.Format(" ProjectName like '%{0}%' and ProjectNo like '%{1}%' ",ProjectName,ProjectNo);
                   rdt= SqlPagerHelper.GetPager("tbProject", "ProjectNo,ProjectName", "UpdateTime desc", pagesize, pageindex, where, out totalCount);
                  
                   break;
                case "produce":
                   where = string.Format(" FName like '%{0}%' and FBillNo like '%{1}%' ", ProjectName, ProjectNo); 
                   rdt = manufactureBLL.GetPageUnClosedMainProject((pageindex - 1) * pagesize + 1, pageindex * pagesize, where, out totalCount);
                   break;
                default:
                    break;
            }
            string JsonRows = JsonHelper.ToJson(rdt);
          //string  content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + ",\"success\":true}";
          return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + "}"); 
        }

    }
}
