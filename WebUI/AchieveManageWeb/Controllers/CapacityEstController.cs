﻿using AchieveCommon;
using AchieveDAL;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using AchieveManageWeb.Models;

namespace AchieveManageWeb.Controllers
{
      [AchieveManageWeb.App_Start.JudgmentLogin]
    public class CapacityEstController : Controller
    {
        //
        // GET: /CapacityEst/

        public ActionResult Index()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "产能预警")); //日志记录
            return View();
        }
        public ActionResult WeekOper()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "WeekOper")); //日志记录
           return View();
        }
        public ActionResult shopJobOperAlert()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "shopJobOperAlert")); //日志记录
            return View();
        }
          /// <summary>
          /// 返回车间作业工艺预警视图,连续3周、三月
          /// </summary>
          /// <returns></returns>
        public ActionResult getShopJobOperAlert()
        {
                bool removefinished = Request["removefinished"] == null ? true :Convert.ToBoolean(Request["removefinished"]);
                string strWhere = "1=1"; 
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                string opergroups = UserDAL.GetUserOperGroupIDs(uInfo.ID); 
                strWhere += string.Format(" and b.fopernote in ({0}) ", opergroups); 
            //获取当前日期、周 
                int thisweek = DateHelper.GetWeekIndex(DateTime.Now);
                int thisyear = DateTime.Now.Year;
                int thismonth = DateTime.Now.Month;
            //实时计算
                string[] opergroup = opergroups.Split(',');                 
                string sqldt =string.Format( "select opergroupid,opergroupname,daytime,alertvalue from tbopergroup where opergroupid in ({0})",opergroups);
                DataTable dt = SqlHelper.GetDataTableIE(sqldt);
                dt.Columns.Add("lastweek",typeof(double));
                dt.Columns.Add("thisweek", typeof(double));
                dt.Columns.Add("nextweek", typeof(double));
                dt.Columns.Add("lastmonth", typeof(double));
                dt.Columns.Add("thismonth", typeof(double));
                dt.Columns.Add("nextmonth", typeof(double));
                DateTime startDate, endDate,mstartdate,menddate;
                DateHelper.GetWeek(DateTime.Now.Year, thisweek, out startDate, out endDate);//获取本周起止时间；
                DateHelper.GetMonthSE(thisyear, thismonth, out mstartdate, out menddate);//获取本月起止时间；
               
            for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int opergroupid = Convert.ToInt32(dt.Rows[i]["opergroupid"]);
                    dt.Rows[i]["thisweek"] = AchieveBLL.CockpitBLL.operValue(opergroupid, startDate, endDate, removefinished, "all", "plan");
                    dt.Rows[i]["thismonth"] = AchieveBLL.CockpitBLL.operValue(opergroupid, mstartdate, menddate, removefinished, "all", "plan");
                }

               // DateHelper.GetWeek(DateTime.Now.Year, thisweek-1, out startDate, out endDate);//获取上周起止时间；
                //DateHelper.GetMonthSE(thisyear, thismonth - 1, out mstartdate, out menddate);//获取上上月起止时间；
                startDate = startDate.AddDays(-7);
                endDate = endDate.AddDays(-7);
                mstartdate = mstartdate.AddMonths(-1);  
                menddate = mstartdate.AddMonths(1).AddDays(-1); 
            for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int opergroupid = Convert.ToInt32(dt.Rows[i]["opergroupid"]);
                    dt.Rows[i]["lastweek"] = AchieveBLL.CockpitBLL.operValue(opergroupid, startDate, endDate, removefinished, "all", "plan");
                    dt.Rows[i]["lastmonth"] = AchieveBLL.CockpitBLL.operValue(opergroupid, mstartdate, menddate, removefinished, "all", "plan");
                }
            //获取下一周期时间
                //DateHelper.GetWeek(DateTime.Now.Year, thisweek + 1, out startDate, out endDate);//获取本周起止时间；
                //DateHelper.GetMonthSE(thisyear, thismonth + 1, out mstartdate, out menddate);
            startDate = startDate.AddDays(14);
            endDate = endDate.AddDays(14);
            mstartdate = mstartdate.AddMonths(2);
            menddate = mstartdate.AddMonths(1).AddDays(-1); 

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int opergroupid = Convert.ToInt32(dt.Rows[i]["opergroupid"]);
                    dt.Rows[i]["nextweek"] = AchieveBLL.CockpitBLL.operValue(opergroupid, startDate, endDate, removefinished, "all", "plan");
                    dt.Rows[i]["nextmonth"] = AchieveBLL.CockpitBLL.operValue(opergroupid, mstartdate, menddate, removefinished, "all", "plan");
                }


                string content = "";
                int totalCount=dt.Rows.Count;   //输出参数
                string strJson = JsonHelper.ToJson(dt);
                content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}";
                return Content(content); 
        }
    }
}
