﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using AchieveManageWeb.Models.ActionFilters;
using AchieveManageWeb.Models;
using AchieveDAL;
using AchieveBLL;

namespace AchieveManageWeb.Controllers
{
  [AchieveManageWeb.App_Start.JudgmentLogin]
    public class SaleController : Controller
    {
        //
        // GET: /Sale/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SaleOrder()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "销售订单", "SaleOrder")); //日志记录
            return View();
        }
        public ActionResult SaleForecast()
        {
            AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new LogContent(ViewData, "访问视图", "销售预测", "SaleForecast")); //日志记录
            return View();
        }

        public ActionResult getSaleForecastPager()
        {
            //int year = string.IsNullOrWhiteSpace(Request["year"]) ? 2019 : Convert.ToInt32(Request["year"]);//订单年份            
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "FItemID" : Request["sort"];
            string order = Request["order"] == null ? "asc" : Request["order"];
            //string view = Request["view"] == null ? "ProjectGrid" : Request["view"];
            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);//输出的数据页码
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);//每页输出数量
            //string FBillNo = Request["FBillNo"] == null ? "" : Request["FBillNo"];
            //string FName = Request["FName"] == null ? "" : Request["FName"];
            //string FModel = Request["FModel"] == null ? "" : Request["FModel"];
            //string Dept = Request["Dept"] == null ? "all" : Request["Dept"].Trim();
            //string FStatus = Request["FStatus"] == null ? "" : Request["FStatus"];
            //string FWORKTYPEID = Request["FWORKTYPEID"] == null ? "" : Request["FWORKTYPEID"];//订单类型，all所有
            //if (!string.IsNullOrWhiteSpace(FWORKTYPEID) && FWORKTYPEID != "all")   //
            //    strWhere += string.Format(" and FWORKTYPEID = '{0}'", FWORKTYPEID.Trim());

            //string FPlanCommitDate = Request["FPlanCommitDate"] == null ? "" : Request["FPlanCommitDate"];
            //if (FBillNo.Trim() != "" && !SqlInjection.GetString(FBillNo))   //防止sql注入
            //    strWhere += string.Format(" and FBillNo like '%{0}%'", FBillNo.Trim());

            //if (FName.Trim() != "" && !SqlInjection.GetString(FName))   //防止sql注入
            //    strWhere += string.Format(" and FName like '%{0}%'", FName.Trim());

            //if (FModel.Trim() != "" && !SqlInjection.GetString(FModel))   //防止sql注入
            //    strWhere += string.Format(" and FModel like '%{0}%'", FModel.Trim());
            int totalCount;
           DataTable dt = SaleBLL.getSaleForecastPager(pagesize, pageindex, strWhere, sort + " " + order, out  totalCount);
               string   JsonRows = AchieveCommon.JsonHelper.ToJson(dt);
               string content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + "}";                   
            return Content(content);
        }

        public ActionResult getSaleOrderPager()
        {
            //int year = string.IsNullOrWhiteSpace(Request["year"]) ? 2019 : Convert.ToInt32(Request["year"]);//订单年份            
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "FItemID" : Request["sort"];
            string order = Request["order"] == null ? "asc" : Request["order"];
            //string view = Request["view"] == null ? "ProjectGrid" : Request["view"];
            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);//输出的数据页码
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);//每页输出数量
            //string FBillNo = Request["FBillNo"] == null ? "" : Request["FBillNo"];
            //string FName = Request["FName"] == null ? "" : Request["FName"];
            //string FModel = Request["FModel"] == null ? "" : Request["FModel"];
            //string Dept = Request["Dept"] == null ? "all" : Request["Dept"].Trim();
            //string FStatus = Request["FStatus"] == null ? "" : Request["FStatus"];
            //string FWORKTYPEID = Request["FWORKTYPEID"] == null ? "" : Request["FWORKTYPEID"];//订单类型，all所有
            //if (!string.IsNullOrWhiteSpace(FWORKTYPEID) && FWORKTYPEID != "all")   //
            //    strWhere += string.Format(" and FWORKTYPEID = '{0}'", FWORKTYPEID.Trim());

            //string FPlanCommitDate = Request["FPlanCommitDate"] == null ? "" : Request["FPlanCommitDate"];
            //if (FBillNo.Trim() != "" && !SqlInjection.GetString(FBillNo))   //防止sql注入
            //    strWhere += string.Format(" and FBillNo like '%{0}%'", FBillNo.Trim());

            //if (FName.Trim() != "" && !SqlInjection.GetString(FName))   //防止sql注入
            //    strWhere += string.Format(" and FName like '%{0}%'", FName.Trim());

            //if (FModel.Trim() != "" && !SqlInjection.GetString(FModel))   //防止sql注入
            //    strWhere += string.Format(" and FModel like '%{0}%'", FModel.Trim());
            int totalCount;
            DataTable dt = SaleBLL.getSaleOrderPager(pagesize, pageindex, strWhere, sort + " " + order, out  totalCount);
            string JsonRows = AchieveCommon.JsonHelper.ToJson(dt);
            string content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + "}";
            return Content(content);  
        }
        public ActionResult getSaleOrderGroupPager()
        {
            //int year = string.IsNullOrWhiteSpace(Request["year"]) ? 2019 : Convert.ToInt32(Request["year"]);//订单年份            
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "FDate" : Request["sort"];
            string order = Request["order"] == null ? "desc" : Request["order"];
            //string view = Request["view"] == null ? "ProjectGrid" : Request["view"];
            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);//输出的数据页码
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);//每页输出数量
            //string FBillNo = Request["FBillNo"] == null ? "" : Request["FBillNo"];
            //string FName = Request["FName"] == null ? "" : Request["FName"];
            //string FModel = Request["FModel"] == null ? "" : Request["FModel"];
            //string Dept = Request["Dept"] == null ? "all" : Request["Dept"].Trim();
            //string FStatus = Request["FStatus"] == null ? "" : Request["FStatus"];
            //string FWORKTYPEID = Request["FWORKTYPEID"] == null ? "" : Request["FWORKTYPEID"];//订单类型，all所有
            //if (!string.IsNullOrWhiteSpace(FWORKTYPEID) && FWORKTYPEID != "all")   //
            //    strWhere += string.Format(" and FWORKTYPEID = '{0}'", FWORKTYPEID.Trim());

            //string FPlanCommitDate = Request["FPlanCommitDate"] == null ? "" : Request["FPlanCommitDate"];
            //if (FBillNo.Trim() != "" && !SqlInjection.GetString(FBillNo))   //防止sql注入
            //    strWhere += string.Format(" and FBillNo like '%{0}%'", FBillNo.Trim());

            //if (FName.Trim() != "" && !SqlInjection.GetString(FName))   //防止sql注入
            //    strWhere += string.Format(" and FName like '%{0}%'", FName.Trim());

            //if (FModel.Trim() != "" && !SqlInjection.GetString(FModel))   //防止sql注入
            //    strWhere += string.Format(" and FModel like '%{0}%'", FModel.Trim());
            int totalCount;
            DataTable dt = SaleBLL.getSaleOrderGroupPager(pagesize, pageindex, strWhere, sort + " " + order, out  totalCount);
            string JsonRows = AchieveCommon.JsonHelper.ToJson(dt);
            string content = "{\"total\": " + totalCount.ToString() + ",\"rows\":" + JsonRows + "}";
            return Content(content);
        }

    }
}
