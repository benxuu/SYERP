﻿using AchieveBLL;
using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AchieveManageWeb.Controllers
{
    [AchieveManageWeb.App_Start.JudgmentLogin]
    public class NotesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllNotesInfo()
        {
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "id" : Request["sort"];
            string order = Request["order"] == null ? "asc" : Request["order"];
            string typeID = Request["typeID"] == null ? "" : Request["typeID"];
            string citeID = Request["citeID"] == null ? "" : Request["citeID"];
            if (!string.IsNullOrEmpty(Request["typeID"]) && !SqlInjection.GetString(Request["typeID"]))
            {
                strWhere += " and typeID = '" + typeID + "'";
            }
            if (!string.IsNullOrEmpty(Request["citeID"]) && !SqlInjection.GetString(Request["citeID"]))
            {
                strWhere += " and citeID = '" + citeID + "'";
            }
             

            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);
            int totalCount = 0;   //输出参数 
            //if (order.IndexOf(',') != -1)   //如果有","就是多列排序（不能拿列判断，列名中间可能有","符号）
            //{
            //    //多列排序：
            //    //sort：ParentId,Sort,AddDate
            //    //order：asc,desc,asc
            //    string sortMulti = "";  //拼接排序条件，例：ParentId desc,Sort asc
            //    string[] sortArray = sort.Split(',');   //列名中间有","符号，这里也要出错。正常不会有
            //    string[] orderArray = order.Split(',');
            //    for (int i = 0; i < sortArray.Length; i++)
            //    {
            //        sortMulti += sortArray[i] + " " + orderArray[i] + ",";
            //    }
            //    strJson = new NotesBLL().GetPager("vw_Notes", "id,ftypeid,ftitle,fcontent,ftypename,fsort,CreateTime,CreateBy,UpdateTime,UpdateBy", sortMulti.Trim(','), pagesize, pageindex, strWhere, out totalCount);
            //}
            //else
            //{
            //    strJson = new NotesBLL().GetPager("vw_Notes", "id,ftypeid,ftitle,fcontent,ftypename,fsort,CreateTime,CreateBy,UpdateTime,UpdateBy", sort + " " + order, pagesize, pageindex, strWhere, out totalCount);
            //}
            
            DataTable dt = SqlPagerHelper.GetPagerBySQL(SqlHelper.connStr, "tbNote", "*", sort + " " + order,  pagesize, pageindex, strWhere, out totalCount);

            //var jsonResult = new { total = totalCount.ToString(), rows = strJson };
            string strJson = JsonHelper.ToJson(dt);
            return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        }

        /// <summary>
        /// 新增页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult NotesAdd()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNotes()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                //NotesEntity NotesAdd = new NotesEntity();
                //NotesAdd.ftitle = Request["FTitle"];
                //NotesAdd.ftypeid = int.Parse(Request["FTypeId"]);
                //NotesAdd.fcontent = Request["FContent"];
                 string CreateBy = uInfo.AccountName;
                 DateTime CreateTime = DateTime.Now;
                string UpdateBy = uInfo.AccountName;
                 DateTime UpdateTime = DateTime.Now;
                //int id = new NotesBLL().Add(NotesAdd);
                string ID = Request["ID"];
                string typeID = Request["typeID"];
                string citeID = Request["citeID"];
                string noteContent = Request["noteContent"];

                string sql = @"insert into tbNote(ID,typeID,citeID,noteContent,createTime,createBy,updateTime,updateBy,status)
                values('{0}','{1}','{2}','{3}','{4}','{5}','{4}','{6}',0)";
                sql = string.Format(sql, ID, typeID, citeID, noteContent, DateHelper.NowTime, CreateBy, UpdateBy);
                int id = SqlHelper.ExecuteNonQuerySql(SqlHelper.connStr, sql);
                if (id > 0)
                {
                    return Content("{\"msg\":\"添加成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"添加失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }
        }

         

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditNotes()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;

                string ID = Request["ID"];
                string noteContent = Request["noteContent"];
                //NotesEntity NotesEdit = new NotesBLL().GetModel(id);
                //NotesEdit.ftitle = Request["FTitle"];
                //NotesEdit.ftypeid = int.Parse(Request["FTypeId"]);
                //NotesEdit.fcontent = Request["FContent"];
               string UpdateBy = uInfo.AccountName;
               string sql = "update tbNote set noteContent='{0}',updateBy='{1}',updateTime='{2}' where ID='{3}' ";
               sql = string.Format(sql,noteContent, UpdateBy, DateHelper.NowTime, ID);
               int result = SqlHelper.ExecuteNonQuerySql(SqlHelper.connStr,sql);
               // int result = new NotesBLL().Update(NotesEdit);
                if (result > 0)
                {
                    return Content("{\"msg\":\"修改成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"修改失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }
        }

        public ActionResult DelNotesByID()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                string ID = Request["ID"];
                if (!string.IsNullOrEmpty(ID))
                {
                    string sql = "delete tbNote  where ID='{0}' ";
                    sql = string.Format(sql,ID);
                    int result = SqlHelper.ExecuteNonQuerySql(SqlHelper.connStr, sql);


                    if (result>0)
                    {
                        return Content("{\"msg\":\"删除成功！\",\"success\":true}");
                    }
                    else
                    {
                        return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                    }
                }
                else
                {
                    return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"删除失败," + ex.Message + "\",\"success\":false}");
            }
        }


    }
}
