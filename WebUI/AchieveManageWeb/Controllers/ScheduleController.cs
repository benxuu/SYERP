﻿//last modified：2020/1/20，ben，修改排程日期规则，8小时为1天，宽放3天，委外5天，最长90天；
using AchieveBLL;
using AchieveDAL;
using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using AchieveManageWeb.Models.ActionFilters;

namespace AchieveManageWeb.Controllers
{

     [AchieveManageWeb.App_Start.JudgmentLogin]
    public class ScheduleController : Controller
    {
        //
        // GET: /Schedule/

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取工序计划表，输入FBillNo，ICMO.FBillNo==shworkbill.FICMONO，暂不支持分页，该功能暂不交付，保留下一步使用
        /// </summary>
        /// <returns></returns>
        public ActionResult doSchedule()
        {
            string t = ConfigHelper.ReadConfig("MOclTime");
            Int32 MOclTime = string.IsNullOrWhiteSpace(t) ? 3 : Convert.ToInt32(t); //默认完工提前日为3天

            //首先获取前台传递过来的参数
            string FBillNo;
            if (string.IsNullOrWhiteSpace(Request["FBillNo"]))
            {
                return Content("{\"msg\":\"修改失败,未获取到订单号\",\"success\":false}");                
            }
            else
            {
                FBillNo = Request["FBillNo"];
            }

            SqlParameter[] paras = new SqlParameter[]{                        
                new SqlParameter("@FBillNo", SqlDbType.VarChar) { Value = FBillNo} 
            };

            string sql = "select fwbinterid,fentryid,fplanstartdate,fplanenddate,ftotalworktime,FEntrySelfz0374,FEntrySelfz0375,FPlanCommitDate,FPlanFinishDate  from shworkbillentry a left join ICMO b on a.FEntrySelfz0373=b.FBillNo where FEntrySelfz0373=@FBillNo order by fentryid desc";
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, sql, paras);
            DateTime planED = Convert.ToDateTime(dt.Rows[0]["FPlanFinishDate"]);
            DateTime fpday = planED.AddDays(-MOclTime);//
            List<string> sqllist = new List<string>();

            //进入倒排工期计算
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int fwbinterid = (int)dt.Rows[i]["fwbinterid"];
                    dt.Rows[i]["FEntrySelfz0375"] = fpday;
                    sqllist.Add("update shworkbillentry set FEntrySelfz0375='" + fpday.ToString() + "' where fwbinterid=" + fwbinterid);
                    double ftotalworktime = Convert.ToDouble(dt.Rows[i]["ftotalworktime"]);//数据库字段为decimal类型
                    int days = (int)Math.Ceiling(ftotalworktime / 8);//转换为天；
                    fpday = fpday.AddDays(-days);
                    dt.Rows[i]["FEntrySelfz0374"] = fpday;
                    sqllist.Add("update shworkbillentry set FEntrySelfz0374='" + fpday.ToString() + "' where fwbinterid=" + fwbinterid);
                    fpday = fpday.AddDays(-1);//保证前置工序提前1天完成
                }
                //写入数据库；
                int count = SqlHelper.ExecuteNonQuery(SqlHelper.connStrK3, sqllist);

                string result = "{\"msg\":\"排程计算完成,成功写入数据" + count + "条!\",\"success\":true}";

                return Content(result);
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }

        }

         /// <summary>
         /// 未使用！
         /// 根据生产任务单单号和完工日期，倒排计算隶属工序单的时间，不考虑提前完工时间
         /// </summary>
         /// <param name="FBillNo">任务单单号，主单和子单同样对待</param>
         /// <param name="finishDate">任务单完工日期</param>
         /// <returns></returns>
        public int scheduleByFBillNo(string FBillNo, DateTime finishDate)
        {           
            SqlParameter[] paras = new SqlParameter[]{ 
                new SqlParameter("@FBillNo", SqlDbType.VarChar) { Value = FBillNo} 
            };
            string sql = "select fwbinterid,fentryid,ftotalworktime,FEntrySelfz0374,FEntrySelfz0375  from shworkbillentry where FEntrySelfz0373=@FBillNo order by fentryid desc";
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, sql, paras);
            DateTime fpday = finishDate; 
            List<string> sqllist = new List<string>();
            //进入倒排工期计算
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int fwbinterid = (int)dt.Rows[i]["fwbinterid"];
                    dt.Rows[i]["FEntrySelfz0375"] = fpday;
                    sqllist.Add("update shworkbillentry  WITH(HOLDLOCK)  set FEntrySelfz0375='" + fpday.ToString() + "' where fwbinterid=" + fwbinterid);
                    double ftotalworktime = Convert.ToDouble(dt.Rows[i]["ftotalworktime"]);//数据库字段为decimal类型
                    int days = (int)Math.Ceiling(ftotalworktime / 8);//转换为天；
                    fpday = fpday.AddDays(-days);
                    dt.Rows[i]["FEntrySelfz0374"] = fpday;
                    sqllist.Add("update shworkbillentry  WITH(HOLDLOCK)  set FEntrySelfz0374='" + fpday.ToString() + "' where fwbinterid=" + fwbinterid);
                    fpday = fpday.AddDays(-1);//保证前置工序提前1天完成
                }
                //写入数据库；
                int count = SqlHelper.ExecuteNonQuery(SqlHelper.connStrK3, sqllist);
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
         /// <summary>
         /// 排程基本函数，输入生产任务单，完工日期，对该任务单所属的所有工单进行排程，返回最迟备货日期；
         /// 规则：工时每8小时算1天，最小单位1天，工时为0的计3天，因为0一般为委外！
         /// </summary>
         /// <param name="FBillNo">任务单号</param>
         /// <param name="finishDate">最迟完工日期</param>
        /// <returns>最迟备货日期</returns>
        public DateTime scheduleByFBillNoDatetime(string FBillNo, DateTime finishDate)
        {
            SqlParameter[] paras = new SqlParameter[]{ 
                new SqlParameter("@FBillNo", SqlDbType.VarChar) { Value = FBillNo} 
            };
            string sql = "select fwbinterid,fentryid,ftotalworktime,FEntrySelfz0374,FEntrySelfz0375  from shworkbillentry where FEntrySelfz0373=@FBillNo order by fentryid desc";
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, sql, paras);
            DateTime fpday = finishDate;
            DateTime fpdayTemp;
            List<string> sqllist = new List<string>();
            //进入倒排工期计算
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int fwbinterid = (int)dt.Rows[i]["fwbinterid"];
                    //dt.Rows[i]["FEntrySelfz0375"] = fpday;
                   
                    double ftotalworktime = Convert.ToDouble(dt.Rows[i]["ftotalworktime"]);//数据库字段为decimal类型                    
                    int days = 3+(int)Math.Ceiling(ftotalworktime / 8);//转换为天；//最少一天 ，所有工序放宽3天，即至少4天；                   
                    if (days==0)
                    {
                        days = 5;//工时为0的日期定为5天，考虑一般为委外工单；
                    }
                    else if (days>90)//最长排程时长为90天；
                    {
                        days = 90;
                    }
                     
                    fpdayTemp = fpday.AddDays(1-days);//当天已有一天！
                    sqllist.Add("update shworkbillentry  WITH(HOLDLOCK)  set FEntrySelfz0374='" + fpdayTemp.ToString() + "', FEntrySelfz0375='" + fpday.ToString() + "' where fwbinterid=" + fwbinterid);
                    fpday = fpdayTemp.AddDays(-1);//保证前置工序提前1天完成                    
                }
                //写入数据库；
                int count = SqlHelper.ExecuteNonQuery(SqlHelper.connStrK3, sqllist);
               // System.Diagnostics.Debug.WriteLine("update:" + FBillNo+"，数据："+count);
                return fpday;//返回排程后得出的最迟备货日期
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void UpdateData(DataTable dt, DateTime TimeNow)
        {
            List<string> sqllist_main = new List<string>();
            List<string> over_main_list = new List<string>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow Order = dt.Rows[i];
                Int32[] OverLimitList = (int[])Order["OverLimitList"];
                Int32 OverLimit = (int)Order["OverLimit"];
                String OrderId = (string)Order["OrderNumber"];
                Int32[] OrderStartEnd = (int[])Order["OrderStartEnd"];
                Int32 OrderDeadline = (int)Order["OrderDeadline"];
                Int32[] ProcessNumber = (int[])Order["OrderProc"];
                Int32[] ProcessDay = (int[])Order["OrderProcDay"];
                Double[] ProcessTime = (double[])Order["OrderProcTime"];
                Int32[] OrderProcStartEnd = (int[])Order["OrderProcStartEnd"];
                Int32 OrderOverTime = (int)Order["OrderOverTime"];
                Int32 CanNotFinish = (int)Order["CanNotFinish"];
                Int32[] OrderChildId = (int[])Order["fwbinterid"];
                Int32 LengthOfBill = (int)Order["LengthBill"];
                //Double[] OverValues = (double[])Order["OverValues"];
                List<string> sqllist = new List<string>();

                //sqllist_main.Add("update icmo set OverCapacityLimit=" + OverLimit + " where FBillNo='" + OrderId + "'");  //是否超出产能上限

                //while (OrderId.Contains("_") || OrderId.Contains("-")){
                //    if (OrderId.Contains("_"))
                //    {
                //        OrderId = OrderId.Substring(0, OrderId.LastIndexOf("_"));
                //    }
                //    if (OrderId.Contains("-")){
                //        OrderId = OrderId.Substring(0, OrderId.LastIndexOf("-"));
                //    }
                //}
                //if (over_main_list.Contains(OrderId) != true)
                //{
                //    if (OverLimit == 0)
                //    {
                //        sqllist_main.Add("update icmo set OverCapacityLimit=" + OverLimit + " where FBillNo='" + OrderId + "'");
                //    }
                //    if (OverLimit == 1)
                //    {
                //        sqllist_main.Add("update icmo set OverCapacityLimit=" + OverLimit + " where FBillNo='" + OrderId + "'");
                //        over_main_list.Add(OrderId);
                //    }
                //}             
                for (int j = 0; j < LengthOfBill; j++)
                {
                    int fwbinterid = OrderChildId[j];
                    DateTime startday = TimeNow.AddDays(OrderProcStartEnd[2 * j]);
                    DateTime endday = TimeNow.AddDays(OrderProcStartEnd[2 * j + 1]);
                    //int OverOrnot = OverLimitList[j];
                    //double overvalue = OverValues[j];
                    sqllist.Add("update shworkbillentry set FEntrySelfz0374='" + startday.ToString() + "' where fwbinterid=" + fwbinterid);
                    sqllist.Add("update shworkbillentry set FEntrySelfz0375='" + endday.ToString() + "' where fwbinterid=" + fwbinterid);
                    //sqllist.Add("update shworkbillentry set OverCapacityLimit=" + OverOrnot + " where fwbinterid=" + fwbinterid);
                    //sqllist.Add("update shworkbillentry set OverCapacityValues=" + overvalue + " where fwbinterid=" + fwbinterid);

                }
                int count = SqlHelper.ExecuteNonQuery(SqlHelper.connStrK3, sqllist);
            }
            int count1 = SqlHelper.ExecuteNonQuery(SqlHelper.connStrK3, sqllist_main);
        }


      
         /// <summary>
         /// 完整排程，分两步执行，第一步：取所有未完成未逾期的主生产计划单，按bom层级从主单完工日期倒排；
         /// 第二步：对经过第一步后含未排程工单的计划单，以该计划单的完工日期单独进行倒排；此时不考虑提前完工日期！
         /// 规则：1、逾期任务单，交货期改为当天日期后延15天；
         /// 2、未完成
        /// 3、主单提前完工日期，默认3天，ConfigHelper.ReadConfig("MOclTime");
         /// </summary>
         /// <returns></returns>
        public ActionResult doAllSchedule()
        {
            //try
            //{
                     string t = ConfigHelper.ReadConfig("MOclTime");
                                Int32 MOclTime = string.IsNullOrWhiteSpace(t) ? 3 : Convert.ToInt32(t); //默认完工提前日为3天
                                string returnMsg="排程处理任务单如下：";
                                //首先获取所有未完成未逾期的主生产计划单,规则：未关闭&&未完成&&计划完成时间>今天
                                StringBuilder sbsql = new StringBuilder();
                                //sbsql.Append("select  FItemID,FBillNo,FPlanCommitDate,FPlanFinishDate from ICMO where FStatus<3 and fmrpclosed=0 and  FPlanFinishDate>'"+DateTime.Now.ToString()+"' ");
                                //新规则，逾期工单按后延15天计算                
                                sbsql.Append("select  FItemID,FBillNo,FPlanCommitDate,FPlanFinishDate from ICMO where FStatus<3 and fmrpclosed=0 ");
                                  //抽取主作业计划单,规则不包含-、_两种连接符
                                 sbsql.Append( " and Fbillno not like '%v_%'  ESCAPE   'v'  and  Fbillno not like '%v-%' ESCAPE   'v'");
                                 DataTable dtMoMain = SqlHelper.GetDataTable(SqlHelper.connStrK3, sbsql.ToString());
                                 returnMsg += string.Format("主任务单共计{0}条；", dtMoMain.Rows.Count);
          
                                //对于每个主任务单，首先对其工单进行排程，然后获取其分层的未完工的子生产任务单；
                                 foreach (DataRow item in dtMoMain.Rows)
                                 {
                                     System.Diagnostics.Debug.WriteLine("开始处理单据:" + item["FBillNo"].ToString() );
                                     returnMsg += item["FBillNo"].ToString()+ ";"; 
                                     DateTime FPlanFinishDate = Convert.ToDateTime(item["FPlanFinishDate"]);
                                     if (FPlanFinishDate<DateTime.Now)//对逾期任务单，以当前日期延迟15天作为交货日期；
                                     {
                                         FPlanFinishDate = DateTime.Now.AddDays(15);
                                     }
                                     DateTime fday = FPlanFinishDate.AddDays(-MOclTime);//考虑提前完工日期；
                                     fday = scheduleByFBillNoDatetime(item["FBillNo"].ToString(), fday);//1、对主单的工序计划进行排程，返回进入主单工序的最迟日期
                                     //获取所有1级子单
                                     DataTable dtmochild = manufactureBLL.GetManufactureSubOrder(item["FBillNo"].ToString(), "FStatus<3", 1);
                 
                                     foreach (DataRow itemchild in dtmochild.Rows)
                                     {
                                         returnMsg += "处理子单:" + itemchild["FBillNo"].ToString() + ",完工日期:"+fday.ToString()+";"; 
                                         doScheduleLoop(itemchild, fday);
                                        
                                     }                
                                }
                          string   tempstr=  "根据主单进行排程处理完毕！进入排程第二阶段：找出含有未排程工单的生产计划单，不区分子单主单，直接排程；";
                                 System.Diagnostics.Debug.WriteLine(tempstr);
                                 returnMsg += tempstr;
                                 //排程第二阶段：找出含有未排程工单的生产计划单，不区分子单主单，直接排程；
                                 string sql2 = @"select  distinct b.fbillno,b.FPlanfinishdate from shworkbillentry a 
                                                left join icmo b on a.FEntrySelfz0373=b.FBillNo
                                                where a.fstatus <3 
                                                and a.fentryselfz0374 is null
                                                and b.fstatus<3
                                                and b.fmrpclosed=0 
                                                order by  b.fbillno";

                                 DataTable dtMoOther = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql2.ToString());
                                tempstr=string.Format("问题任务单共计{0}条；", dtMoOther.Rows.Count);
                                returnMsg += tempstr;
                                 foreach (DataRow itemOther in dtMoOther.Rows)
                                 {
                                     tempstr = string.Format("开始处理计划单:{0},完工日期{1}；", itemOther["fbillno"].ToString(), itemOther["FPlanfinishdate"].ToString());
                                     returnMsg += tempstr; 
                                     scheduleByFBillNoDatetime(itemOther["fbillno"].ToString(), Convert.ToDateTime(itemOther["FPlanfinishdate"]));
                                 }

                                LoggerHelper.Info(returnMsg);
                                AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new AchieveManageWeb.Models.LogContent(ViewData, "自动排产", returnMsg)); //日志记录
                                string result = "{\"msg\":\"排程计算完成!"+returnMsg+"\",\"success\":true}";
                                return Content(result);
            //}
            //catch (Exception ex)
            //{
            //    return Content("{\"msg\":\"出现异常," + ex.Message + "\",\"success\":false}");
            //}
            
        }

        private void doScheduleLoop(DataRow mochild,DateTime fday) {
            DateTime fdaychild = scheduleByFBillNoDatetime(mochild["FBillNo"].ToString(), fday);//对1级子任务单进行排程，并返回最迟备货期
            //判断itemchild任务单是否有子单，如有，进行递归排程
            object x = mochild["hasChild"];
            if (Convert.ToBoolean(x))            
            {
                DataTable dtmochildc = manufactureBLL.GetManufactureSubOrder(mochild["FBillNo"].ToString(), "FStatus<3", 2);
                foreach (DataRow item in dtmochildc.Rows)
                {
                    doScheduleLoop(item, fdaychild);
                }
            }
            else
            {
                return;
            }
           // return DateTime.Now;        
        }
        public ActionResult doAllSchedule_test2()  // 自动排程的排程按钮——对所有未完工订单进行排程
        {
            //string FPlanCommitDate = Request["FPlanCommitDate"] == null ? "" : Request["FPlanCommitDate"];
            //string FPlanFinishDate = Request["FPlanFinishDate"] == null ? "" : Request["FPlanFinishDate"];
            //DateTime TimeNow = Convert.ToDateTime(FPlanCommitDate);
            //DateTime TimeEnd = Convert.ToDateTime(FPlanFinishDate);
            string ScheduleStartDate = Request["FSDate"] == "" ? DateTime.Now.ToString() : Request["FSDate"];

            double CapacityCoefficient = Request["CapacityCoefficient"] == "" ? 0.7 : Convert.ToDouble(Request["CapacityCoefficient"]);
            if (ScheduleStartDate != "")
            {
                ScheduleStartDate = Convert.ToDateTime(ScheduleStartDate).Date.ToString();
            }

            DateTime TimeNow;
            string returnMsg = "排程处理任务单如下：";
            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("select FBillNo,FPlanCommitDate,FPlanFinishDate from ICMO where FStatus<3 and fmrpclosed=0 and FWORKTYPEID != 68");
            //sbsql.Append(" and Fbillno not like '%v_%'  ESCAPE   'v'  and  Fbillno not like '%v-%' ESCAPE   'v'");
            DataTable dtMoMain = SqlHelper.GetDataTable(SqlHelper.connStrK3, sbsql.ToString());
            returnMsg += string.Format("任务单共计{0}条；", dtMoMain.Rows.Count);

            //DataTable allbill = dtMoMain.Copy();
            //foreach (DataRow item in dtMoMain.Rows)
            //{
            //    //DataTable dtmochild = manufactureBLL.GetManufactureSubOrder_test1(item["FBillNo"].ToString(), "FStatus<3", 1);
            //    DataTable dtmochild = manufactureBLL.GetManufactureSubOrder_test1(item["FBillNo"].ToString(), "FStatus<3", 1);

            //    foreach (DataRow childbii in dtmochild.Rows)
            //    {
            //        allbill.ImportRow(childbii);
            //    }
            //}
            if (ScheduleStartDate == "")
            {
                //TimeNow = Convert.ToDateTime(allbill.Compute("Min(FPlanCommitDate)", "true"));
                TimeNow = DateTime.Now;
            }
            else
            {
                TimeNow = Convert.ToDateTime(ScheduleStartDate);
            }

            DataTable MainProcess = null;
            foreach (DataRow bill in dtMoMain.Rows)
            {
                DataTable process;
                process = scheduleByFBillNoDatetable(bill["FBillNo"].ToString(), bill, TimeNow);//1、获取主单数据
                // 将主单合并
                if (MainProcess == null)
                {
                    MainProcess = process;
                }
                else
                {
                    if (process != null && process.Rows.Count > 0)
                    {
                        foreach (DataRow dr in process.Rows)
                        {
                            MainProcess.ImportRow(dr);
                        }
                    }
                }
            }
            DataColumn dc = null;
            dc = MainProcess.Columns.Add("Crowd", Type.GetType("System.Double"));
            DataTable MainProcess_i = MainProcess.Clone();
            foreach (DataRow dr in MainProcess.Rows)
            {
                dr["Crowd"] = Convert.ToDouble(dr["ProcessFinishDate"]) / Convert.ToDouble(dr["ProcessTotalDay"]);
                MainProcess_i.ImportRow(dr);
            }
            // 对所有3个月内的订单进行排程
            DataTable MainProcessSchedule = ScheduleOfPareto(MainProcess_i);
            //  更新到数据库
            UpdateData(MainProcessSchedule, TimeNow);
            //int[] NumsOfOver = GetOverBills();
            //returnMsg += string.Format("存在产能超出上限的订单共计{0}条；", NumsOfOver[0]);
            //returnMsg += string.Format("存在产能超出上限的工序共计{0}条；", NumsOfOver[1]);


            string result = "{\"msg\":\"排程计算完成!" + returnMsg + "\",\"success\":true}";


            //string result = "{\"msg\":\"排程计算完成!" + returnMsg + "\",\"success\":true, \"MainBills\":{0}, \"Processes\":{1}}", NumsOfOver[0], NumsOfOver[1];



            return Content(result);






            ////dr["ProcessFinishDate"] = DateDiff(Convert.ToDateTime(dr["FPlanFinishDate"]), TimeNow);
            //// DateTime TimeNow = DateTime.Now;
            //StringBuilder sbsql = new StringBuilder();
            ////string returnMsg = "排程处理任务单如下：";
            //sbsql.Append("select  FItemID,FBillNo,FPlanCommitDate,FPlanFinishDate from ICMO where FStatus<3 and fmrpclosed=0 ");
            ////抽取主作业计划单,规则不包含-、_两种连接符
            //sbsql.Append(" and Fbillno not like '%v_%'  ESCAPE   'v'  and  Fbillno not like '%v-%' ESCAPE   'v'");
            //// sbsql.Append(" and FPlanCommitDate >= " + string.Format("'{0}'", TimeNow.ToString()) + "and FPlanFinishDate <=" + string.Format("'{0}'", TimeEnd.ToString()));

            //DataTable dtMoMain = SqlHelper.GetDataTable(SqlHelper.connStrK3, sbsql.ToString());
            //returnMsg += string.Format("主任务单共计{0}条；", dtMoMain.Rows.Count);
            //DataTable MainProcess = null;

            ////对于每个主任务单，首先对其工单进行排程，然后获取其分层的未完工的子生产任务单；
            //foreach (DataRow item in dtMoMain.Rows)
            //{
            //    System.Diagnostics.Debug.WriteLine("开始处理单据:" + item["FBillNo"].ToString());
            //    returnMsg += item["FBillNo"].ToString() + ";";
            //    //DateTime fday = FPlanFinishDate.AddDays(-MOclTime);//考虑提前完工日期；
            //    DataTable process;
            //    process = scheduleByFBillNoDatetable(item["FBillNo"].ToString(), item, TimeNow);//1、获取主单数据
            //    // 将主单合并
            //    if (MainProcess == null)
            //    {
            //        MainProcess = process;
            //    }
            //    else
            //    {
            //        if (process != null && process.Rows.Count > 0)
            //        {
            //            foreach (DataRow dr in process.Rows)
            //            {
            //                MainProcess.ImportRow(dr);
            //            }
            //        }
            //    }
            //    //获取所有1级子单
            //    DataTable dtmochild = manufactureBLL.GetManufactureSubOrder_test(item["FBillNo"].ToString(), "FStatus<3", 1, TimeNow, TimeEnd);

            //    foreach (DataRow itemchild in dtmochild.Rows)
            //    {
            //        DataTable fdaychild = scheduleByFBillNoDatetable(itemchild["FBillNo"].ToString(), itemchild, TimeNow);
            //        if (fdaychild != null && fdaychild.Rows.Count > 0)
            //        {
            //            foreach (DataRow dr in fdaychild.Rows)
            //            {
            //                MainProcess.ImportRow(dr);
            //            }
            //        }
            //        //returnMsg += "处理子单:" + itemchild["FBillNo"].ToString() + ",完工日期:" + fday.ToString() + ";";
            //        //doScheduleLoop(itemchild, fday);

            //    }
            //}
            ///*DataColumn dc = null;
            //dc = MainProcess.Columns.Add("ProcessFinishDate", Type.GetType("System.Int32"));
            //dc = MainProcess.Columns.Add("ProcessOfDay", Type.GetType("System.Int32"));
            //dc = MainProcess.Columns.Add("ProcessTotalDay", Type.GetType("System.Int32"));*/
            //DataColumn dc = null;
            //dc = MainProcess.Columns.Add("Crowd", Type.GetType("System.Double"));
            //DataTable MainProcess_i = MainProcess.Clone();

            ////foreach (DataRow dr in MainProcess.Rows)
            ////{
            ////    if (Convert.ToDateTime(dr["FPlanFinishDate"]) <= TimeNow.AddDays(90) && Convert.ToDateTime(dr["FPlanCommitDate"]) >= TimeNow)
            ////    {
            ////        /*dr["ProcessFinishDate"] = DateDiff(Convert.ToDateTime(dr["FPlanFinishDate"]), TimeNow);
            ////        dr["ProcessOfDay"] = Convert.ToInt32(dr["ftotalworktime"]) / Convert.ToInt32(dr["ProcessTime"]);*/
            ////        dr["Crowd"] = Convert.ToDouble(dr["ProcessFinishDate"]) / Convert.ToDouble(dr["ProcessTotalDay"]);
            ////        MainProcess_i.ImportRow(dr);
            ////    }
            ////}

            //foreach (DataRow dr in MainProcess.Rows)
            //{
            //    /*dr["ProcessFinishDate"] = DateDiff(Convert.ToDateTime(dr["FPlanFinishDate"]), TimeNow);
            //    dr["ProcessOfDay"] = Convert.ToInt32(dr["ftotalworktime"]) / Convert.ToInt32(dr["ProcessTime"]);*/
            //    dr["Crowd"] = Convert.ToDouble(dr["ProcessFinishDate"]) / Convert.ToDouble(dr["ProcessTotalDay"]);
            //    MainProcess_i.ImportRow(dr);
            //}
            //// 对所有3个月内的订单进行排程
            //DataTable MainProcessSchedule = ScheduleOfPareto(MainProcess_i);
            ////  更新到数据库
            //UpdateData(MainProcessSchedule, TimeNow);


            ///*Save_Table(MainProcess_i, "D:/datatable.bin");
            //DataTable M = MainProcess_i.DefaultView.ToTable(false, "FOperID");
            //DataTable X1 = MainProcess_i.DefaultView.ToTable(true, "FOperID");
            //Int32 S3 = X1.Rows.Count;*/


            ///*string tempstr = "根据主单进行排程处理完毕！进入排程第二阶段：找出含有未排程工单的生产计划单，不区分子单主单，直接排程；";
            //System.Diagnostics.Debug.WriteLine(tempstr);
            //returnMsg += tempstr;
            ////排程第二阶段：找出含有未排程工单的生产计划单，不区分子单主单，直接排程；
            //string sql2 = @"select  distinct b.fbillno,b.FPlanfinishdate from shworkbillentry a 
            //                                    left join icmo b on a.FEntrySelfz0373=b.FBillNo
            //                                    where a.fstatus <3 
            //                                    and a.fentryselfz0374 is null
            //                                    and b.fstatus<3
            //                                    and b.fmrpclosed=0 
            //                                    order by  b.fbillno";

            //DataTable dtMoOther = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql2.ToString());
            //tempstr = string.Format("问题任务单共计{0}条；", dtMoOther.Rows.Count);
            //returnMsg += tempstr;
            //foreach (DataRow itemOther in dtMoOther.Rows)
            //{
            //    tempstr = string.Format("开始处理计划单:{0},完工日期{1}；", itemOther["fbillno"].ToString(), itemOther["FPlanfinishdate"].ToString());
            //    returnMsg += tempstr;
            //    scheduleByFBillNoDatetime(itemOther["fbillno"].ToString(), Convert.ToDateTime(itemOther["FPlanfinishdate"]));
            //}

            //LoggerHelper.Info(returnMsg);
            //AchieveManageWeb.Models.ActionFilters.LoggerHelper.Notes(new AchieveManageWeb.Models.LogContent(ViewData, "自动排产", returnMsg)); //日志记录
            //*/
            //string result = "{\"msg\":\"排程计算完成!" + returnMsg + "\",\"success\":true}";
            //return Content(result);
            ////}
            ////catch (Exception ex)
            ////{
            ////    return Content("{\"msg\":\"出现异常," + ex.Message + "\",\"success\":false}");
            ////}

        } // 自动排程里的排程功能

        public DataTable scheduleByFBillNoDatetable(string FBillNo, DataRow item, DateTime TimeNow)
        {
            SqlParameter[] paras = new SqlParameter[]{ 
                new SqlParameter("@FBillNo", SqlDbType.VarChar) { Value = FBillNo} 
            };
            string sql = "select fwbinterid,fentryid,FOperID, ftotalworktime,FEntrySelfz0374,FEntrySelfz0375,FEntrySelfz0376,FEntrySelfz0377  from shworkbillentry  where FEntrySelfz0373=@FBillNo and  fstatus<3 and FEntrySelfz0376 is not null and FEntrySelfz0377 is not null order by fentryid asc";
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, sql, paras);  // 从数据库索取sql命令的数据
            //DateTime fpday = finishDate;
            //DateTime fpdayTemp;
            DataColumn dc = null;
            DateTime FPlanCommitDate = Convert.ToDateTime(item["FPlanCommitDate"]);
            DateTime FPlanFinishDate = Convert.ToDateTime(item["FPlanFinishDate"]);
            dc = dt.Columns.Add("id", Type.GetType("System.String"));
            dc = dt.Columns.Add("FPlanCommitDate", Type.GetType("System.DateTime"));
            dc = dt.Columns.Add("FPlanFinishDate", Type.GetType("System.DateTime"));
            dc = dt.Columns.Add("ProcessTime", Type.GetType("System.Int32"));
            dc = dt.Columns.Add("ProcessTotalDay", Type.GetType("System.Int32"));
            dc = dt.Columns.Add("ProcessFinishDate", Type.GetType("System.Int32"));
            dc = dt.Columns.Add("ProcessPerDay", Type.GetType("System.Double"));

            Int32 ProcessTotalDay = 0;
            foreach (DataRow dr in dt.Rows)
            {
                dr["ProcessTime"] = DateDiff(Convert.ToDateTime(dr["FEntrySelfz0377"]), Convert.ToDateTime(dr["FEntrySelfz0376"]));
                ProcessTotalDay = ProcessTotalDay + Convert.ToInt32(dr["ProcessTime"]);
            }

            foreach (DataRow dr in dt.Rows)
            {
                dr["id"] = FBillNo;
                dr["FPlanCommitDate"] = FPlanCommitDate;
                dr["FPlanFinishDate"] = FPlanFinishDate;
                dr["ProcessFinishDate"] = DateDiff(Convert.ToDateTime(dr["FPlanFinishDate"]), TimeNow);
                dr["ProcessPerDay"] = Convert.ToDouble(dr["ftotalworktime"]) / Convert.ToInt32(dr["ProcessTime"]);
                dr["ProcessTotalDay"] = ProcessTotalDay;
            }
            return dt;
        }

        public DataTable scheduleByFBillNoDatetable(string FBillNo, DataRow item)
        {
            SqlParameter[] paras = new SqlParameter[]{ 
                new SqlParameter("@FBillNo", SqlDbType.VarChar) { Value = FBillNo} 
            };
            string sql = "select FOperNote,FEntrySelfz0374,FEntrySelfz0375,FEntrySelfz0376,FEntrySelfz0377  from shworkbillentry  where FEntrySelfz0373=@FBillNo and  fstatus<3 order by fentryid asc";
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, sql, paras);  // 从数据库索取sql命令的数据
            //DateTime fpday = finishDate;
            //DateTime fpdayTemp;
            DataColumn dc = null;
            //DateTime FPlanCommitDate = Convert.ToDateTime(item["FPlanCommitDate"]);
            //DateTime FPlanFinishDate = Convert.ToDateTime(item["FPlanFinishDate"]);
            dc = dt.Columns.Add("id", Type.GetType("System.String"));
            //dc = dt.Columns.Add("FPlanCommitDate", Type.GetType("System.DateTime"));
            //dc = dt.Columns.Add("FPlanFinishDate", Type.GetType("System.DateTime"));

            foreach (DataRow dr in dt.Rows)
            {
                dr["id"] = FBillNo;
                //dr["FPlanCommitDate"] = FPlanCommitDate;
                //dr["FPlanFinishDate"] = FPlanFinishDate;
            }
            return dt;
        }


        public static Int32 DateDiff(DateTime DateTime1, DateTime DateTime2)
        {
            //string dateDiff = null;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            Int32 nDays = ts.Days + 1;

            //dateDiff = ts.Days.ToString() + "天" + ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分钟" + ts.Seconds.ToString() + "秒";
            return nDays;

            #region note
            //C#中使用TimeSpan计算两个时间的差值
            //可以反加两个日期之间任何一个时间单位。
            //TimeSpan ts = Date1 - Date2;
            //double dDays = ts.TotalDays;//带小数的天数，比如1天12小时结果就是1.5
            //int nDays = ts.Days;//整数天数，1天12小时或者1天20小时结果都是1 
            #endregion
        }

        public static List<List<String>> Index(DataTable dt)
        {
            Int32 popSize = dt.Rows.Count;
            int[] rankedIndiv = new int[popSize];
            int[] nondominatedRank = new int[popSize];
            Int32 rankCounter = 1;
            List<List<String>> Index = new List<List<String>>();
            while (rankedIndiv.Sum() < popSize)
            {
                List<String> list1 = new List<String>();
                for (int i = 0; i < popSize; i++)
                {
                    if (rankedIndiv[i] == 1)
                    {
                        continue;
                    }
                    Int32 dominates = 1;
                    for (int j = 0; j < popSize; j++)
                    {
                        if (rankedIndiv[j] == 1 || i == j)
                        {
                            continue;
                        }
                        if (Convert.ToInt32(dt.Rows[j]["ProcessFinishDate"]) <= Convert.ToInt32(dt.Rows[i]["ProcessFinishDate"]) && Convert.ToInt32(dt.Rows[j]["Crowd"]) <= Convert.ToInt32(dt.Rows[i]["Crowd"]))
                        {
                            if (Convert.ToInt32(dt.Rows[j]["ProcessFinishDate"]) < Convert.ToInt32(dt.Rows[i]["ProcessFinishDate"]) || Convert.ToInt32(dt.Rows[j]["Crowd"]) < Convert.ToInt32(dt.Rows[i]["Crowd"]))
                            {
                                dominates = 0;
                                break;
                            }
                        }
                    }
                    if (dominates == 1)
                    {
                        nondominatedRank[i] = rankCounter;
                        rankedIndiv[i] = 1;
                        list1.Add(Convert.ToString(dt.Rows[i]["id"]));
                    }

                }
                Index.Add(list1);
                rankCounter = rankCounter + 1;
            }
            return Index;


        }
        public Dictionary<int, double[]> set_dictionary(DataTable dt, Int32 days, Double default_value)
        {
            days = days + 1;
            Dictionary<int, double[]> d = new Dictionary<int, double[]>();
            double[] rows = new double[days];
            for (int i = 0; i < days; i++)
            {
                rows[i] = default_value;
            }
            foreach (DataRow dr in dt.Rows)
            {
                d.Add(Convert.ToInt32(dr["FOperID"]), rows);
            }

            return d;
        }

        public Dictionary<int, double[]> set_dictionary(Int32 days)
        {

            days = days + 1;
            Dictionary<int, double[]> d = new Dictionary<int, double[]>();
            d.Add(40003, CraftOfDay(80, days));
            d.Add(40004, CraftOfDay(40, days));
            d.Add(40015, CraftOfDay(233, days));
            d.Add(40016, CraftOfDay(13, days));
            d.Add(40017, CraftOfDay(29, days));
            d.Add(40018, CraftOfDay(10, days));
            d.Add(40019, CraftOfDay(10, days));
            d.Add(40020, CraftOfDay(14, days));
            d.Add(40021, CraftOfDay(26, days));
            d.Add(40022, CraftOfDay(400, days));
            d.Add(40023, CraftOfDay(10, days));
            d.Add(40024, CraftOfDay(10, days));
            d.Add(40025, CraftOfDay(20, days));
            d.Add(40026, CraftOfDay(20, days));
            d.Add(40027, CraftOfDay(30, days));
            d.Add(40028, CraftOfDay(10, days));
            d.Add(40029, CraftOfDay(28, days));
            d.Add(40031, CraftOfDay(10, days));
            d.Add(40032, CraftOfDay(10, days));
            d.Add(40033, CraftOfDay(10, days));
            d.Add(40034, CraftOfDay(10, days));
            d.Add(40035, CraftOfDay(10, days));
            d.Add(40036, CraftOfDay(10, days));
            d.Add(40037, CraftOfDay(10, days));
            d.Add(40038, CraftOfDay(10, days));
            d.Add(40039, CraftOfDay(10, days));
            d.Add(40040, CraftOfDay(10, days));
            d.Add(40041, CraftOfDay(10, days));
            d.Add(40042, CraftOfDay(10, days));
            d.Add(40043, CraftOfDay(10, days));
            d.Add(40044, CraftOfDay(10, days));
            d.Add(40045, CraftOfDay(10, days));
            d.Add(40046, CraftOfDay(10, days));
            d.Add(40047, CraftOfDay(10, days));
            d.Add(40048, CraftOfDay(10, days));
            d.Add(40049, CraftOfDay(10, days));
            d.Add(40050, CraftOfDay(10, days));

            return d;
        }

        public Double[] CraftOfDay(Double default_value, Int32 days)
        {
            double[] rows = new double[days];
            for (int i = 0; i < days; i++)
            {
                rows[i] = default_value;
            }
            return rows;
        }

        public DataTable set_datatable(String[] columns, String[] columns_type, Int32 rows)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < columns.Length; i++)
            {
                string co = columns[i].ToString();
                string type = columns_type[i].ToString();
                dt.Columns.Add(co, Type.GetType(type));
            }
            for (int i = 0; i < rows; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < columns.Length; j++)
                {
                    string co = columns[j].ToString();
                    if (columns_type[j] == "System.Int32")
                    {
                        dr[co] = 0;
                    }
                    if (columns_type[j] == "System.Int32[]")
                    {
                        dr[co] = new int[50] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
                    }
                    if (columns_type[j] == "System.Double[]")
                    {
                        dr[co] = new double[50] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable set_datatable(Int32 rows, Int32 columns, Int32 default_value)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < columns; i++)
            {
                string co = (i + 1).ToString();
                dt.Columns.Add(co);
            }
            for (int i = 0; i < rows; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < columns; j++)
                {
                    string co = (j + 1).ToString();
                    dr[co] = default_value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable ScheduleOfPareto(DataTable MainProcess_i)
        {
            Int32 SizeOrderNumber = MainProcess_i.Rows.Count;
            DataTable Pareto = MainProcess_i.DefaultView.ToTable(true, new string[] { "id", "ProcessFinishDate", "Crowd" });  // 获取订单编号， 完工时间，宽放系数；
            List<List<String>> index = Index(Pareto);  // 帕累托解；
            DataTable M = MainProcess_i.DefaultView.ToTable(false, "FOperID");  // 所有工序编号；
            DataTable X1 = MainProcess_i.DefaultView.ToTable(true, "FOperID");  // 工序编号去重；
            X1.DefaultView.Sort = "FOperID ASC";
            X1 = X1.DefaultView.ToTable();

            Int32 S3 = X1.Rows.Count;  // 工序种类数量；
            Dictionary<int, double[]> Solution = set_dictionary(X1, 1000, 0);  // 实际生产负荷；
            // Dictionary<int, double[]> CheckSolution = set_dictionary(X1, 100, 50);  // 设置剩余符合检查表；
            Dictionary<int, double[]> CheckSolution = set_dictionary(1000);  // 设置剩余符合检查表；

            string[] co = { "OrderNumber", "OrderStartEnd", "OrderDeadline", "OrderProc", "OrderProcDay", "OrderProcTime", "OrderProcStartEnd", "OrderOverTime", "CanNotFinish", "fwbinterid", "LengthBill", "OverLimit", "OverLimitList", "OverValues" };
            string[] co_type = { "System.String", "System.Int32[]", "System.Int32", "System.Int32[]", "System.Int32[]", "System.Double[]", "System.Int32[]", "System.Int32", "System.Int32", "System.Int32[]", "System.Int32", "System.Int32", "System.Int32[]", "System.Double[]" };
            DataTable GantData = set_datatable(co, co_type, Pareto.Rows.Count);
            Int32 q = 0;

            for (int i = 0; i < index.Count; i++)
            {
                List<String> ParetoFront_i = index[i];
                DataTable newdt = Pareto.Clone();
                for (int m = 0; m < ParetoFront_i.Count; m++)
                {
                    StringBuilder sbsql = new StringBuilder();
                    sbsql.Append("id=" + String.Format("'{0}'", ParetoFront_i[m].ToString()));
                    DataRow[] spliterros = Pareto.Select(sbsql.ToString());
                    newdt.ImportRow(spliterros[0]);
                }
                DataView dv = new DataView(newdt);
                dv.Sort = "ProcessFinishDate asc";
                newdt = dv.ToTable();
                foreach (DataRow dr in newdt.Rows)
                {
                    GantData.Rows[q]["OrderNumber"] = dr["id"];
                    String id = String.Format("id={0}", String.Format("'{0}'", dr["id"].ToString()));
                    DataRow[] ProcTimeAbi = MainProcess_i.Select(id);
                    Int32 a = 0;
                    for (int k = 0; k < ProcTimeAbi.Length; k++)
                    {
                        GantData.Rows[q]["LengthBill"] = ProcTimeAbi.Length;

                        Int32[] fwbinterid = (int[])GantData.Rows[q]["fwbinterid"];
                        fwbinterid[k] = (int)ProcTimeAbi[k]["fwbinterid"];
                        GantData.Rows[q]["fwbinterid"] = fwbinterid;

                        Int32[] OrderProc = (int[])GantData.Rows[q]["OrderProc"];
                        OrderProc[k] = (int)ProcTimeAbi[k]["FOperID"];
                        GantData.Rows[q]["OrderProc"] = OrderProc;

                        GantData.Rows[q]["OrderDeadline"] = (int)ProcTimeAbi[0]["ProcessFinishDate"];

                        Double[] ProcessPerDay = (double[])GantData.Rows[q]["OrderProcTime"];
                        ProcessPerDay[k] = Convert.ToDouble(ProcTimeAbi[k]["ProcessPerDay"]);
                        GantData.Rows[q]["OrderProcTime"] = ProcessPerDay;

                        Int32[] ProcessTime = (int[])GantData.Rows[q]["OrderProcDay"];
                        ProcessTime[k] = (int)ProcTimeAbi[k]["ProcessTime"] - 1;
                        GantData.Rows[q]["OrderProcDay"] = ProcessTime;
                        bool NormalCondition1 = true;
                        bool NormalCondition2 = true;
                        double OverCapacityValue = 0;
                        if (a + ProcessTime[k] > 1000)
                        {
                            GantData.Rows[q]["CanNotFinish"] = 1;
                            continue;
                        }


                        for (int day = a; day <= a + ProcessTime[k]; day++)
                        {
                            if (CheckSolution[OrderProc[k]][day] - ProcessPerDay[k] < 0)
                            {
                                OverCapacityValue = ProcessPerDay[k] - CheckSolution[OrderProc[k]][day];
                                NormalCondition1 = false;
                                break;
                            }
                        }
                        if (NormalCondition1)
                        {
                            for (int day = a; day <= a + ProcessTime[k]; day++)
                            {
                                Solution[OrderProc[k]][day] += ProcessPerDay[k];
                                CheckSolution[OrderProc[k]][day] -= ProcessPerDay[k];
                            }
                            if (ProcessTime[k] == 0)
                            {
                                Int32[] StartEnd = (int[])GantData.Rows[q]["OrderProcStartEnd"];
                                StartEnd[2 * k] = a;
                                StartEnd[2 * k + 1] = a;
                                GantData.Rows[q]["OrderProcStartEnd"] = StartEnd;
                            }
                            else
                            {
                                Int32[] StartEnd = (int[])GantData.Rows[q]["OrderProcStartEnd"];
                                StartEnd[2 * k] = a;
                                a += ProcessTime[k];
                                StartEnd[2 * k + 1] = a;
                                GantData.Rows[q]["OrderProcStartEnd"] = StartEnd;
                            }
                        }
                        else
                        {
                            for (int day = a; day <= a + ProcessTime[k]; day++)
                            {
                                if (CheckSolution[OrderProc[k]][day] <= 0)
                                {
                                    NormalCondition2 = false;
                                    break;
                                }
                                if (CheckSolution[OrderProc[k]][day] > 0)
                                {
                                    continue;
                                }
                            }
                        }
                        if (NormalCondition1 == false && NormalCondition2)
                        {
                            GantData.Rows[q]["OverLimit"] = 1;
                            Int32[] LimitList = (int[])GantData.Rows[q]["OverLimitList"];
                            LimitList[k] = 1;
                            GantData.Rows[q]["OverLimitList"] = LimitList;

                            Double[] OverVlues = (double[])GantData.Rows[q]["OverValues"];
                            OverVlues[k] = OverCapacityValue;
                            GantData.Rows[q]["OverValues"] = OverVlues;
                            for (int day = a; day <= a + ProcessTime[k]; day++)
                            {
                                Solution[OrderProc[k]][day] += ProcessPerDay[k];
                                CheckSolution[OrderProc[k]][day] -= ProcessPerDay[k];
                            }
                            if (ProcessTime[k] == 0)
                            {
                                Int32[] StartEnd = (int[])GantData.Rows[q]["OrderProcStartEnd"];
                                StartEnd[2 * k] = a;
                                StartEnd[2 * k + 1] = a;
                                GantData.Rows[q]["OrderProcStartEnd"] = StartEnd;
                            }
                            else
                            {
                                Int32[] StartEnd = (int[])GantData.Rows[q]["OrderProcStartEnd"];
                                StartEnd[2 * k] = a;
                                a += ProcessTime[k];
                                StartEnd[2 * k + 1] = a;
                                GantData.Rows[q]["OrderProcStartEnd"] = StartEnd;
                            }
                        }
                        if (NormalCondition1 == false && NormalCondition2 == false)
                        {
                            Int32 n = 0;
                            if (ProcessTime[k] == 0)
                            {
                                n = 1;
                            }
                            else
                            {
                                n = ProcessTime[k] + 1;
                            }
                            List<int> x = new List<int>();
                            for (int p = a; p <= 100; p++)
                            {
                                if (CheckSolution[OrderProc[k]][p] >= 0)
                                {
                                    Solution[OrderProc[k]][p] += ProcessPerDay[k];
                                    CheckSolution[OrderProc[k]][p] -= ProcessPerDay[k];
                                    n -= 1;
                                    x.Add(p);
                                }
                                if (n == 0)
                                {
                                    Int32[] StartEnd = (int[])GantData.Rows[q]["OrderProcStartEnd"];
                                    StartEnd[2 * k] = x.Min();
                                    StartEnd[2 * k + 1] = p;
                                    GantData.Rows[q]["OrderProcStartEnd"] = StartEnd;
                                    a = p;
                                    break;
                                }
                                if (p == 100 && n != 0)
                                {
                                    GantData.Rows[q]["CanNotFinish"] = 1;
                                    a = p;
                                    continue;
                                }
                            }
                        }
                    }
                    Int32[] OrderProcStartEnd = (int[])GantData.Rows[q]["OrderProcStartEnd"];
                    Int32[] OrderStartEnd = (int[])GantData.Rows[q]["OrderStartEnd"];
                    OrderStartEnd[0] = OrderProcStartEnd.Min();
                    OrderStartEnd[1] = OrderProcStartEnd.Max();
                    GantData.Rows[q]["OrderStartEnd"] = OrderStartEnd;
                    q = q + 1;
                }

            }
            return GantData;
        }
        public DataTable getbills()
        {
            string returnMsg = "排程处理任务单如下：";
            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("select  FItemID,FBillNo,FPlanCommitDate,FPlanFinishDate from ICMO where FStatus<3 and fmrpclosed=0 and FWORKTYPEID != 68");
            //sbsql.Append(" and Fbillno not like '%v_%'  ESCAPE   'v'  and  Fbillno not like '%v-%' ESCAPE   'v'");
            DataTable dtMoMain = SqlHelper.GetDataTable(SqlHelper.connStrK3, sbsql.ToString());
            returnMsg += string.Format("主任务单共计{0}条；", dtMoMain.Rows.Count);

            //DataTable allbill = dtMoMain.Copy();
            //foreach (DataRow item in dtMoMain.Rows)
            //{
            //    //DataTable dtmochild = manufactureBLL.GetManufactureSubOrder_test1(item["FBillNo"].ToString(), "FStatus<3", 1);
            //    DataTable dtmochild = manufactureBLL.GetManufactureSubOrder_test1(item["FBillNo"].ToString(), "FStatus<3", 1);

            //    foreach (DataRow childbii in dtmochild.Rows)
            //    {
            //        allbill.ImportRow(childbii);
            //    }
            //}

            DataTable MainProcess = null;
            foreach (DataRow bill in dtMoMain.Rows)
            {
                DataTable process;
                process = scheduleByFBillNoDatetable(bill["FBillNo"].ToString(), bill);//1、获取主单数据
                // 将主单合并
                if (MainProcess == null)
                {
                    MainProcess = process;
                }
                else
                {
                    if (process != null && process.Rows.Count > 0)
                    {
                        foreach (DataRow dr in process.Rows)
                        {
                            MainProcess.ImportRow(dr);
                        }
                    }
                }
            }
            //DataColumn dc = null;
            //dc = MainProcess.Columns.Add("Crowd", Type.GetType("System.Double"));
            //DataTable MainProcess_i = MainProcess.Clone();
            return MainProcess;
        }


        public FileStreamResult exportExecl()
        {

            //string dg_type = Request.Params["dg_type"];//获取数据视图类型 
            //string cycle = Request.Params["cycle"];
            //bool removefinished = Convert.ToBoolean(Request.Params["removefinished"]);
            //int adjust = Convert.ToInt32(Request.Params["adjust"]);
            //string department = Request.Params["department"];
            //string schedule = Request.Params["schedule"];
            //bool classifyOper = Convert.ToBoolean(Request.Params["classifyOper"]);
            //DateTime startDate, endDate;
            //DataTable dt = new DataTable();//输出表格
            //创建Excel文件的对象
            NPOI.HSSF.UserModel.HSSFWorkbook book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            //添加一个sheet
            NPOI.SS.UserModel.ISheet sheet1 = book.CreateSheet("Sheet1");
            NPOI.SS.UserModel.IRow row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue("订单编号");
            row1.CreateCell(1).SetCellValue("工序名称");
            row1.CreateCell(2).SetCellValue("预排开工时间");
            row1.CreateCell(3).SetCellValue("预排完工时间");
            row1.CreateCell(4).SetCellValue("计划开工时间");
            row1.CreateCell(5).SetCellValue("计划完工时间");
            DataTable dt = getbills();
            int k = 1;
            foreach (DataRow item in dt.Rows)
            {
                NPOI.SS.UserModel.IRow rowtemp = sheet1.CreateRow(k);
                rowtemp.CreateCell(0).SetCellValue(item[5].ToString());
                rowtemp.CreateCell(1).SetCellValue(item[0].ToString());
                rowtemp.CreateCell(2).SetCellValue(item[1].ToString());
                rowtemp.CreateCell(3).SetCellValue(item[2].ToString());
                rowtemp.CreateCell(4).SetCellValue(item[3].ToString());
                rowtemp.CreateCell(5).SetCellValue(item[4].ToString());

                //for (int j = 0; j < 6; j++)
                //{
                //    rowtemp.CreateCell(j).SetCellValue(item[j].ToString());
                //}
                k++;
            }


            // 写入到客户端 
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            book.Write(ms);
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            return File(ms, "application/vnd.ms-excel", HttpUtility.UrlEncode("导出数据", Encoding.UTF8).ToString() + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");

        }




         /// <summary>
         /// 排程方式2，扫描所有生产任务单，不考虑子单主单，按当前任务单进行倒排；
         /// 用于在按主单倒排后，检查没有主单的任务单，根据其计划完工日期进行倒排；
         /// </summary>
        private void doScheduleSubICMO() { 
        
        }
    }
}
