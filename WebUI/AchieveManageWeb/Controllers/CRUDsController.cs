﻿using AchieveBLL;
using AchieveCommon;
using AchieveEntity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;


namespace AchieveManageWeb.Controllers
{
    [AchieveManageWeb.App_Start.JudgmentLogin]
    public class CRUDsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
////引用序列化、反序列化JSON字符串用到的空间
//using Newtonsoft.Json;  
//using Newtonsoft.Json.Linq; 
////定义一个JSON字符串 
//string jsonText = "[{'a':'aaa','b':'bbb','c':'ccc'},{'a':'aaa2','b':'bbb2','c':'ccc2'}]";  
////反序列化JSON字符串,将JSON字符串转换成LIST列表
//List<Customer> _list = JsonConvert.DeserializeObject<List<Customer>>(jsonText);  
////读取列表中的值
//Console.WriteLine(_list[1].a);  
//foreach (Customer c in _list)  
//{  
//    Console.WriteLine(c.c);  

//————————————————


        public ActionResult GetAllNotesInfo()
        {
            string strWhere = "1=1";
            string sort = Request["sort"] == null ? "id" : Request["sort"];
            string order = Request["order"] == null ? "asc" : Request["order"];
           // string typeID = Request["typeID"] == null ? "" : Request["typeID"];
            //首先获取前台传递过来的参数
            int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
            int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);
            string postData = Request["postData"];
            object pd = JsonConvert.DeserializeObject(postData);
            //反序列化JSON字符串
            JObject jo = JObject.Parse(postData);
            string[] values = jo.Properties().Select(item => item.Value.ToString()).ToArray();
            foreach (JToken child in jo.Children())
            {
                var property1 = child as JProperty;
               // MessageBox.Show(property1.Name + ":" + property1.Value);
                System.Diagnostics.Debug.WriteLine(property1.Name + ":" + property1.Value);
} 
 
            JsonReader reader = new JsonTextReader(new StringReader(postData));
            string lastType = "";
            string lastValue="";
            while (reader.Read())
            {
              string  thisType = reader.TokenType.ToString();
                var thisValue = reader.Value;
                if (thisValue==null)
              {
                  continue;
              }
              
              if (lastType=="PropertyName")
              {
                  switch (lastValue)
                  {
                      case "typeID":
                          if (thisValue!=null)
                          { 
                          strWhere += " and typeID='" + thisValue+"'";
                          }
                          break;
                      case "citeID":
                          if (thisValue != null)
                          {
                              strWhere += " and citeID='" + thisValue + "'";
                          }
                          break;
                      default:
                          break;
                  }
              }
            
                  lastType = thisType;
                  lastValue = thisValue.ToString();
             
                System.Diagnostics.Debug.WriteLine(reader.TokenType + "\t\t" + reader.ValueType + "\t\t" + reader.Value + "\r\n");
            }
            //JArray ja = (JArray)JsonConvert.DeserializeObject(postData);

            //将反序列化的JSON字符串转换成对象
            //JObject o = (JObject)ja[1];

            //读取对象中的各项值
            //Console.WriteLine(o["a"]);
            //Console.WriteLine(ja[1]["a"]); 


       
            int totalCount = 0;   //输出参数 

            DataTable dt = SqlPagerHelper.GetPagerBySQL(SqlHelper.connStr, "tbNote", "*", sort + " " + order, pagesize, pageindex, strWhere, out totalCount);

            //var jsonResult = new { total = totalCount.ToString(), rows = strJson };
            IsoDateTimeConverter timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";


          //  Console.WriteLine(JsonConvert.SerializeObject(dt, Formatting.Indented, timeFormat));
 
            string strJson = JsonConvert.SerializeObject(dt,Formatting.Indented,timeFormat);
           // string strJson = JsonHelper.ToJson(dt);
            return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        }

       

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;
                //NewsEntity NewsAdd = new NewsEntity();
                //NewsAdd.ftitle = Request["FTitle"];
                //NewsAdd.ftypeid = int.Parse(Request["FTypeId"]);
                //NewsAdd.fcontent = Request["FContent"];
                 string CreateBy = uInfo.AccountName;
                 DateTime CreateTime = DateTime.Now;
                string UpdateBy = uInfo.AccountName;
                string postData = Request["postData"];
                object pd = JsonConvert.DeserializeObject(postData);
                //反序列化JSON字符串
                JArray ja = (JArray)JsonConvert.DeserializeObject(postData);
                //将反序列化的JSON字符串转换成对象
                JObject o = (JObject)ja[0];
                System.Diagnostics.Debug.WriteLine(o["ID"]);

                //读取对象中的各项值
                Console.WriteLine(o["ID"]);
                Console.WriteLine(ja[0]["typeID"]); 
                string ID = Request["ID"];
                string typeID = Request["typeID"];
                string citeID = Request["citeID"];
               

                string sql = @"insert into tbNote(ID,typeID,citeID,noteContent,createTime,createBy,updateTime,updateBy,status)
                values('{0}','{1}','{2}','{3}','{4}','{5}','{4}','{6}',0)";
              //  sql = string.Format(sql, ID, typeID, citeID, noteContent, DateHelper.NowTime, CreateBy, UpdateBy);
                int id = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.connStr, sql));
                if (id > 0)
                {
                    return Content("{\"msg\":\"添加成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"添加失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }
        }

        /// <summary>
        /// 编辑页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult NewsEdit()
        {
            int id = 0;
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                id = int.Parse(Request["id"]);
                NewsEntity NewsEdit = new NewsBLL().GetModel(id);
                return View(NewsEdit);
            }
            return new EmptyResult();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditNews()
        {
            try
            {
                UserEntity uInfo = ViewData["Account"] as UserEntity;

                int id = Convert.ToInt32(Request["id"]);
                NewsEntity NewsEdit = new NewsBLL().GetModel(id);
                NewsEdit.ftitle = Request["FTitle"];
                NewsEdit.ftypeid = int.Parse(Request["FTypeId"]);
                NewsEdit.fcontent = Request["FContent"];
                NewsEdit.UpdateBy = uInfo.AccountName;
                NewsEdit.UpdateTime = DateTime.Now;
                int result = new NewsBLL().Update(NewsEdit);
                if (result > 0)
                {
                    return Content("{\"msg\":\"修改成功！\",\"success\":true}");
                }
                else
                {
                    return Content("{\"msg\":\"修改失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
            }
        }

        public ActionResult DelNewsByIDs()
        {
            try
            {
                string Ids = Request["IDs"] == null ? "" : Request["IDs"];
                if (!string.IsNullOrEmpty(Ids))
                {
                    if (new NewsBLL().DeleteList(Ids))
                    {
                        return Content("{\"msg\":\"删除成功！\",\"success\":true}");
                    }
                    else
                    {
                        return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                    }
                }
                else
                {
                    return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"删除失败," + ex.Message + "\",\"success\":false}");
            }
        }

        /// <summary>
        /// 待完善  暂不使用
        /// </summary>
        /// <returns></returns>
        public ActionResult GetNewsList()
        {
            int newstypeid = int.Parse(Request["newstype"] ?? "0");
            string strWhere = " 1=1 and ftypeid =" + newstypeid;
            DataTable dt = new NewsBLL().GetList(10, strWhere, " id desc ");
            string strJson = "[";
            foreach (DataRow dr in dt.Rows)
            {
                strJson += "{\"text\":\"" + dr["ftitle"].ToString() + "\"},";
            }
            strJson = strJson.TrimEnd(',');
            strJson += "]";
            return Content(strJson);
        }
    }
}
