﻿using AchieveBLL;
using AchieveCommon;
using AchieveEntity;
using AchieveManageWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace AchieveManageWeb.Controllers
{
    public class DevController : Controller
    {
        //
        // GET: /Dev/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult systemFlow()
        {
            return View();
        }

        public ActionResult getSubInfo()
        {
            int InfoID = Request["InfoID"] == null ? 0 : Convert.ToInt32(Request["InfoID"]);
            DataTable dt = BasicInfoBLL.getSubInfo(InfoID);
            string strJson = JsonHelper.ToJson(dt);
            string content = "{\"total\": " + dt.Rows.Count.ToString() + ",\"rows\":" + strJson + "}";
            return Content(content);
        }

        /// <summary>
        /// 基础资料通用查询
        /// </summary>
        /// <returns></returns>
        public ActionResult getBasicInfo()
        {
            int InfoID = Request["InfoID"] == null ? 0 : Convert.ToInt32(Request["InfoID"]);
            string type = Request["type"] == null ? "itself" : Request["type"];
            DataTable dt=new DataTable();
            switch (type)
            {
                case "itself":
                    BasicInfoBLL.getInfo(InfoID);
                    break;
                case "subItems":
                    dt = BasicInfoBLL.getSubInfo(InfoID);
                    break;
                case "parents":
                    dt = BasicInfoBLL.getParentsInfo(InfoID);
                break;
                default:
                    break;
            } 
            string strJson = JsonHelper.ToJson(dt);
            string content = "{\"total\": " + dt.Rows.Count.ToString() + ",\"rows\":" + strJson + "}";
            return Content(content);
        }
        public ActionResult addNewInfo()
        {

            int InfoID = Request["InfoID"] == null ? 0 : Convert.ToInt32(Request["InfoID"]);
            int ParentID = Request["ParentID"] == null ? 0 : Convert.ToInt32(Request["ParentID"]);
            int OrderID = Request["OrderID"] == null ? 0 : Convert.ToInt32(Request["OrderID"]);
            string InfoName = Request["InfoName"] == null ? "" : Request["InfoName"];
            string Remark = Request["Remark"] == null ? "" : Request["Remark"];
            int r = BasicInfoBLL.addNewInfo(InfoID, InfoName, ParentID, Remark, OrderID);
            if (r == 1)
            {
                return Content("{\"msg\":\"添加成功！\",\"success\":true}");
            }
            else
            {
                return Content("{\"msg\":\"添加失败！\",\"success\":false}");
            }
        }

        public ActionResult updateInfo()
        {

            int InfoID = Request["InfoID"] == null ? 0 : Convert.ToInt32(Request["InfoID"]);
            int ParentID = Request["ParentID"] == null ? 0 : Convert.ToInt32(Request["ParentID"]);
            int OrderID = Request["OrderID"] == null ? 0 : Convert.ToInt32(Request["OrderID"]);
            string InfoName = Request["InfoName"] == null ? "" : Request["InfoName"];
            string Remark = Request["Remark"] == null ? "" : Request["Remark"];
            int r = BasicInfoBLL.updateInfo(InfoID, InfoName, ParentID, Remark, OrderID);
            if (r == 1)
            {
                return Content("{\"msg\":\"修改成功！\",\"success\":true}");
            }
            else
            {
                return Content("{\"msg\":\"修改失败！\",\"success\":false}");
            }
        }
        public ActionResult deleteInfo()
        {
            int InfoID = Request["InfoID"] == null ? 0 : Convert.ToInt32(Request["InfoID"]);

            int r = BasicInfoBLL.deleteInfo(InfoID);
            if (r == 1)
            {
                return Content("{\"msg\":\"删除成功！\",\"success\":true}");
            }
            else
            {
                return Content("{\"msg\":\"删除失败！\",\"success\":false}");
            }
        }



    }
}
