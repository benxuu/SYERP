﻿using AchieveBLL;
using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;  
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
namespace AchieveManageWeb.Controllers
{
    [AchieveManageWeb.App_Start.JudgmentLogin]
    public class CodeController : Controller
    {
        //
        // GET: /Code/

        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 返回基础信息编码json，一般用于combobox选项，前端传入typeid 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCodebyTypeId()
        {
            int typeid = Request["typeid"] == null ? 0 : Convert.ToInt32( Request["typeid"]);
             //string sql=string.Format(" select codename,codeno from tbBasicInfoCode where typeid={0}",typeid);
            DataTable dt = CodeBLL.GetCodebyTypeId(typeid);
             return Content(JsonHelper.ToJson(dt));        
        }

        //public ActionResult GetAllCodeInfo()
        //{
        //    string strWhere = "1=1";
        //    string sort = Request["sort"] == null ? "Id" : Request["sort"];
        //    string order = Request["order"] == null ? "asc" : Request["order"];
        //    if (!string.IsNullOrEmpty(Request["CodeName"]) && !SqlInjection.GetString(Request["CodeName"]))
        //    {
        //        strWhere += " and Name like '%" + Request["CodeName"] + "%'";
        //    }
        //    //首先获取前台传递过来的参数
        //    int pageindex = Request["page"] == null ? 1 : Convert.ToInt32(Request["page"]);
        //    int pagesize = Request["rows"] == null ? 10 : Convert.ToInt32(Request["rows"]);
        //    int totalCount;   //输出参数
        //    string strJson = "";    //输出结果
        //    if (order.IndexOf(',') != -1)   //如果有","就是多列排序（不能拿列判断，列名中间可能有","符号）
        //    {
        //        //多列排序：
        //        //sort：ParentId,Sort,AddDate
        //        //order：asc,desc,asc
        //        string sortMulti = "";  //拼接排序条件，例：ParentId desc,Sort asc
        //        string[] sortArray = sort.Split(',');   //列名中间有","符号，这里也要出错。正常不会有
        //        string[] orderArray = order.Split(',');
        //        for (int i = 0; i < sortArray.Length; i++)
        //        {
        //            sortMulti += sortArray[i] + " " + orderArray[i] + ",";
        //        }
        //        strJson = new CodeBLL().GetPager("tbCode", "Id,Name,ParentId,Code,LinkAddress,Icon,Sort,CreateTime,CreateBy,UpdateTime,UpdateBy", sortMulti.Trim(','), pagesize, pageindex, strWhere, out totalCount);
        //    }
        //    else
        //    {
        //        strJson = new CodeBLL().GetPager("tbCode", "Id,Name,ParentId,Code,LinkAddress,Icon,Sort,CreateTime,CreateBy,UpdateTime,UpdateBy", sort + " " + order, pagesize, pageindex, strWhere, out totalCount);
        //    }
        //    var jsonResult = new { total = totalCount.ToString(), rows = strJson };
        //    return Content("{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}");
        //}

        /// <summary>
        /// 新增页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult CodeAdd()
        {
            return View();
        }

        /// <summary>
        /// 新增菜单
        /// </summary>
        /// <returns></returns>
        //public ActionResult AddCode()
        //{
        //    try
        //    {
        //        UserEntity uInfo = ViewData["Account"] as UserEntity;
        //        CodeEntity CodeAdd = new CodeEntity();
        //        CodeAdd.Name = Request["CodeName"];
        //        CodeAdd.Code = Request["CodeCode"];
        //        CodeAdd.LinkAddress = Request["CodeLinkAddress"];
        //        CodeAdd.Icon = Request["CodeIcon"];
        //        CodeAdd.Sort = int.Parse(Request["CodeSort"]);
        //        if (Request["CodeParentId"] != null && Request["CodeParentId"] != "")
        //        {
        //            CodeAdd.ParentId = Convert.ToInt32(Request["CodeParentId"]);
        //        }
        //        else
        //        {
        //            CodeAdd.ParentId = 0;
        //        }
        //        CodeAdd.CreateBy = uInfo.AccountName;
        //        CodeAdd.CreateTime = DateTime.Now;
        //        CodeAdd.UpdateBy = uInfo.AccountName;
        //        CodeAdd.UpdateTime = DateTime.Now;

        //        int CodeId = new CodeBLL().AddCode(CodeAdd);
        //        if (CodeId > 0)
        //        {
        //            return Content("{\"msg\":\"添加成功！\",\"success\":true}");
        //        }
        //        else
        //        {
        //            return Content("{\"msg\":\"添加失败！\",\"success\":false}");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content("{\"msg\":\"添加失败," + ex.Message + "\",\"success\":false}");
        //    }
        //}

        /// <summary>
        /// 编辑页面展示
        /// </summary>
        /// <returns></returns>
        public ActionResult CodeEdit()
        {
            return View();
        }

        /// <summary>
        /// 编辑菜单
        /// </summary>
        /// <returns></returns>
        //public ActionResult EditCode()
        //{
        //    try
        //    {
        //        UserEntity uInfo = ViewData["Account"] as UserEntity;
        //        int id = Convert.ToInt32(Request["id"]);
        //        string originalName = Request["originalName"];
        //        CodeEntity CodeEdit = new CodeEntity();
        //        CodeEdit.Id = id;
        //        CodeEdit.Name = Request["CodeName"];
        //        CodeEdit.Code = Request["CodeCode"];
        //        CodeEdit.Icon = Request["CodeIcon"];
        //        CodeEdit.Sort = int.Parse(Request["CodeSort"]);
        //        CodeEdit.LinkAddress = Request["CodeLinkAddress"];
        //        if (Request["CodeParentId"] != null && Request["CodeParentId"] != "")
        //        {
        //            CodeEdit.ParentId = Convert.ToInt32(Request["CodeParentId"]);
        //        }
        //        else
        //        {
        //            CodeEdit.ParentId = 0;   //根节点
        //        }
        //        //CodeEdit.CreateBy = uInfo.AccountName;
        //        //CodeEdit.CreateTime = DateTime.Now;
        //        CodeEdit.UpdateBy = uInfo.AccountName;
        //        CodeEdit.UpdateTime = DateTime.Now;
        //        bool result = new CodeBLL().EditCode(CodeEdit, originalName);
        //        if (result)
        //        {
        //            return Content("{\"msg\":\"修改成功！\",\"success\":true}");
        //        }
        //        else
        //        {
        //            return Content("{\"msg\":\"修改失败！\",\"success\":false}");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content("{\"msg\":\"修改失败," + ex.Message + "\",\"success\":false}");
        //    }
        //}

        //public ActionResult DelCodeByIDs()
        //{
        //    try
        //    {
        //        string Ids = Request["IDs"] == null ? "" : Request["IDs"];
        //        if (!string.IsNullOrEmpty(Ids))
        //        {
        //            if (new CodeBLL().DeleteCode(Ids))
        //            {
        //                return Content("{\"msg\":\"删除成功！\",\"success\":true}");
        //            }
        //            else
        //            {
        //                return Content("{\"msg\":\"删除失败！\",\"success\":false}");
        //            }
        //        }
        //        else
        //        {
        //            return Content("{\"msg\":\"删除失败！\",\"success\":false}");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content("{\"msg\":\"删除失败," + ex.Message + "\",\"success\":false}");
        //    }
        //}

        ///// <summary>
        ///// 菜单树
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetAllCodeTree()
        //{
        //    string CodeJson = new CodeBLL().GetAllCode("");
        //    return Content(CodeJson);
        //}

        ///// <summary>
        ///// 角色菜单树
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetAllRoleCodeTree()
        //{
        //    int roleid = Convert.ToInt32(Request["roleid"]);
        //    string roleCodeJson = new CodeBLL().GetAllCode(roleid);
        //    return Content(roleCodeJson);
        //}

        ///// <summary>
        ///// 分配按钮页面
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult CodeButtonSet()
        //{
        //    return View();
        //}

        ///// <summary>
        ///// 分配按钮权限
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult SetCodeButton()
        //{
        //    string Codeid = Request["Codeid"];
        //    string buttonids = Request["buttonids"];

        //    bool result = new CodeButtonBLL().SaveCodeButton(Codeid, buttonids);
        //    if (result)
        //    {
        //        return Content("{\"msg\":\"分配按钮成功！\",\"success\":true}");
        //    }
        //    else
        //    {
        //        return Content("{\"msg\":\"分配按钮失败！\",\"success\":false}");
        //    }
        //}

        ///// <summary>
        ///// 获取已配置的菜单按钮权限
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetCodeButtonByCodeID()
        //{
        //    int mid = Convert.ToInt32(Request["Codeid"]);  //菜单id
        //    string jsonStr = new CodeButtonBLL().GetButtonByCodeId(mid);
        //    return Content(jsonStr);
        //}

        ///// <summary>
        ///// 角色菜单树
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetAllRoleCodeButtonTree()
        //{
        //    int roleid = Convert.ToInt32(Request["roleid"]);
        //    string roleCodeJson = new CodeBLL().GetAllCodeButtonTree(roleid);
        //    return Content(roleCodeJson);
        //}

    }
}
