﻿using AchieveCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AchieveManageWeb.Controllers
{
    public class CommonController : Controller
    {
        //
        // GET: /Common，通用视图控制器/

        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// combbox列表--部门
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectDepartment()
        {
            string sql = "select Id,DepartmentName from tbDepartment where ParentID=0";
            string strJson = JsonHelper.GetJsonFromSql(sql);
            return Content(strJson);
        }

        public ActionResult AutoComplete_ProjectNo()
        {
            string search = Request["q"] == null ? "" : Request["q"];
            if (search=="" || SqlInjection.GetString(search))//存在注入
            {
                 return Content(null);      
            }
           // string sql = @"select top 10 ProjectNo,ProjectName from Fund where code like '%" + search + "%' or name like '%" + search + "%' order by code";
            string sql = string.Format("select  top 10 projectNo, projectName from [tbProject] where projectNo like '%{0}%' or projectName  like '%{0}%' order by projectNo", search);
            string strJson = JsonHelper.GetJsonFromSql(sql);
            return Content(strJson);           
        }

        public ActionResult AutoComplete_ItemCode()
        {
            string search = Request["q"] == null ? "" : Request["q"];
            if (search == "" || SqlInjection.GetString(search))//存在注入
            {
                return Content(null);
            }
            string sql = string.Format("select top 10 FItemID,FModel,FName,FNumber from t_ICITEMCORE  where FNumber like '{0}%' or FName like '%{0}%' order by FNumber", search);
            string strJson = JsonHelper.GetJsonFromSqlK3(sql);
            return Content(strJson);
        }

        /// <summary>
        /// 获取基础信息，供combo选择，必须参数父级id：p
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectBasicInfo()
        {    
            string ParentID = Request["p"] == null ? "" : Request["p"];
            if (ParentID=="")
            {
                 return Content(null);
            }
            string sql = "select InfoID,InfoName from [tbBasicInfo] where ParentID="+ParentID;
            string strJson = JsonHelper.GetJsonFromSql(sql);
            return Content(strJson);
        }

        /// <summary>
        /// 根据项目编码查询项目名称
        /// </summary>
        /// <returns></returns>
        public ActionResult getProjectNameByNo()
        {
            string projectNo = Request["projectNo"] == null ? "" : Request["projectNo"];
            if (projectNo == "")
            {
                return Content(null);
            }
            string sql = string.Format("select projectName from [tbProject] where projectNo='{0}'",projectNo);
            string strJson = JsonHelper.GetJsonFromSql(sql);
            return Content(strJson);
        }

        /// <summary>
        /// combbox列表--所有用户
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectUser()
        {
            string sql = "select ID,AccountName,RealName FROM tbUser where IsAble=1";
            string strJson = JsonHelper.GetJsonFromSql(sql);
            return Content(strJson);
        }
        /// <summary>
        /// 根据id和路径删除附件
        /// </summary>
        /// <returns></returns>
        public ActionResult DelAppendByID()
        {
            try
            {
                string AppendID = Request["ID"] == null ? "" : Request["ID"];
                string AppendPath = Request["AppendPath"] == null ? "" : Request["AppendPath"];

                if (!string.IsNullOrEmpty(AppendID) && !string.IsNullOrEmpty(AppendPath))
                {
                    List<string> sqllist = new List<string>();
                    sqllist.Add(string.Format("delete tbAppendInfo where AppendID = '{0}' ;", AppendID));
                    int x = SqlHelper.ExecuteNonQuery(SqlHelper.connStr, sqllist);
                    if (x > 0)
                    {
                        //删除文件
                        var filePath = Server.MapPath(string.Format("~/{0}", "uploads"));
                        System.IO.File.Delete(System.IO.Path.Combine(filePath, AppendPath));
                        return Content("{\"msg\":\"删除成功！\",\"success\":true}");
                    }
                    else
                    {
                        return Content("{\"msg\":\"删除失败！\",\"success\":false}");
                    }
                }
                else
                {
                    return Content("{\"msg\":\"删除失败！附件id为空\",\"success\":false}");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"msg\":\"删除失败," + ex.Message + "\",\"success\":false}");
            }
        }

    }
}
