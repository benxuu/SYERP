﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AchieveCommon
{
  public  class DataTableHelper
    {

      public static DataTable RowMoveUp(DataTable dt,int rowIndex) {
          if (rowIndex>0)
          {
          DataRow newdr = dt.NewRow();
          newdr.ItemArray = dt.Rows[rowIndex].ItemArray;
          dt.Rows.RemoveAt(rowIndex);
          dt.Rows.InsertAt(newdr, rowIndex - 1);
          dt.AcceptChanges();
          }         
          return dt;      
      }
            /// <summary>
            /// 对sourceDt进行排序，并且可以自由设置显示的条数
            /// --select top (num) from sourceDt order by strSort
            /// </summary>
            /// <param name="sourceDt">原DataTable</param>
            /// <param name="strSort">排序的字段，不用加order by (如: name desc 或 name desc,datecreated desc)</param>
            /// <param name="topNum">显示的数量（为0 是显示全部）</param>
            /// <returns>new DataTable</returns>
            public static DataTable SortTop(DataTable sourceDt, string strSort, int topNum)
            {
                DataTable dt = sourceDt.Clone();

                int totalCount = sourceDt.Rows.Count;
                if (totalCount > 0)
                {
                    DataView dv = sourceDt.DefaultView;
                    dv.Sort = strSort;

                    //计算要显示的条数
                    int count = topNum > 0 ? (topNum > totalCount ? totalCount : topNum) : totalCount;
                    for (int i = 0; i < count; i++)
                    {
                        dt.ImportRow(dv[i].Row);
                    }
                }
                return dt;
            }
            

            /// <summary>
            /// 对sourceDt进行查询和排序，并且可以自由设置显示的条数
            /// --select top (count) from sourceDt where strSelect order by strSort
            /// </summary>
            /// <param name="sourceDt">原DataTable</param>
            /// <param name="strSelect">查询条件(如:name='me')</param>
            /// <param name="strSort">排序字段，不用加order by (如: name desc 或 name desc,datecreated desc)</param>
            /// <param name="count">显示的数量（为0 是显示全部）</param>
            /// <returns>new DataTable</returns>
            public static DataTable SelectTop(DataTable sourceDt, string strSelect, string strSort, int count)
            {
                DataTable dt = sourceDt.Clone();
                bool noSort = string.IsNullOrEmpty(strSort);
                DataRow[] dr = noSort ? sourceDt.Select(strSelect) : sourceDt.Select(strSelect, strSort);
                //foreach (DataRow row in dr)
                //{
                //    dt.Rows.Add(row.ItemArray);
                //}

               // int dtCount = dt.Rows.Count;
                int dtCount = dr.Length;
                if (dtCount > 0)
                {
                    //计算要显示的条数
                    int num = count > 0 ? (count > dtCount ? dtCount : count) : dtCount;
                    //DataView dv = dt.DefaultView;

                    //if (!noSort)
                    //{
                    //    dv.Sort = strSort;
                    //}

                    for (int i = 0; i < num; i++)
                    {
                        dt.Rows.Add(dr[i].ItemArray);
                    }
                }
                return dt;
            }
      /// <summary>
      /// 范围选择，用于分页
      /// </summary>
      /// <param name="sourceDt"></param>
      /// <param name="startRowNum"></param>
      /// <param name="endRowNum"></param>
      /// <returns></returns>
      
      public static DataTable SelectRange(DataTable sourceDt,  int startRowNum,int endRowNum,string strSort,string strSelect="1=1")
    {

        DataTable dt = sourceDt.Clone();
        bool noSort = string.IsNullOrEmpty(strSort);
        DataRow[] dr = noSort ? sourceDt.Select(strSelect) : sourceDt.Select(strSelect, strSort);
        
        int dtCount0 = dr.Length;
        for (int i = 0; i < dtCount0; i++)
            {
                dt.Rows.Add(dr[i].ItemArray);
            }
        int dtCount = dt.Rows.Count;
        DataTable dtout = sourceDt.Clone(); 
                if (endRowNum > dtCount) { endRowNum = dtCount; } 
                    for (int i = startRowNum-1; i < endRowNum; i++)
                    {
                        if (i<dtCount)
                        {
                            dtout.ImportRow(dt.Rows[i]);
                        }
                    }
                    return dtout;
            }
      public static DataTable SelectRange(DataTable sourceDt, int startRowNum, int endRowNum)
      {
          DataTable dt = sourceDt.Clone();
          int dtCount = sourceDt.Rows.Count;
          if (endRowNum > dtCount) { endRowNum = dtCount; }
          for (int i = startRowNum - 1; i < endRowNum; i++)
          {
              if (i < dtCount)
              {
                  dt.ImportRow(sourceDt.Rows[i]);
              }
          }
          return dt;
      }
            /// <summary>
            /// DateTable的分页操作
            /// </summary>
            /// <param name="dt">要进行分页的DataTable</param>
            /// <param name="currentPageIndex">当前页数</param>
            /// <param name="pageSize">一页显示的条数</param>
            /// <returns>第pageIndex页的数据</returns>
            public static DataTable SetPage(DataTable dt, int currentPageIndex, int pageSize)
            {
                if (currentPageIndex == 0)
                {
                    return dt;
                }

                DataTable newdt = dt.Clone();

                int rowbegin = (currentPageIndex - 1) * pageSize;//当前页的第一条数据在dt中的位置
                int rowend = currentPageIndex * pageSize;//当前页的最后一条数据在dt中的位置

                if (rowbegin >= dt.Rows.Count)
                {
                    return newdt;
                }

                if (rowend > dt.Rows.Count)
                {
                    rowend = dt.Rows.Count;
                }

                DataView dv = dt.DefaultView;
                for (int i = rowbegin; i <= rowend - 1; i++)
                {
                    newdt.ImportRow(dv[i].Row);
                }

                return newdt;
            }
        }
    }
 