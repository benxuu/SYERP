﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace AchieveCommon
{
    public abstract class easyUiHelper
    {

        public easyUiHelper() { }

        public static string queryDatagridJson(string sql ) {
            DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStr, sql);
            int totalCount = dt.Rows.Count;
            string strJson = JsonHelper.ToJson(dt);
            return   "{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}"; 
        }

        public static string queryDatagridJson(string sql,bool isK3DB=false)
        {
            DataTable dt= new DataTable();
            if (isK3DB)
            {
                dt=  SqlHelper.GetDataTable(SqlHelper.connStrK3, sql);
            }else
	            {
                  dt= SqlHelper.GetDataTable(SqlHelper.connStr, sql);
	            }   
            int totalCount = dt.Rows.Count;
            string strJson = JsonHelper.ToJson(dt);
            return "{\"total\": " + totalCount.ToString() + ",\"rows\":" + strJson + "}";
        }

    }
}
