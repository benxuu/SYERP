﻿using AchieveCommon;
using AchieveDALFactory;
using AchieveEntity;
using AchieveInterfaceDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Timers;
using System.Data.Common;
//create by Ben
//Modification time:2019.3.26
namespace AchieveBLL
{
    public class RptBLL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RptName">gskl钢水控流,yjgc冶金工程,nyhj能源环境,dljt电缆卷筒,zzb制造部,cl车轮</param>
        /// <returns></returns>
        public DataTable tbRptMonth(string RptName)
        {
            string deptTb = string.Empty;
            switch (RptName)
            {
                case "gskl"://钢水控流
                    deptTb = "rpt01";
                    break;
                case "yjgc"://冶金工程
                    deptTb = "rpt02";
                    break;
                case "nyhj":
                    deptTb = "rpt03";//能源环境
                    break;
                case "dljt":
                    deptTb = "rpt06";//电缆卷筒
                    break;
                case "zzb"://制造部
                    deptTb = "rpt05";
                    break;
                case "cl"://制造部
                    deptTb = "rpt07";
                    break;
                default:
                    break;
            }

            DataTable dt = new DataTable();
            string strSql = @"select rowid=30, 科目,isNull(一月,0)as 一月,isNull(二月,0)as 二月,isNull(三月,0)as 三月,isNull(四月,0)as 四月,
                            isNull(五月,0)as 五月,isNull(六月,0)as 六月,isNull(七月,0)as 七月,isNull(八月,0)as 八月,isNull(九月,0)as 九月,
                                isNull(十月,0)as 十月,isNull(十一月,0)as 十一月,isNull(十二月,0)as 十二月,FAccountID as kmid from " + deptTb;

            dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, strSql.ToString());

            #region 初始化报表数据视图
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch ((int)dt.Rows[i]["kmid"])
                {
                    case 1074://开票金额
                        dt.Rows[i]["rowid"] = 0;
                        break;
                    case 1://"回款金额":
                        dt.Rows[i]["rowid"] = 1;
                        break;
                    case 2://"发货金额":
                        dt.Rows[i]["rowid"] = 2;
                        break;
                    case 1133://"直接材料":
                        dt.Rows[i]["rowid"] = 4;
                        break;
                    case 1192://"直接人工":
                        dt.Rows[i]["rowid"] = 5;
                        break;
                    case 1136://"易耗品":
                        dt.Rows[i]["rowid"] = 6;
                        break;
                    case 1145://"运输费":
                        dt.Rows[i]["rowid"] = 7;
                        break;
                    case 1152://"外协费":
                        dt.Rows[i]["rowid"] = 8;
                        break;
                    case 1257://"部门工资":
                        dt.Rows[i]["rowid"] = 12;
                        break;
                    case 1185://"办公费":
                        dt.Rows[i]["rowid"] = 13;
                        break;
                    case 1186://"差旅费":
                        dt.Rows[i]["rowid"] = 14;
                        break;
                    case 1187://"招待费":
                        dt.Rows[i]["rowid"] = 15;
                        break;
                    case 5://"研发费":
                        dt.Rows[i]["rowid"] = 21;
                        break;
                    case 3://"退款金额":"回款退款":
                        dt.Rows[i]["rowid"] = 22;
                        break;
                    case 6://"付款金额":
                        dt.Rows[i]["rowid"] = 23;
                        break;
                    case 1142://办公费2-制造部，叠加入1185科目
                        for (int k = 2; k < 14; k++)
                        {
                            dt.Select("kmid=1185")[0][k] = Convert.ToDecimal(dt.Select("kmid=1185")[0][k]) + Convert.ToDecimal(dt.Rows[i][k]);
                        }
                        break;
                    case 1143://差旅费2-制造部，叠加入1186科目
                        for (int k = 2; k < 14; k++)
                        {
                            dt.Select("kmid=1186")[0][k] = Convert.ToDecimal(dt.Select("kmid=1186")[0][k]) + Convert.ToDecimal(dt.Rows[i][k]);
                        }
                        break;
                    default:
                        // dt.Rows[i]["rowid"] =30;
                        break;
                }
            }



            dt.Rows.Add(3, "销售金额");
            dt.Rows.Add(9, "制造费用");
            dt.Rows.Add(10, "项目利润");
            dt.Rows.Add(11, "项目利润率");
            dt.Rows.Add(16, "部门费用");
            dt.Rows.Add(17, "研发费用");
            //if (RptName == "gskl" | RptName == "yjgc" | RptName == "nyhj")//三个部门，需要计算公摊费用、营业外补贴收入
            //{
            //    string sql = "select 18 as rowid,'公摊费用' as 科目, sum(一月)/3 as 一月,sum(二月)/3 as 二月,sum(三月)/3 as 三月,sum(四月)/3 as 四月,sum(五月)/3 as 五月,sum(六月)/3 as 六月,sum(七月)/3 as 七月,sum(八月)/3 as 八月,sum(九月)/3 as 九月,sum(十月)/3 as 十月,sum(十一月)/3 as 十一月,sum(十二月)/3 as 十二月 from rpt04 WHERE 科目 not in('综合部付款','补贴收入','营业外收入')";
            //    DataTable dtf = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql);
            //    dt.Rows.Add(dtf.Rows[0].ItemArray);
            //}
            //else
            //{
            dt.Rows.Add(18, "公摊费用", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            //}

            dt.Rows.Add(19, "利润");
            dt.Rows.Add(20, "利润率");

            DataView dataView = dt.DefaultView;//利用视图排序
            dataView.Sort = "rowid asc";
            dt = dataView.ToTable();//排序完毕
            dt.Columns.Add("全年", typeof(Decimal));//添加全年统计列

            #endregion

            #region 报表月数据公式计算
            for (int i = 2; i < 14; i++)//选择列，0：id；1：科目；2：一月...
            {
                #region//计算研发比率和订单比例
                decimal yfRate = 0;
                decimal ddRate = 1;
                try
                {
                    decimal rate = Convert.ToDecimal(dt.Rows[21][i]) / (Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[6][i]));//   研发费/(直接材料+易耗品)>1 

                    if (rate > 1)
                    {
                        yfRate = 1;
                    }
                    else
                    {
                        yfRate = rate;
                    }
                }
                catch (Exception)
                {
                    yfRate = 0;
                }
                ddRate = 1 - yfRate;
                #endregion
                //00计算原始制造费用，原始制造费用：=SUM(4:8)，仅用于研发费用计算！
                decimal ozzfy = Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[5][i]) + Convert.ToDecimal(dt.Rows[6][i]) + Convert.ToDecimal(dt.Rows[7][i]) + Convert.ToDecimal(dt.Rows[8][i]);
                //0，开票金额：原始数据
                //1，回款金额:回款金额+退款金额
                dt.Rows[1][i] = Convert.ToDecimal(dt.Rows[1][i]) + Convert.ToDecimal(dt.Rows[22][i]);
                //2,发货金额：发货金额/1.16
                dt.Rows[2][i] = Convert.ToDecimal(dt.Rows[2][i]) / 1.16m;
                //3,销售金额:
                dt.Rows[3][i] = Convert.ToDecimal(dt.Rows[0][i]) * 0.2m + Convert.ToDecimal(dt.Rows[1][i]) / 1.16m * 0.3m + Convert.ToDecimal(dt.Rows[2][i]) * 0.5m;
                //4,直接材料:直接材料*订单比例
                dt.Rows[4][i] = Convert.ToDecimal(dt.Rows[4][i]) * ddRate;
                //5.直接人工:直接人工*订单比例
                dt.Rows[5][i] = Convert.ToDecimal(dt.Rows[5][i]) * ddRate;
                //6.易耗品:易耗品*订单比例
                dt.Rows[6][i] = Convert.ToDecimal(dt.Rows[6][i]) * ddRate;
                //7.运输费:运输费*订单比例
                dt.Rows[7][i] = Convert.ToDecimal(dt.Rows[7][i]) * ddRate;
                //8.外协费:外协费*订单比例
                dt.Rows[8][i] = Convert.ToDecimal(dt.Rows[8][i]) * ddRate;
                //9.制造费用：=SUM(4:8)
                dt.Rows[9][i] = Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[5][i]) + Convert.ToDecimal(dt.Rows[6][i]) + Convert.ToDecimal(dt.Rows[7][i]) + Convert.ToDecimal(dt.Rows[8][i]);
                //10.项目利润
                dt.Rows[10][i] = Convert.ToDecimal(dt.Rows[3][i]) - Convert.ToDecimal(dt.Rows[9][i]);
                //11.项目利润率
                try
                {
                    dt.Rows[11][i] = Convert.ToDecimal(dt.Rows[10][i]) / Convert.ToDecimal(dt.Rows[3][i]);
                }
                catch (Exception)
                {

                    dt.Rows[11][i] = 0;
                }
                //12：15.部门工资、办公费、差旅费、招待费，使用原始数据
                //16.部门费用，sum(12:15)
                dt.Rows[16][i] = Convert.ToDecimal(dt.Rows[12][i]) + Convert.ToDecimal(dt.Rows[13][i]) + Convert.ToDecimal(dt.Rows[14][i]) + Convert.ToDecimal(dt.Rows[15][i]);
                //17.研发费用，原始制造费用*研发比率 
                dt.Rows[17][i] = ozzfy * yfRate;

                //18 公摊费用,视图初始化中已加载
                //19 利润,
                dt.Rows[19][i] = Convert.ToDecimal(dt.Rows[10][i]) - Convert.ToDecimal(dt.Rows[16][i]) - Convert.ToDecimal(dt.Rows[17][i]) - Convert.ToDecimal(dt.Rows[18][i]);
                //20.计算利润率
                try
                {
                    dt.Rows[20][i] = Convert.ToDecimal(dt.Rows[19][i]) / Convert.ToDecimal(dt.Rows[3][i]);
                }
                catch (Exception)
                {

                    dt.Rows[20][i] = 0;
                }

            }

            #endregion

            # region  计算全年统计列

            for (int i = 0; i < 21; i++)//共20行
            {
                if (i == 11)//项目利润率
                {
                    decimal lrl = 0;

                    try
                    {
                        lrl = Convert.ToDecimal(dt.Rows[10]["全年"]) / Convert.ToDecimal(dt.Rows[3]["全年"]);
                    }
                    catch (Exception)
                    {

                        lrl = 0;
                    }
                    dt.Rows[i]["全年"] = lrl;
                }
                else if (i == 20)//利润率
                {
                    decimal lrl = 0;

                    try
                    {
                        lrl = Convert.ToDecimal(dt.Rows[19]["全年"]) / Convert.ToDecimal(dt.Rows[3]["全年"]);
                    }
                    catch (Exception)
                    {

                        lrl = 0;
                    }
                    dt.Rows[i]["全年"] = lrl;
                }
                else
                {
                    decimal sum = 0;

                    for (int j = 2; j < 14; j++)
                    {
                        sum += Convert.ToDecimal(dt.Rows[i][j]);
                    }
                    dt.Rows[i]["全年"] = sum;

                }

            }
            #endregion

            //删除不需要显示的行数据（科目），保留前21行
            //for (int i = dt.Rows.Count - 1; i > 20; i--)
            //{
            //    dt.Rows.RemoveAt(i);
            //}


            return dt;
        }

        /// <summary>
        /// 费用报销报表数据
        /// </summary>
        /// <param name="groupType">聚集方法，部门|项目</param>
        /// <param name="status">是否审核</param>
        /// <param name="period">期间</param>
        /// <param name="periodType">期间类型，报账时间|审核期间</param>
        /// <returns></returns>
        public static DataTable getfybxDt(string groupType, string status, string period, string periodType)
        {
            //create the output datatable struct
            //string groupType = Request["grouptype"] == null ? "dept" : Request["grouptype"];//projectNo
            //string status = Request["status"] == null ? "all" : Request["status"];//all，0，1
            //string period = Request["period"] == null ? "" : Request["period"];//“2020-01”
            //string periodType = Request["periodType"] == null ? "claimTime" : Request["periodType"];//claimPeriod 

            //DataTable dt = RptBLL.getfybxDt(groupType, status, period, periodType);
            string where = "1=1 ";
            if (status != "all")
            {
                where += string.Format("and a.status='{0}' ", status);
            }
            if (period == "all" || period == "全部")
            {
                where += string.Format("and a.claimTime between '2020-01-01' and '2021-01-01' ");
            }else
	{
                 where += string.Format("and a.period='{0}' ", period);
	}

            DataTable dt = new DataTable();
            dt.Columns.Add("qijian", typeof(string));
            dt.Columns.Add("dept", typeof(string));
            dt.Columns.Add("projectNo", typeof(string));
            dt.Columns.Add("projectName", typeof(string));
            dt.Columns.Add("管理费用办公费", typeof(decimal));
            dt.Columns.Add("管理费用差旅费", typeof(decimal));
            dt.Columns.Add("管理费用招待费", typeof(decimal));
            dt.Columns.Add("管理费用小计", typeof(decimal));
            dt.Columns.Add("销售费用办公费", typeof(decimal));
            dt.Columns.Add("销售费用差旅费", typeof(decimal));
            dt.Columns.Add("销售费用招待费", typeof(decimal));
            dt.Columns.Add("销售费用提成款", typeof(decimal));
            dt.Columns.Add("销售费用小计", typeof(decimal));
            dt.Columns.Add("财务费用贴息款", typeof(decimal));
            dt.Columns.Add("其他", typeof(decimal));
            dt.Columns.Add("sum", typeof(decimal));
            dt.Columns["管理费用办公费"].DefaultValue = 0.00;
            dt.Columns["管理费用差旅费"].DefaultValue = 0.00;
            dt.Columns["管理费用招待费"].DefaultValue = 0.00;
            dt.Columns["管理费用小计"].DefaultValue = 0.00;
            dt.Columns["销售费用办公费"].DefaultValue = 0.00;
            dt.Columns["销售费用差旅费"].DefaultValue = 0.00;
            dt.Columns["销售费用招待费"].DefaultValue = 0.00;
            dt.Columns["销售费用提成款"].DefaultValue = 0.00;
            dt.Columns["销售费用小计"].DefaultValue = 0.00;
            dt.Columns["财务费用贴息款"].DefaultValue = 0.00;
            dt.Columns["其他"].DefaultValue = 0.00;
            dt.Columns["sum"].DefaultValue = 0.00;

            string sql = "";

            if (groupType == "dept")//根据项目编码聚类
            {

                if (periodType == "claimPeriod")
                {
                    sql = @"select a.period as qijian,b.dept,feeType,feeName,
sum(amount)as total from tbExpenseClaim a left join tbExpenseClaimEntry b on b.claimId=a.id
where "+ where + "group by a.period,b.dept,feeType,feeName  order by qijian asc ";
                }
                else
                {
                    sql = @"select CONVERT(VarChar(7), claimTime, 120) as qijian,b.dept,feeType,feeName,
sum(amount)as total from tbExpenseClaim a left join tbExpenseClaimEntry b on b.claimId=a.id
where " + where + "group by CONVERT(VarChar(7), claimTime, 120),b.dept,feeType,feeName   order by qijian asc ";
                }

                DataTable dt0 = SqlHelper.GetDataTableIE(sql);
                foreach (DataRow item in dt0.Rows)
                {
                    var qijian = item["qijian"].ToString();
                    var dept = item["dept"].ToString();
                    var feeName = item["feeName"].ToString();
                    DataRow[] arrRows = dt.Select("qijian='" + qijian + "' and dept='" + dept + "'");
                    if (arrRows.Length == 0)//不存在期间行
                    {
                        DataRow r = dt.NewRow();
                        r["qijian"] = qijian;
                        r["dept"] = dept;
                        if (feeName == "其他")
                        {
                            r["其他"] = item["total"];
                        }
                        else if (feeName.Length > 1 && dt.Columns.Contains(item["feeType"].ToString() + item["feeName"].ToString()))
                        {
                            r[item["feeType"].ToString() + item["feeName"].ToString()] = item["total"];
                        }

                        dt.Rows.Add(r);
                    }
                    else//存在期间行
                    {
                        if (feeName == "其他")
                        {
                            arrRows[0]["其他"] = item["total"];
                        }
                        else if (feeName.Length > 1 && dt.Columns.Contains(item["feeType"].ToString() + item["feeName"].ToString()))
                        {
                            arrRows[0][item["feeType"].ToString() + item["feeName"].ToString()] = item["total"];
                        }

                    }
                }

            }

            else if (groupType == "projectNo")//根据承担部门聚类
            {

                if (periodType == "claimPeriod")
                {

                    sql = @" select a.period as qijian, b.projectNo,b.projectName, feeType,feeName,
sum(amount) as total from tbExpenseClaim a left join tbExpenseClaimEntry b on b.claimId=a.id
where "+ where + "group by  a.period, b.projectNo,b.projectName,feeType,feeName  order by qijian asc ";

                }
                else
                {
                    sql = @" select CONVERT(VarChar(7), claimTime, 120) as qijian,b.projectNo,b.projectName, feeType,feeName,
sum(amount) as total from tbExpenseClaim a left join tbExpenseClaimEntry b on b.claimId=a.id
where "+ where + " group by CONVERT(VarChar(7), claimTime, 120),b.projectNo,b.projectName,feeType,feeName order by qijian asc ";
                }


                DataTable dt0 = SqlHelper.GetDataTableIE(sql);
                foreach (DataRow item in dt0.Rows)
                {
                    var qijian = item["qijian"].ToString();
                    var projectNo = item["projectNo"].ToString();
                    var feeName = item["feeName"].ToString();
                    DataRow[] arrRows = dt.Select("qijian='" + qijian + "' and projectNo='" + projectNo + "'");
                    if (arrRows.Length == 0)//不存在期间行
                    {
                        DataRow r = dt.NewRow();
                        r["qijian"] = qijian;
                        r["projectNo"] = projectNo;
                        if (feeName == "其他")
                        {
                            r["其他"] = item["total"];
                        }
                        else if (feeName.Length > 1 && dt.Columns.Contains(item["feeType"].ToString() + item["feeName"].ToString()))
                        {
                            
                            r[item["feeType"].ToString() + item["feeName"].ToString()] = item["total"];
                        }

                        dt.Rows.Add(r);
                    }
                    else//存在期间行
                    {
                        if (feeName == "其他")
                        {
                            arrRows[0]["其他"] = item["total"];
                        }
                        else if (feeName.Length > 1 && dt.Columns.Contains(item["feeType"].ToString() + item["feeName"].ToString()))
                        {
                            arrRows[0][item["feeType"].ToString() + item["feeName"].ToString()] = item["total"];
                        }
                    }
                }
            }
            decimal glbg = 0, glcl = 0, glzd = 0, glxj = 0, xsbg = 0, xscl = 0, xszd = 0, xstc = 0, xsxj = 0, tx = 0, qt = 0, ssum = 0;
            foreach (DataRow item in dt.Rows)
            {
                item["管理费用小计"] = decimal.Parse(item["管理费用办公费"].ToString()) + decimal.Parse(item["管理费用差旅费"].ToString()) + decimal.Parse(item["管理费用招待费"].ToString());
                item["销售费用小计"] = decimal.Parse(item["销售费用办公费"].ToString()) + decimal.Parse(item["销售费用差旅费"].ToString()) + decimal.Parse(item["销售费用招待费"].ToString()) + decimal.Parse(item["销售费用提成款"].ToString());
                item["sum"] = decimal.Parse(item["管理费用小计"].ToString()) + decimal.Parse(item["销售费用小计"].ToString()) + decimal.Parse(item["财务费用贴息款"].ToString()) + decimal.Parse(item["其他"].ToString());
                glbg += decimal.Parse(item["管理费用办公费"].ToString());
                glcl += decimal.Parse(item["管理费用差旅费"].ToString());
                glzd += decimal.Parse(item["管理费用招待费"].ToString());
                glxj += decimal.Parse(item["管理费用小计"].ToString());
                xsbg += decimal.Parse(item["销售费用办公费"].ToString());
                xscl += decimal.Parse(item["销售费用差旅费"].ToString());
                xszd += decimal.Parse(item["销售费用招待费"].ToString());
                xstc += decimal.Parse(item["销售费用提成款"].ToString());
                xsxj += decimal.Parse(item["销售费用小计"].ToString());
                qt += decimal.Parse(item["其他"].ToString());
                tx += decimal.Parse(item["财务费用贴息款"].ToString());
                ssum += decimal.Parse(item["sum"].ToString());
            }
            DataRow rsum = dt.NewRow();
            rsum["qijian"] = "合计";
            rsum["管理费用办公费"] = glbg;
            rsum["管理费用差旅费"] = glcl;
            rsum["管理费用招待费"] = glzd;
            rsum["管理费用小计"] = glxj;
            rsum["销售费用办公费"] = xsbg;
            rsum["销售费用差旅费"] = xscl;
            rsum["销售费用招待费"] = xszd;
            rsum["销售费用提成款"] = xstc;
            rsum["销售费用小计"] = xsxj;
            rsum["其他"] = qt;
            rsum["财务费用贴息款"] = tx;
            rsum["sum"] = ssum;
            dt.Rows.Add(rsum); 
            return dt; 
        }

        public static DataTable getRptDt2019(string RptName)
        {
            string deptTb = string.Empty;
            switch (RptName)
            {
                case "gskl"://钢水控流
                    deptTb = "rpt01";
                    break;
                case "yjgc"://冶金工程
                    deptTb = "rpt02";
                    break;
                case "nyhj":
                    deptTb = "rpt03";//能源环境
                    break;
                case "dljt":
                    deptTb = "rpt06";//电缆卷筒
                    break;
                case "zzb"://制造部
                    deptTb = "rpt05";
                    break;

                default:
                    break;

            }

            DataTable dt = new DataTable();
            string strSql = @"select rowid=30, 科目,isNull(一月,0)as 一月,isNull(二月,0)as 二月,isNull(三月,0)as 三月,isNull(四月,0)as 四月,
                            isNull(五月,0)as 五月,isNull(六月,0)as 六月,isNull(七月,0)as 七月,isNull(八月,0)as 八月,isNull(九月,0)as 九月,
                                isNull(十月,0)as 十月,isNull(十一月,0)as 十一月,isNull(十二月,0)as 十二月 from " + deptTb;

            dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, CommandType.Text, strSql.ToString());

            #region 初始化报表数据视图
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i]["科目"].ToString().Trim())
                {
                    case "开票金额":
                        dt.Rows[i]["rowid"] = 0;
                        break;
                    case "回款金额":
                        dt.Rows[i]["rowid"] = 1;
                        break;
                    case "发货金额":
                        dt.Rows[i]["rowid"] = 2;
                        break;
                    case "直接材料":
                        dt.Rows[i]["rowid"] = 4;
                        break;
                    case "直接人工":
                        dt.Rows[i]["rowid"] = 5;
                        break;
                    case "易耗品":
                        dt.Rows[i]["rowid"] = 6;
                        break;
                    case "运输费":
                        dt.Rows[i]["rowid"] = 7;
                        break;
                    case "外协费":
                        dt.Rows[i]["rowid"] = 8;
                        break;
                    case "部门工资":
                        dt.Rows[i]["rowid"] = 12;
                        break;
                    case "办公费":
                        dt.Rows[i]["rowid"] = 13;
                        break;
                    case "差旅费":
                        dt.Rows[i]["rowid"] = 14;
                        break;
                    case "招待费":
                        dt.Rows[i]["rowid"] = 15;
                        break;
                    case "研发费":
                        dt.Rows[i]["rowid"] = 21;
                        break;
                    case "退款金额":
                        dt.Rows[i]["rowid"] = 22;
                        break;
                    case "回款退款":
                        if (RptName == "zzb")
                        {
                            dt.Rows[i]["rowid"] = 22;
                        }
                        break;
                    case "付款金额":
                        dt.Rows[i]["rowid"] = 23;
                        break;
                    default:
                        // dt.Rows[i]["rowid"] =30;
                        break;
                }
            }

            if (RptName == "zzb")//制造部需分别合并两项办公费、两项差旅费
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["rowid"]) == 13)//办公费
                    {
                        for (int j = i + 1; j < dt.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(dt.Rows[j]["rowid"]) == 13)//第二个办公费
                            {
                                //累加到第一行
                                for (int k = 2; k < 14; k++)
                                {
                                    dt.Rows[i][k] = (decimal)dt.Rows[i][k] + (decimal)dt.Rows[j][k];
                                }
                                //删除第二行
                                dt.Rows.RemoveAt(j);
                                break;
                            }
                        }
                        break;
                    }
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["rowid"]) == 14)//差旅费
                    {
                        for (int j = i + 1; j < dt.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(dt.Rows[j]["rowid"]) == 14)//第二个差旅费
                            {
                                //累加到第一行
                                for (int k = 2; k < 14; k++)
                                {
                                    dt.Rows[i][k] = (decimal)dt.Rows[i][k] + (decimal)dt.Rows[j][k];
                                }
                                //删除第二行
                                dt.Rows.RemoveAt(j);
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            dt.Rows.Add(3, "销售金额");
            dt.Rows.Add(9, "制造费用");
            dt.Rows.Add(10, "项目利润");
            dt.Rows.Add(11, "项目利润率");
            dt.Rows.Add(16, "部门费用");
            dt.Rows.Add(17, "研发费用");
            if (RptName == "gskl" | RptName == "yjgc" | RptName == "nyhj")//三个需要计算公摊费用的部门
            {
             //"select 18 as rowid,'公摊费用' as 科目, sum(一月)/3 as 一月,sum(二月)/3 as 二月,sum(三月)/3 as 三月,sum(四月)/3 as 四月,sum(五月)/3 as 五月,sum(六月)/3 as 六月,sum(七月)/3 as 七月,sum(八月)/3 as 八月,sum(九月)/3 as 九月,sum(十月)/3 as 十月,sum(十一月)/3 as 十一月,sum(十二月)/3 as 十二月 from rpt04 WHERE 科目 not in('综合部付款','补贴收入','营业外收入')";

                string sql = @" select 18 as rowid,'公摊费用' as 科目, isnull(sum(一月)/3,0) as 一月,isnull(sum(二月)/3,0) as 二月,isnull( sum(三月)/3,0) as 三月,isnull( sum(四月)/3,0) as 四月,
isnull( sum(五月)/3,0) as 五月,isnull(sum(六月)/3,0) as 六月,isnull( sum(七月)/3,0) as 七月,
isnull( sum(八月)/3,0) as 八月,isnull( sum(九月)/3,0) as 九月,isnull( sum(十月)/3,0) as 十月,isnull(sum(十一月)/3,0) as 十一月,
isnull( sum(十二月)/3,0) as 十二月 from rpt04 WHERE 科目 not in('综合部付款','补贴收入','营业外收入')";
                DataTable dtf = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql);
                dt.Rows.Add(dtf.Rows[0].ItemArray);
            }
            else
            {
                dt.Rows.Add(18, "公摊费用", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            }

            dt.Rows.Add(19, "利润");
            dt.Rows.Add(20, "利润率");

            DataView dataView = dt.DefaultView;//利用视图排序
            dataView.Sort = "rowid asc";
            dt = dataView.ToTable();//排序完毕
            dt.Columns.Add("全年", typeof(Decimal));//添加全年统计列
            dt.Columns["全年"].DefaultValue = 0;

            #endregion

            #region 报表月数据公式计算
            for (int i = 2; i < 14; i++)//选择列，0：id；1：科目；2：一月...
            {
                #region//计算研发比率和订单比例
                decimal yfRate = 0;
                decimal ddRate = 1;
                try
                {
                    decimal rate = Convert.ToDecimal(dt.Rows[21][i]) / (Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[6][i]));//   研发费/(直接材料+易耗品)>1 

                    if (rate > 1)
                    {
                        yfRate = 1;
                    }
                    else
                    {
                        yfRate = rate;
                    }
                }
                catch (Exception)
                {

                    yfRate = 0;
                }
                ddRate = 1 - yfRate;
                #endregion
                //00计算原始制造费用，原始制造费用：=SUM(4:8)，仅用于研发费用计算！
                decimal ozzfy = Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[5][i]) + Convert.ToDecimal(dt.Rows[6][i]) + Convert.ToDecimal(dt.Rows[7][i]) + Convert.ToDecimal(dt.Rows[8][i]);
                //0，开票金额：原始数据
                //1，回款金额:回款金额+退款金额
                dt.Rows[1][i] = Convert.ToDecimal(dt.Rows[1][i]) + Convert.ToDecimal(dt.Rows[22][i]);
                //2,发货金额：发货金额/1.16
                dt.Rows[2][i] = Convert.ToDecimal(dt.Rows[2][i]) / 1.16m;
                //3,销售金额:
                dt.Rows[3][i] = Convert.ToDecimal(dt.Rows[0][i]) * 0.2m + Convert.ToDecimal(dt.Rows[1][i]) / 1.16m * 0.3m + Convert.ToDecimal(dt.Rows[2][i]) * 0.5m;
                //4,直接材料:直接材料*订单比例
                dt.Rows[4][i] = Convert.ToDecimal(dt.Rows[4][i]) * ddRate;
                //5.直接人工:直接人工*订单比例
                dt.Rows[5][i] = Convert.ToDecimal(dt.Rows[5][i]) * ddRate;
                //6.易耗品:易耗品*订单比例
                dt.Rows[6][i] = Convert.ToDecimal(dt.Rows[6][i]) * ddRate;
                //7.运输费:运输费*订单比例
                dt.Rows[7][i] = Convert.ToDecimal(dt.Rows[7][i]) * ddRate;
                //8.外协费:外协费*订单比例
                dt.Rows[8][i] = Convert.ToDecimal(dt.Rows[8][i]) * ddRate;
                //9.制造费用：=SUM(4:8)
                dt.Rows[9][i] = Convert.ToDecimal(dt.Rows[4][i]) + Convert.ToDecimal(dt.Rows[5][i]) + Convert.ToDecimal(dt.Rows[6][i]) + Convert.ToDecimal(dt.Rows[7][i]) + Convert.ToDecimal(dt.Rows[8][i]);
                //10.项目利润
                dt.Rows[10][i] = Convert.ToDecimal(dt.Rows[3][i]) - Convert.ToDecimal(dt.Rows[9][i]);
                //11.项目利润率
                try
                {
                    dt.Rows[11][i] = Convert.ToDecimal(dt.Rows[10][i]) / Convert.ToDecimal(dt.Rows[3][i]);
                }
                catch (Exception)
                {

                    dt.Rows[11][i] = 0;
                }
                //12：15.部门工资、办公费、差旅费、招待费，使用原始数据
                //16.部门费用，sum(12:15)
                dt.Rows[16][i] = Convert.ToDecimal(dt.Rows[12][i]) + Convert.ToDecimal(dt.Rows[13][i]) + Convert.ToDecimal(dt.Rows[14][i]) + Convert.ToDecimal(dt.Rows[15][i]);
                //17.研发费用，原始制造费用*研发比率 
                dt.Rows[17][i] = ozzfy * yfRate;

                //18 公摊费用,视图初始化中已加载
                //19 利润,
                dt.Rows[19][i] = Convert.ToDecimal(dt.Rows[10][i]) - Convert.ToDecimal(dt.Rows[16][i]) - Convert.ToDecimal(dt.Rows[17][i]) - Convert.ToDecimal(dt.Rows[18][i]);
                //20.计算利润率
                try
                {
                    dt.Rows[20][i] = Convert.ToDecimal(dt.Rows[19][i]) / Convert.ToDecimal(dt.Rows[3][i]);
                }
                catch (Exception)
                {

                    dt.Rows[20][i] = 0;
                }

            }

            #endregion

            # region  计算全年统计列

            for (int i = 0; i < 21; i++)//共20行
            {
                if (i == 11)//项目利润率
                {
                    decimal lrl = 0;

                    try
                    {
                        lrl = Convert.ToDecimal(dt.Rows[10]["全年"]) / Convert.ToDecimal(dt.Rows[3]["全年"]);
                    }
                    catch (Exception)
                    {

                        lrl = 0;
                    }
                    dt.Rows[i]["全年"] = lrl;
                }
                else if (i == 20)//利润率
                {
                    decimal lrl = 0;

                    try
                    {
                        lrl = Convert.ToDecimal(dt.Rows[19]["全年"]) / Convert.ToDecimal(dt.Rows[3]["全年"]);
                    }
                    catch (Exception)
                    {

                        lrl = 0;
                    }
                    dt.Rows[i]["全年"] = lrl;
                }
                else
                {
                    decimal sum = 0;

                    for (int j = 2; j < 14; j++)
                    {
                        sum += Convert.ToDecimal(dt.Rows[i][j]);
                    }
                    dt.Rows[i]["全年"] = sum;

                }

            }
            #endregion

            //删除不需要显示的行数据（科目），保留前21行
            for (int i = dt.Rows.Count - 1; i > 20; i--)
            {
                dt.Rows.RemoveAt(i);
            }
            return dt;

        }

        /// <summary>
        /// 报销费用汇总表
        /// </summary>
        /// <returns></returns>
        public static DataTable baoxiaofeiyong(string deptRealName,string periodType,  string status) {

            DataTable dt = new DataTable();
            dt.Columns.Add("项目", typeof(string));
            dt.Columns.Add("行次", typeof(int));
            string[] yf = { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" };
            for (int i = 0; i < yf.Length; i++)
            {
                dt.Columns.Add(yf[i], typeof(decimal));
                dt.Columns[yf[i]].DefaultValue = 0;
            }
            string[] rowname = { "销售费用", "管理费用", "财务费用", 
                                   "销售费用办公费", "销售费用差旅费", "销售费用招待费", "销售费用提成款", 
                                   "管理费用办公费", "管理费用差旅费", "管理费用招待费" };
            for (int i = 0; i < rowname.Length; i++)
            {
            DataRow dr1 = dt.NewRow();
            dr1["项目"] = rowname[i];
            dt.Rows.Add(dr1);
            }

            DataTable dts = RptBLL.getfybxDt("dept", status, "all", periodType);//取按部门、已审核的财务报销单claimPeriod/claimTime
            DataRow[] drs = dts.Select("dept='" + deptRealName + "' and qijian like '2020%'");
            foreach (DataRow item in drs)
            {
               string qijian = item["qijian"].ToString();
               int qijianindex =Convert.ToInt32(qijian.Substring(qijian.Length - 2));
               dt.Rows[0][qijianindex + 1] = item["销售费用小计"];
               dt.Rows[1][qijianindex + 1] = item["管理费用小计"];
               dt.Rows[2][qijianindex + 1] = item["财务费用贴息款"];
               for (int i = 3; i < rowname.Length; i++)
               {
                   dt.Rows[i][qijianindex + 1] = item[rowname[i]]; 
               }
            }
            return dt;        
        }

        /// <summary>
        /// 能源环保事业部月报表
        /// </summary>
        /// <returns></returns>
        public static DataTable getNyhjMonthRptDt(int year, string periodType, string status)
        {
            string deptName = "能源环保事业部";
            DataTable dt = new DataTable();
            dt.Columns.Add("项目", typeof(string));
            dt.Columns.Add("行次", typeof(int));
            string[] yf = { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" };
            for (int i = 0; i < yf.Length; i++)
            {
                dt.Columns.Add(yf[i], typeof(decimal));
                dt.Columns[yf[i]].DefaultValue = 0;
            }
       
            dt.Columns.Add("hasChild", typeof(Boolean));
            dt.Columns["hasChild"].DefaultValue=false;
           
          //  string xmstr = "一、营业收入;    减：营业成本;          税金及附加;          销售费用;          管理费用;          研发费用;          财务费用;     加：投资收益（损失以“-”号填列）;二、营业利润（亏损以“-”号填列）;     加：营业外收入;     减：营业外支出;三、利润总额（亏损总额以“-”号填列）;        减： 公摊费用;            所得税费用;四、净利润（净亏损以“-”号填列）";
            string xmstr = "一、营业收入;    减：营业成本;          税金及附加;          销售费用;          管理费用;          财务费用;          研发费用;     加：投资收益（损失以“-”号填列）;二、营业利润（亏损以“-”号填列）;     加：营业外收入;     减：营业外支出;三、利润总额（亏损总额以“-”号填列）;        减： 公摊费用;            所得税费用;四、净利润（净亏损以“-”号填列）";
            string[] xmsa = xmstr.Split(';');

            for (int i = 0; i < 15; i++)
            {
                DataRow dr = dt.NewRow();
                dr["项目"] = xmsa[i];
                dr["行次"] = i + 1;
                dt.Rows.Add(dr);
            }
            //允许展开标志
            dt.Rows[0]["hasChild"] = true;//销售产值
            dt.Rows[1]["hasChild"] = true;//营业成本
            dt.Rows[3]["hasChild"] = true;//销售
            dt.Rows[4]["hasChild"] = true;//管理
            dt.Rows[5]["hasChild"] = true;//财务费用
            DataTable dts = getRpt03();//能源环保事业部表，excel表2；
            DataTable dtgt = getRpt04();//公摊费用表

            DataRow kpje = dts.Select("科目='开票金额'")[0];
            DataRow zjcl=dts.Select("科目='直接材料'")[0];
            DataRow zjrg=dts.Select("科目='直接人工'")[0];
            DataRow yhp=dts.Select("科目='易耗品'")[0];
            DataRow ysf=dts.Select("科目='运输费'")[0];
            DataRow wxf=dts.Select("科目='外协费'")[0]; 
            DataRow zdf=dts.Select("科目='招待费'")[0]; 
            DataRow clf=dts.Select("科目='差旅费'")[0]; 
            DataRow bmgz=dts.Select("科目='部门工资'")[0]; 
            DataRow bgf=dts.Select("科目='办公费'")[0]; 
            DataRow yff=dts.Select("科目='研发费'")[0]; 
            DataRow zzfy = dts.NewRow();
            zzfy["科目"] = "制造费用";
            DataRow yfbl = dts.NewRow();
            yfbl["科目"] = "研发比例";
            double[] gs = CostBLL.DepMonthtWorkTime(deptName, year);//工时
            DataTable dtcb = baoxiaofeiyong(deptName, periodType, status);//取费用成本表（0：销售费用，1：管理费用，2：财务费用）
            for (int i = 0; i < yf.Length; i++)
			{
                for (int j = 0; j < 15; j++)
                {
                    dt.Rows[j][yf[i]] = 0;//表格值初始化为0
                }

                 dt.Rows[0][yf[i]] = kpje[yf[i]];//第一行赋值 :营业收入=开票金额
                 zzfy[yf[i]]=(decimal)zjcl[yf[i]]+(decimal)zjrg[yf[i]]+(decimal)yhp[yf[i]]+(decimal)ysf[yf[i]]+(decimal)wxf[yf[i]];//制造费用
                 dt.Rows[1][yf[i]] = (decimal)zzfy[yf[i]] +Convert.ToDecimal(gs[i] * 9.5);//第二行：营业成本=制造费用+工时*9.5
                 dt.Rows[2][yf[i]] =0;//第三行 :   税金及附加，未提供数据
                 dt.Rows[3][yf[i]] = dtcb.Rows[0][yf[i]];//第四行:销售费用
                 dt.Rows[4][yf[i]] = dtcb.Rows[1][yf[i]];//第五行:管理费用
                 dt.Rows[5][yf[i]] = dtcb.Rows[2][yf[i]];//第六行:财务费用          
                //研发比例
                decimal a=(decimal)zjcl[yf[i]]+(decimal)yhp[yf[i]];
                if (a==0)
	                {
		                  yfbl[yf[i]]=0;
	                }else
	                {
                       yfbl[yf[i]]=(decimal)yff[yf[i]]/a>1?1:(decimal)yff[yf[i]]/a; 
	                }                 
                //研发费用=制造费用*研发比例
                 dt.Rows[6][yf[i]]= (decimal)zzfy[yf[i]]*(decimal)yfbl[yf[i]];//第7行:研发费用 
                 dt.Rows[7][yf[i]]=0;//8.投资收益，暂无数据
                
                //9: 二、营业利润（亏损以“-”号填列） 
                dt.Rows[8][yf[i]]= (decimal)dt.Rows[0][yf[i]]- (decimal)dt.Rows[1][yf[i]]- (decimal)dt.Rows[2][yf[i]]- (decimal)dt.Rows[3][yf[i]]-(decimal)dt.Rows[4][yf[i]]- (decimal)dt.Rows[5][yf[i]]- (decimal)dt.Rows[6][yf[i]]+ (decimal)dt.Rows[7][yf[i]];//9:营业利润
                dt.Rows[9][yf[i]] = 0; //10，营业外收入，暂无数据
                dt.Rows[10][yf[i]] = 0; //11，营业外支出，暂无数据

                dt.Rows[11][yf[i]] = (decimal)dt.Rows[8][yf[i]] + (decimal)dt.Rows[9][yf[i]] - (decimal)dt.Rows[10][yf[i]];//12.利润总额=营业利润+营业外收入-营业外支出
                
                dt.Rows[12][yf[i]] = dtgt.Rows[0][yf[i]];//第13行，公摊费用
                dt.Rows[13][yf[i]] = 0;//14.所得税费用， 暂无数据
                //15
                dt.Rows[14][yf[i]] = (decimal)dt.Rows[11][yf[i]] - (decimal)dt.Rows[12][yf[i]] - (decimal)dt.Rows[13][yf[i]];//14.净利润=利润总额-公摊-所得税
            }           
            return dt; 
        }

        /// <summary>
        /// 部门销售产值月报表
        /// </summary>
        /// <param name="year"></param>
        /// <param name="periodType"></param>
        /// <param name="status"></param>
        /// <param name="deptName"></param>
        /// <returns></returns>
        public static DataTable getDeptXSCZMonthRptDt(int year, string periodType, string status, string deptName)
        {            
            DataTable dt = new DataTable();
            dt.Columns.Add("项目", typeof(string));
            dt.Columns.Add("行次", typeof(int));
            string[] yf = { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" };
            for (int i = 0; i < yf.Length; i++)
            {
                dt.Columns.Add(yf[i], typeof(decimal));
                dt.Columns[yf[i]].DefaultValue = 0;
            }

            dt.Columns.Add("hasChild", typeof(Boolean));
            dt.Columns["hasChild"].DefaultValue = false;

            string xmstr = "一、销售产值;    减：营业成本;          税金及附加;          销售费用;          管理费用;          财务费用;          研发费用;     加：投资收益（损失以“-”号填列）;二、营业利润（亏损以“-”号填列）;     加：营业外收入;     减：营业外支出;三、利润总额（亏损总额以“-”号填列）;        减： 公摊费用;            所得税费用;四、净利润（净亏损以“-”号填列）";
            string[] xmsa = xmstr.Split(';');

            for (int i = 0; i < 15; i++)
            {
                DataRow dr = dt.NewRow();  
                dr["项目"] = xmsa[i];
                dr["行次"] = i + 1;
                dt.Rows.Add(dr);
            }
            //允许展开标志
            dt.Rows[0]["hasChild"] = true;//销售产值
            dt.Rows[1]["hasChild"] = true;//营业成本
            dt.Rows[3]["hasChild"] = true;//销售
            dt.Rows[4]["hasChild"] = true;//管理
            dt.Rows[5]["hasChild"] = true;//财务费用
            DataTable dts = getRpt03();//能源环保事业部表，excel表2；

            DataTable dtgt = getRpt04();//公摊费用表

            DataRow drs = dts.Select("科目='开票金额'")[0];
            DataRow zjcl = dts.Select("科目='直接材料'")[0];
            DataRow zjrg = dts.Select("科目='直接人工'")[0];
            DataRow yhp = dts.Select("科目='易耗品'")[0];
            DataRow ysf = dts.Select("科目='运输费'")[0];
            DataRow wxf = dts.Select("科目='外协费'")[0];
            DataRow zdf = dts.Select("科目='招待费'")[0];
            DataRow clf = dts.Select("科目='差旅费'")[0];
            DataRow bmgz = dts.Select("科目='部门工资'")[0];
            DataRow bgf = dts.Select("科目='办公费'")[0];
            DataRow yff = dts.Select("科目='研发费'")[0];
            DataRow zzfy = dts.NewRow();
            zzfy["科目"] = "制造费用";
            DataRow yfbl = dts.NewRow();
            yfbl["科目"] = "研发比例"; 

            for (int i = 0; i < yf.Length; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    dt.Rows[j][yf[i]] = 0;//表格值初始化为0
                }
            }
            double[] gs = CostBLL.DepMonthtWorkTime(deptName, year);//工时
            double[] ll = CostBLL.DepMonthtItemAmount(deptName, year);//领料 
            DataTable dtcb = baoxiaofeiyong(deptName, periodType, status);//取费用成本表
            //dt.Rows[3].ItemArray = dtcb.Rows[0].ItemArray;//第四行 : 销售费用 = 招待费+差旅费
            //dt.Rows[4].ItemArray = dtcb.Rows[1].ItemArray;//第5行 : 管理费用 = 部门工资+办公费
            //dt.Rows[5].ItemArray = dtcb.Rows[2].ItemArray;//第6行 财务费用
            //dt.Rows[3]["行次"] = 4;
            //dt.Rows[4]["行次"] = 5;
            //dt.Rows[5]["行次"] = 6;
            //计算销售产值 工时*25+领料           
            for (int i = 0; i < yf.Length; i++)
            {
                dt.Rows[0][yf[i]] = gs[i] * 25 + ll[i];// 第一行赋值 : 销售产值
            }         


           for (int i = 0; i < yf.Length; i++)
            {  
                //制造费用
                zzfy[yf[i]] = (decimal)zjcl[yf[i]] + (decimal)zjrg[yf[i]] + (decimal)yhp[yf[i]] + (decimal)ysf[yf[i]] + (decimal)wxf[yf[i]];
                zzfy[yf[i]] = (decimal)zjcl[yf[i]] + (decimal)zjrg[yf[i]] + (decimal)yhp[yf[i]] + (decimal)ysf[yf[i]] + (decimal)wxf[yf[i]];//制造费用
                dt.Rows[1][yf[i]] = (decimal)zzfy[yf[i]] + Convert.ToDecimal(gs[i] * 9.5);//第二行：营业成本=制造费用+工时*9.5
                dt.Rows[2][yf[i]] = 0;//第三行 :   税金及附加，未提供数据
                dt.Rows[3][yf[i]] = dtcb.Rows[0][yf[i]];//第四行:销售费用
                dt.Rows[4][yf[i]] = dtcb.Rows[1][yf[i]];//第五行:管理费用
                dt.Rows[5][yf[i]] = dtcb.Rows[2][yf[i]];//第六行:财务费用          
                //研发比例
                decimal a = (decimal)zjcl[yf[i]] + (decimal)yhp[yf[i]];
                if (a == 0)
                {
                    yfbl[yf[i]] = 0;
                }
                else
                {
                    yfbl[yf[i]] = (decimal)yff[yf[i]] / a > 1 ? 1 : (decimal)yff[yf[i]] / a;
                }
                //研发费用=制造费用*研发比例
                dt.Rows[6][yf[i]] = (decimal)zzfy[yf[i]] * (decimal)yfbl[yf[i]];//第7行:研发费用 
                dt.Rows[7][yf[i]] = 0;//8.投资收益，暂无数据

                //9: 二、营业利润（亏损以“-”号填列） 
                dt.Rows[8][yf[i]] = (decimal)dt.Rows[0][yf[i]] - (decimal)dt.Rows[1][yf[i]] - (decimal)dt.Rows[2][yf[i]] - (decimal)dt.Rows[3][yf[i]] - (decimal)dt.Rows[4][yf[i]] - (decimal)dt.Rows[5][yf[i]] - (decimal)dt.Rows[6][yf[i]] + (decimal)dt.Rows[7][yf[i]];//9:营业利润
                dt.Rows[9][yf[i]] = 0; //10，营业外收入，暂无数据
                dt.Rows[10][yf[i]] = 0; //11，营业外支出，暂无数据

                dt.Rows[11][yf[i]] = (decimal)dt.Rows[8][yf[i]] + (decimal)dt.Rows[9][yf[i]] - (decimal)dt.Rows[10][yf[i]];//12.利润总额=营业利润+营业外收入-营业外支出

                dt.Rows[12][yf[i]] = dtgt.Rows[0][yf[i]];//第13行，公摊费用
                dt.Rows[13][yf[i]] = 0;//14.所得税费用， 暂无数据
                //15
                dt.Rows[14][yf[i]] = (decimal)dt.Rows[11][yf[i]] - (decimal)dt.Rows[12][yf[i]] - (decimal)dt.Rows[13][yf[i]];//14.净利润=利润总额-公摊-所得税
            } 
            return dt; 
        }


        /// <summary>
        /// 事业部月度报表的子报表-营业收入
        /// </summary>
        /// <param name="dept"></param>
        /// <returns></returns>
        public static DataTable getSubMonthRptDt(string dept,int year,string rptType)
        {
            DataTable dt = new DataTable();
          string  deptRealName = "";
            if (dept=="nyhb")
            {
                deptRealName = "能源环保事业部";
            DataTable dts = getRpt03();//能源环保事业部表，excel表2；
           dt.Columns.Add("项目", typeof(string));         
           dt.Columns.Add("一月", typeof(decimal));
           dt.Columns.Add("二月", typeof(decimal));
           dt.Columns.Add("三月", typeof(decimal));
           dt.Columns.Add("四月", typeof(decimal));
           dt.Columns.Add("五月", typeof(decimal));
           dt.Columns.Add("六月", typeof(decimal));
           dt.Columns.Add("七月", typeof(decimal));
           dt.Columns.Add("八月", typeof(decimal));
           dt.Columns.Add("九月", typeof(decimal));
           dt.Columns.Add("十月", typeof(decimal));
           dt.Columns.Add("十一月", typeof(decimal));
           dt.Columns.Add("十二月", typeof(decimal));
           dt.Columns.Add("全年", typeof(decimal));
           dt.Columns["全年"].DefaultValue = 0; 
           string[] yf = { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" };
           DataRow dr1 = dt.NewRow();  
           dr1["项目"] = "开票金额";
                for (int i = 0; i < yf.Length; i++)
			{
                dr1[yf[i]] = dts.Select("科目='开票金额'")[0][yf[i]];
			}                
           dt.Rows.Add(dr1);
           DataRow dr2 = dt.NewRow();
           dr2["项目"] = "回款金额";
           for (int i = 0; i < yf.Length; i++)
           {
               dr2[yf[i]] = dts.Select("科目='回款金额'")[0][yf[i]];
               dr2[yf[i]] = (decimal)dr2[yf[i]] + (decimal)dts.Select("科目='退款金额'")[0][yf[i]];
           }
           dt.Rows.Add(dr2);
           DataRow dr3 = dt.NewRow();
           dr3["项目"] = "发货金额";
           for (int i = 0; i < yf.Length; i++)
           {
               dr3[yf[i]] = (decimal)dts.Select("科目='发货金额'")[0][yf[i]] / (decimal)1.16;
           }
           dt.Rows.Add(dr3);
           if (rptType == "销售产值")
           { 
               DataRow dr4 = dt.NewRow();
               dr4["项目"] = "营业收入";   //计算销售产值 工时*25+领料 
               for (int i = 0; i < yf.Length; i++)
               {
                   dr4[yf[i]] = dr1[yf[i]];// (decimal)dts.Select("科目='发货金额'")[0][yf[i]] / (decimal)1.16;
               }
               dt.Rows.Add(dr4);  
           }
           else {  
                   DataRow dr4 = dt.NewRow();
                   dr4["项目"] = "销售产值";   //计算销售产值 工时*25+领料 
                    double[] gs = CostBLL.DepMonthtWorkTime(deptRealName, year);
                    double[] ll = CostBLL.DepMonthtItemAmount(deptRealName, year);
                   for (int i = 0; i < yf.Length; i++)
                   {
                       dr4[yf[i]] = gs[i]*25+ll[i];// (decimal)dts.Select("科目='发货金额'")[0][yf[i]] / (decimal)1.16;
                   }
                   dt.Rows.Add(dr4); 
           }
   
           

                //统计全年
           for (int i = 0; i < yf.Length; i++)
           {
               for (int j = 0; j < dt.Rows.Count; j++)
               {
                   dt.Rows[j]["全年"] = (decimal)dt.Rows[j]["全年"] + (decimal)dt.Rows[j][yf[i]];
               } 
           } 

            }
            return dt;        
        }
        /// <summary>
        /// 获取rpt03表，即能源环保事业部表，excel表2；
        /// </summary>
        /// <returns></returns>
        public static DataTable getRpt03() {
            string sqlstr = @"SELECT  [部门]
            ,[FItemID]
      ,[FAccountID]
      ,[科目]
      ,isnull([一月],0) as [一月]
	   ,isnull([二月],0) as [二月]
	    ,isnull([三月],0)as [三月]
		 ,isnull([四月],0) as [四月]
		  ,isnull([五月],0) as [五月]
		  ,isnull([六月],0) as [六月]
		  ,isnull([七月],0) as [七月]
		  ,isnull([八月],0) as [八月]
		  ,isnull([九月],0) as [九月]
		 ,isnull([十月],0) as [十月]
		 ,isnull([十一月],0) as [十一月] ,isnull([十二月],0) as [十二月]    
  FROM [dbo].[rpt03]";
            DataTable dts = SqlHelper.GetDataTableK3(sqlstr);//能源环保事业部表，excel表2；
            return dts;        
        }
        /// <summary>
        /// 获取rpt04表，即公摊费用表，excel表1；
        /// </summary>
        /// <returns></returns>
        public static DataTable getRpt04()
        {
            string sqlgt = @"SELECT  isnull(sum([一月]),0)/3 as [一月],
isnull(sum([二月]),0)/3 as [二月],
  isnull(sum([三月]),0)/3 as [三月],  
  isnull(sum([四月]),0)/3 as [四月],
 isnull(sum([五月]),0)/3 as [五月],
   isnull(sum([六月]),0)/3 as [六月],  isnull(sum([七月]),0)/3 as [七月],
 isnull(sum([八月]),0)/3 as [八月],
   isnull(sum([九月]),0)/3 as [九月],  isnull(sum([十月]),0)/3 as [十月],
 isnull(sum([十一月]),0)/3 as [十一月],
  isnull( sum([十二月]),0)/3 as [十二月] 
  FROM  [dbo].[rpt04]
  where [科目] not in ('综合部付款','补贴收入','营业外收入')";
            DataTable dtgt = SqlHelper.GetDataTableK3(sqlgt);//公摊费用表
            return dtgt;
        }
        //营业成本-子报表数据
        public static DataTable getSubMonthRptYYCB(string dept,int year)
        {

            DataTable dt = new DataTable();
            if (dept == "nyhb")
            {
                string deptName = "能源环保事业部";
                DataTable dts = getRpt03();//能源环保事业部表，excel表2；
                dt.Columns.Add("项目", typeof(string));
                dt.Columns.Add("一月", typeof(decimal));
                dt.Columns.Add("二月", typeof(decimal));
                dt.Columns.Add("三月", typeof(decimal));
                dt.Columns.Add("四月", typeof(decimal));
                dt.Columns.Add("五月", typeof(decimal));
                dt.Columns.Add("六月", typeof(decimal));
                dt.Columns.Add("七月", typeof(decimal));
                dt.Columns.Add("八月", typeof(decimal));
                dt.Columns.Add("九月", typeof(decimal));
                dt.Columns.Add("十月", typeof(decimal));
                dt.Columns.Add("十一月", typeof(decimal));
                dt.Columns.Add("十二月", typeof(decimal));
                dt.Columns.Add("全年", typeof(decimal));
                dt.Columns["全年"].DefaultValue = 0;
                string[] yf = { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" };
                DataRow dr1 = dt.NewRow();
                dr1["项目"] = "直接材料";
                dt.Rows.Add(dr1);
                DataRow dr2 = dt.NewRow();
                dr2["项目"] = "直接人工";
                dt.Rows.Add(dr2);
                DataRow dr3 = dt.NewRow();
                dr3["项目"] = "易耗品";
                dt.Rows.Add(dr3);
                DataRow dr4 = dt.NewRow();
                dr4["项目"] = "运输费";
                dt.Rows.Add(dr4);
                DataRow dr5 = dt.NewRow();
                dr5["项目"] = "外协费";
                dt.Rows.Add(dr5);
                DataRow dr6 = dt.NewRow();
                dr6["项目"] = "制造费用摊销";
                dt.Rows.Add(dr6);


                DataRow drs = dts.Select("科目='开票金额'")[0];
                DataRow zjcl = dts.Select("科目='直接材料'")[0];
                DataRow zjrg = dts.Select("科目='直接人工'")[0];
                DataRow yhp = dts.Select("科目='易耗品'")[0];
                DataRow ysf = dts.Select("科目='运输费'")[0];
                DataRow wxf = dts.Select("科目='外协费'")[0];
                DataRow zdf = dts.Select("科目='招待费'")[0];
                DataRow clf = dts.Select("科目='差旅费'")[0];
                DataRow bmgz = dts.Select("科目='部门工资'")[0];
                DataRow bgf = dts.Select("科目='办公费'")[0];
                DataRow yff = dts.Select("科目='研发费'")[0];
                DataRow zzfy = dts.NewRow();
                zzfy["科目"] = "制造费用";
                DataRow yfbl = dts.NewRow();
                yfbl["科目"] = "研发比例";
                DataRow ddbl = dts.NewRow();
                yfbl["科目"] = "订单比例";
                double[] gs = CostBLL.DepMonthtWorkTime(deptName, year);//工时
                //double[] ll = CostBLL.DepMonthtItemAmount(deptRealName, year);
                //for (int i = 0; i < yf.Length; i++)
                //{
                //    dr4[yf[i]] = gs[i] * 25 + ll[i];// (decimal)dts.Select("科目='发货金额'")[0][yf[i]] / (decimal)1.16;
                //}
                //dt.Rows.Add(dr4); 
                for (int i = 0; i < yf.Length; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        dt.Rows[j][yf[i]] = 0;//表格值初始化为0
                    }
                   
                    //制造费用=第二行：营业成本
                   // zzfy[yf[i]] = (decimal)zjcl[yf[i]] + (decimal)zjrg[yf[i]] + (decimal)yhp[yf[i]] + (decimal)ysf[yf[i]] + (decimal)wxf[yf[i]];
                   
                    //订单比例
                    decimal a = (decimal)zjcl[yf[i]] + (decimal)yhp[yf[i]];
                    if (a == 0)
                    {
                        ddbl[yf[i]] = 1;
                    }
                    else
                    {
                        ddbl[yf[i]] =1-( (decimal)yff[yf[i]] / a > 1 ? 1 : (decimal)yff[yf[i]] / a);
                    }

                    dt.Rows[0][yf[i]] = (decimal)zjcl[yf[i]] * (decimal)ddbl[yf[i]];//第一行赋值 :直接材料
                    dt.Rows[1][yf[i]] = (decimal)zjrg[yf[i]] * (decimal)ddbl[yf[i]];//第一行赋值 :直接人工
                    dt.Rows[2][yf[i]] = (decimal)yhp[yf[i]] * (decimal)ddbl[yf[i]];//第一行赋值 :易耗品
                    dt.Rows[3][yf[i]] = (decimal)ysf[yf[i]] * (decimal)ddbl[yf[i]];//第一行赋值 :运输费
                    dt.Rows[4][yf[i]] = (decimal)wxf[yf[i]] * (decimal)ddbl[yf[i]];//第一行赋值 :外协费
                    dt.Rows[5][yf[i]] = Convert.ToDecimal(gs[i]*9.5);//第一行赋值 :制造费用摊销
 
                } 
              
                //统计全年
                for (int i = 0; i < yf.Length; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                         dt.Rows[j]["全年"] = (decimal)dt.Rows[j]["全年"] + (decimal)dt.Rows[j][yf[i]];
                    }
                }

            }
            return dt;

           // throw new NotImplementedException();
        }
    }
}
