﻿using AchieveCommon;
using AchieveEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AchieveBLL
{
   public class RDBLL
    {
        //根据nodeid，projectid更新研发项目节点信息
        public static bool updateNode(string nodeid, string executor, string projectID, string ps, string pe, string rs, string re, string AccountName,int status, string Remark = "")//, decimal estWorkTime = 0, decimal weight1 = 0, string workitem1 = "", decimal weight2 = 0, string workitem2 = "")
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tbRDNode set executor=@executor, Remark=@remark, ");
            strSql.Append("pstime=@pstime,petime=@petime,rstime=@rstime,retime=@retime,UpdateTime=@UpdateTime,UpdateBy=@UpdateBy,status=@status where projectID=@projectID and nodeid=@nodeid ");
            SqlParameter[] parameters = {	
                    new SqlParameter("@executor", executor) , 
	                   new SqlParameter("@remark", Remark) ,            
                        //new SqlParameter("@estworktime",estWorkTime) , 
                        // new SqlParameter("@weight1",weight1) , 
                        // new SqlParameter("@workitem1",workitem1) ,
                        // new SqlParameter("@weight2",weight2) , 
                       new SqlParameter("@status",status),
                        new SqlParameter("@pstime", ps) ,            
                        new SqlParameter("@petime",pe) , 
                        new SqlParameter("@rstime", rs) ,  
                        new SqlParameter("@retime", re) ,            
                        new SqlParameter("@projectID",projectID) ,            
                        new SqlParameter("@nodeid",nodeid) ,        
                        new SqlParameter("@UpdateTime", DateTime.Now) ,            
                        new SqlParameter("@UpdateBy", AccountName) 
            };
            int x = SqlHelper.ExecuteNonQuery(SqlHelper.connStr, CommandType.Text, strSql.ToString(), parameters);

            if (x > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



              //field: 'yylxTime', title: '预研立项时间', sortable: false, formatter: getDate
              //                        }, {
              //                            field: 'cbsjTime', title: '初步设计时间', sortable: false, formatter: getDate
              //                        }, {
              //                            field: 'gcsjTime', title: '工程设计时间', sortable: false, formatter: getDate
              //                        }, {
              //                            field: 'yjzzTime', title: '样机制作时间', sortable: false, formatter: getDate
              //                        }, {
              //                            field: 'sxcgTime',
         public static DataTable GetRDList(string sortAndOrder,int pagesize, int pageindex, string strWhere, out int totalCount){ 
           
          string columns = @"
ProjectID,
ProjectNo,
ProjectName,
ProjectManager,
ProjectClerk,
Remark,
UpdateTime,
UpdateBy,
AppendID,
AppendListID,
CreateBy,
CreateTime,
Status,
Department,
IcmoNo,
FitemNo,
FModel,
FName,
RDType,
pLevel,
aFactor,
iFactor,
RDEstCost,
estPrice,
startTime,
endTime,
awardAmount,
evalAmount";
                DataTable dt = AchieveCommon.SqlPagerHelper.GetPager("tbRDMain", columns, sortAndOrder, pagesize, pageindex, strWhere, out totalCount);
             //添加节点时间字段
                dt.Columns.Add("yylxTime", typeof(DateTime));
                dt.Columns.Add("cbsjTime", typeof(DateTime));
                dt.Columns.Add("gcsjTime", typeof(DateTime));
                dt.Columns.Add("yjzzTime", typeof(DateTime));
                dt.Columns.Add("sxcgTime", typeof(DateTime));
                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        string sql = string.Format("select NodeID, PSTime from tbRDnode where ProjectID= '{0}'", item["ProjectID"]);
                        DataTable dtime = SqlHelper.GetDataTableIE(sql);
                        item["yylxTime"] = dtime.Select("NodeID=" + 11)[0]["PSTime"];
                        item["cbsjTime"] = dtime.Select("NodeID=" + 12)[0]["PSTime"];
                        item["gcsjTime"] = dtime.Select("NodeID=" + 13)[0]["PSTime"];
                        item["yjzzTime"] = dtime.Select("NodeID=" + 14)[0]["PSTime"];
                        item["sxcgTime"] = dtime.Select("NodeID=" + 15)[0]["PSTime"];
                    }
                    catch (Exception)
                    {

                        continue;
                    } 
                } 

                return dt;
         
         }

      
           
               
        public static bool updateRDMain(RDMainEntity RDmain)
        {
            return RDmain.Update();
          
        }

    }
}
