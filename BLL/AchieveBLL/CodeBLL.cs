﻿using AchieveCommon;
using AchieveDALFactory;
using AchieveEntity;
using AchieveInterfaceDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Timers;

namespace AchieveBLL
{
   public class CodeBLL
    {
       /// <summary>
        /// 返回基础信息编码json，一般用于combobox选项，前端传入typeid 
       /// </summary>
       /// <returns></returns>
       public static DataTable GetCodebyTypeId(int typeid)
       { 
           string sql = string.Format(" select codename,codeno from tbBasicInfoCode where typeid={0}", typeid);
           DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStr, sql);
           return dt;
       }

       private static List<CodeTree> getChildByCodeID(int CodeID) {

           string sql = @"select a.FinterID,a.FItemID,a.FQty,b.FName,b.FOrderPrice,isnull(c.FinterID,0) as cFinterID 
                        from icCodechild a left Join t_ICITEMCORE b on a.FItemID=b.FItemID left join icCode c on c.fitemid=a.fitemid
                        where a.FinterID=" + CodeID;
           DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql);
           List<CodeTree> btlist = new List<CodeTree>();
           if (dt.Rows.Count > 0)//存在子物料
                   {
                       //添加子物料信息
                       foreach (DataRow item in dt.Rows)
                       {
                           CodeTree bt = new CodeTree();
                           bt.FinterID = (Int32)item["cFinterID"];
                           bt.FItemID = (Int32)item["FItemID"];
                           bt.FName = (string)item["FName"];
                           bt.FQty = Convert.ToDecimal( item["FQty"]);
                           bt.FOrderPrice = Convert.ToDecimal(item["FOrderPrice"]);
                           if (bt.FinterID>0)//存在物料编码
                               {
                               //递归查找
                                    bt.children = getChildByCodeID(bt.FinterID);
                               }
                           btlist.Add(bt);
                  
                       }
                   }
           else {
               return btlist;
           
           }
           return btlist;
       
       }

       public static CodeTree getCodeTreeByItemID(int itemID)
       {
           CodeTree bt = new CodeTree();
           //获取根物料的信息
           string sql = @"select a.FItemID,FName,FOrderPrice,b.FinterID from t_ICITEMCORE a left join icCode b on a.FItemID=b.FItemID where b.FinterID is not null and a.FItemID=" + itemID;
           DataTable dt = SqlHelper.GetDataTable(SqlHelper.connStrK3, sql);
           int c = dt.Rows.Count;
           if (c > 0)
           {
               bt.FinterID = (int)dt.Rows[0]["FinterID"];
               bt.FItemID = itemID;
               bt.FName = dt.Rows[0]["FName"].ToString();
               bt.children = getChildByCodeID(bt.FinterID);// bt.children.Add();
           }
           else {
               bt.FName = "不存在该物料Code信息！";
               return bt;
           }

           //获取子物料数据


           if (true)
           {

           }

           return bt;
       }

       /// <summary>
       /// Code树形结构数据转换为json
       /// </summary>
       /// <param name="bt"></param>
       /// <returns></returns>
       public static string CodeTree2Json(CodeTree bt) {
           StringBuilder sb = new StringBuilder();
           sb.Append("{");
           sb.Append(string.Format("\"name\":\"{0}\",",bt.name));
           sb.Append(string.Format("\"FinterID\":\"{0}\",", bt.FinterID));
           sb.Append(string.Format("\"FItemID\":\"{0}\",", bt.FItemID));
           sb.Append(string.Format("\"FName\":\"{0}\",", bt.FName));
           sb.Append(string.Format("\"FQty\":\"{0}\",", bt.FQty));
           sb.Append(string.Format("\"FOrderPrice\":\"{0}\",", bt.FOrderPrice));
            sb.Append(string.Format("\"value\":{0}",bt.value));
            if (bt.children != null)
            {
                 sb.Append(string.Format(",\"children\":{0}",getnodejson(bt.children)));
            }
             sb.Append("}");
         
            return sb.ToString();
           //sb.Append(string.Format("children"))       
       }

       //Code转为json的递归函数
       private static string getnodejson(List<CodeTree> list)
       {
           StringBuilder sb = new StringBuilder();
           sb.Append("[");
           foreach (CodeTree item in list)
           {
                 StringBuilder sb1 = new StringBuilder();
                   sb1.Append("{");
                   sb1.Append(string.Format("\"name\":\"{0}\",",item.name));
                   sb1.Append(string.Format("\"FinterID\":\"{0}\",", item.FinterID));
                   sb1.Append(string.Format("\"FItemID\":\"{0}\",", item.FItemID));
                   sb1.Append(string.Format("\"FName\":\"{0}\",", item.FName));
                   sb1.Append(string.Format("\"FQty\":\"{0}\",", item.FQty));
                   sb1.Append(string.Format("\"FOrderPrice\":\"{0}\",", item.FOrderPrice));
                    sb1.Append(string.Format("\"value\":{0}",item.value));
                    if (item.children !=null)
                    {
                        sb1.Append(string.Format(",\"children\":{0}", getnodejson(item.children)));
                    } 
                  
                    sb1.Append("},");
                    sb.Append(sb1.ToString());

           } 
           sb.Replace(',',']', sb.Length - 1, 1);
           return sb.ToString(); 
       }


      
   
   }

   

   public class CodeTree{
       public CodeTree() {
             
       }
       public CodeTree(int finterid,string fname)
       {
           this.FinterID = finterid;
           this.FName = fname;
       }
       public string name{
           get { return this.FName; }
       
       }

       public int value
       {
           get { return this.FinterID; }

       }
       public int parentId;
       public int FinterID;
       public int FItemID;
       public string FName;
       public decimal FQty=new decimal();//数量  
       public decimal FOrderPrice =new decimal();
       public List<CodeTree> children;// = new List<CodeTree>();
    }


}
